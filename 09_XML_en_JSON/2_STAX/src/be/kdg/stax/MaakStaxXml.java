package be.kdg.stax;

import be.kdg.model.Piloot1;
import be.kdg.model.Piloten1;
import com.sun.xml.internal.txw2.output.IndentingXMLStreamWriter;

import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.List;

public class MaakStaxXml {
    private static final String FILENAME = "09_XML_en_JSON/2_STAX/data/piloten.xml";

    public static void main(String[] args) {
        List<Piloot1> piloten = Piloten1.getPiloten();

        try {
            PrintWriter printWriter = new PrintWriter(
                    new OutputStreamWriter(new FileOutputStream(FILENAME)));

            XMLOutputFactory xmlOutputFactory = XMLOutputFactory.newInstance();
            XMLStreamWriter streamWriter = xmlOutputFactory.createXMLStreamWriter(printWriter);
            streamWriter = new IndentingXMLStreamWriter(streamWriter);

            //document en rootelement wegschrijven:
            streamWriter.writeStartDocument();
            streamWriter.writeStartElement("race-monaco");


            for (Piloot1 piloot : piloten) {
                //alle Piloot1-elementen met attribuut en children wegschrijven
                streamWriter.writeStartElement("piloot");
                streamWriter.writeAttribute("team", piloot.getTeam());

                streamWriter.writeStartElement("naam");
                streamWriter.writeCharacters(piloot.getNaam());
                streamWriter.writeEndElement();

                streamWriter.writeStartElement("nummer");
                streamWriter.writeCharacters(String.valueOf(piloot.getNummer()));
                streamWriter.writeEndElement();

                streamWriter.writeStartElement("wedstrijd-datum");
                streamWriter.writeCharacters(piloot.getWedstrijdDatum().toString());
                streamWriter.writeEndElement();

                streamWriter.writeEndElement();
            }
            //end-elementen wegschrijven
            streamWriter.writeEndElement();
            streamWriter.writeEndDocument();
            //close
            streamWriter.close();
            printWriter.close();

            System.out.println("XML bestand: \"" + FILENAME + "\" opgeslagen!");

        } catch (XMLStreamException | IOException e) {
            e.printStackTrace();
        }
    }
}