package be.kdg.stax;

import be.kdg.model.Piloot1;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.*;
import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ParseStaxXml {
    private static final String FILENAME = "09_XML_en_JSON/2_STAX/data/piloten.xml";

    public static void main(String[] args) {
        boolean naamElementGevonden = false;
        boolean nummerElementGevonden = false;
        boolean wedstrijdElementGevonden = false;
        List<Piloot1> piloten = new ArrayList<>();

        try {
            XMLInputFactory factory = XMLInputFactory.newInstance();
            XMLEventReader eventReader = factory.createXMLEventReader(new FileReader(FILENAME));

            Piloot1 piloot = null;
            while (eventReader.hasNext()) {
                XMLEvent event = eventReader.nextEvent();

                if (event.getEventType() == XMLStreamConstants.START_ELEMENT) {
                    StartElement startElement = event.asStartElement();
                    String tagName = startElement.getName().getLocalPart();
                    switch (tagName) {
                        case "piloot":
                            piloot = new Piloot1();
                            Iterator<Attribute> attributes = startElement.getAttributes();
                            piloot.setTeam(attributes.next().getValue());
                            break;
                        case "naam":
                            naamElementGevonden = true;
                            break;
                        case "nummer":
                            nummerElementGevonden = true;
                            break;
                        case "wedstrijd-datum":
                            wedstrijdElementGevonden = true;
                            break;
                    }
                } else if (event.getEventType() == XMLStreamConstants.CHARACTERS) {
                    Characters characters = event.asCharacters();
                    if (naamElementGevonden) {
                        piloot.setNaam(characters.getData());
                        naamElementGevonden = false;
                    }
                    if (nummerElementGevonden) {
                        piloot.setNummer(Integer.parseInt(characters.getData()));
                        nummerElementGevonden = false;
                    }
                    if (wedstrijdElementGevonden) {
                        piloot.setWedstrijdDatum(LocalDate.parse(characters.getData()));
                        wedstrijdElementGevonden = false;
                    }
                } else if (event.getEventType() == XMLStreamConstants.END_ELEMENT) {
                    EndElement endElement = event.asEndElement();
                    if (endElement.getName().getLocalPart().equals("piloot")) {
                        // end tag gelezen, dus piloot is volledig en kan aan List worden toegevoegd
                        piloten.add(piloot);
                    }
                }
            }
        } catch (XMLStreamException | IOException e) {
            e.printStackTrace();
        }

        System.out.println("Overzicht piloten:\n");
        piloten.forEach(System.out::println);

    }
}

