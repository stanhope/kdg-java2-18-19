package be.kdg.stax;

import be.kdg.model.Piloten3;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.File;

public class Unmarshal {
    private static final String FILENAME = "09_XML_en_JSON/3_JAXB/data/piloten.xml";

    public static void main(String[] args) {
        try {
            JAXBContext context = JAXBContext.newInstance(Piloten3.class);
            Unmarshaller unmarshaller = context.createUnmarshaller();

            File file = new File(FILENAME);
            //TODO: fix this thing not working
            Piloten3 piloten = (Piloten3) unmarshaller.unmarshal(file);

            System.out.println("Overzicht piloten: \n");
            piloten.getPiloten().forEach(System.out::println);

        } catch (JAXBException e) {
            e.printStackTrace();
        }
    }
}