package be.kdg.stax;

import be.kdg.model.Piloten3;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.File;

public class Marshal {
    private static final String FILENAME = "09_XML_en_JSON/3_JAXB/data/piloten.xml";

    public static void main(String[] args) {
        Piloten3 piloten = new Piloten3();

        try {
            JAXBContext context = JAXBContext.newInstance(Piloten3.class);
            Marshaller jaxbMarshaller = context.createMarshaller();

            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

            jaxbMarshaller.marshal(piloten, new File(FILENAME));
            jaxbMarshaller.marshal(piloten, System.out);

        } catch (JAXBException e) {
            e.printStackTrace();
        }
    }
}