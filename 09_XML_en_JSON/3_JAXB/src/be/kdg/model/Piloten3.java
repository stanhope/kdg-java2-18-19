package be.kdg.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement
public class Piloten3 {
    private List<Piloot1> myList;

    // Default constructor verplicht aanwezig!
    public Piloten3() {
        myList = new ArrayList<>();
        myList.add(new Piloot1("Mercedes", "Hamilton", 44, LocalDate.of(2018, 10, 12)));
        myList.add(new Piloot1("Mercedes", "Rosberg", 6, LocalDate.of(2017, 4, 1)));
        myList.add(new Piloot1("Ferrari", "Vettel", 5, LocalDate.of(2018, 7, 27)));
        myList.add(new Piloot1("Ferrari", "Räikkönen", 7, LocalDate.now()));
        myList.add(new Piloot1("McLaren", "Bottas", 77, LocalDate.of(2018, 9, 5)));
        myList.add(new Piloot1("McLaren", "Massa", 19, LocalDate.now()));
        myList.add(new Piloot1("Red Bull", "Ricciarddo", 3, LocalDate.of(2016, 11, 30)));
        myList.add(new Piloot1("Red Bull", "Kvyat", 26, LocalDate.now()));
    }

    @XmlElement(name="piloot")
    public List<Piloot1> getPiloten() {
        return myList;
    }

    public void setMyList(List<Piloot1> myList) {
        this.myList = myList;
    }
}
