package be.kdg.model;

import be.kdg.stax.LocalDateAdapter;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapters;
import java.time.LocalDate;

@XmlRootElement
@XmlType(propOrder = {"nummer", "naam", "wedstrijdDatum"})
public class Piloot3 {

    private String team;
    private String naam;
    private int nummer;
    @XmlJavaTypeAdapter(LocalDateAdapter.class)
    private LocalDate wedstrijdDatum;

    public Piloot3() {
    }

    public Piloot3(String team, String naam, int nummer, LocalDate wedstrijdDatum) {
        this.team = team;
        this.naam = naam;
        this.nummer = nummer;
        this.wedstrijdDatum = wedstrijdDatum;
    }

    public String getTeam() {
        return team;
    }

    public String getNaam() {
        return naam;
    }

    public int getNummer() {
        return nummer;
    }

    @XmlJavaTypeAdapter(LocalDateAdapter.class)
    public LocalDate getWedstrijdDatum() {
        return wedstrijdDatum;
    }

    @XmlAttribute
    public void setTeam(String team) {
        this.team = team;
    }

    public void setNaam(String naam) {
        this.naam = naam;
    }

    public void setNummer(int nummer) {
        this.nummer = nummer;
    }

    @XmlJavaTypeAdapter(LocalDateAdapter.class)
    public void setWedstrijdDatum(LocalDate wedstrijdDatum) {
        this.wedstrijdDatum = wedstrijdDatum;
    }

    @Override
    public String toString() {
        return String.format("%-10s (%-8s) %2d  --> %s", naam, team, nummer, wedstrijdDatum);
    }
}
