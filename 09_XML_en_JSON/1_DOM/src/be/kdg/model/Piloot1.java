package be.kdg.model;

import java.time.LocalDate;

public class Piloot1 {
    private String team;
    private String naam;
    private int nummer;
    private LocalDate wedstrijdDatum;

    public Piloot1(String team, String naam, int nummer, LocalDate wedstrijdDatum) {
        this.team = team;
        this.naam = naam;
        this.nummer = nummer;
        this.wedstrijdDatum = wedstrijdDatum;
    }

    public Piloot1() {
    }

    public String getTeam() {
        return team;
    }

    public String getNaam() {
        return naam;
    }

    public int getNummer() {
        return nummer;
    }

    public LocalDate getWedstrijdDatum() {
        return wedstrijdDatum;
    }

    public void setTeam(String team) {
        this.team = team;
    }

    public void setNaam(String naam) {
        this.naam = naam;
    }

    public void setNummer(int nummer) {
        this.nummer = nummer;
    }

    public void setWedstrijdDatum(LocalDate wedstrijdDatum) {
        this.wedstrijdDatum = wedstrijdDatum;
    }

    @Override
    public String toString() {
        return String.format("%-10s (%-8s) %2d  --> %s", naam, team, nummer, wedstrijdDatum);
    }
}
