package be.kdg.stax;

import be.kdg.model.Piloot1;
import be.kdg.model.Piloten1;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.util.List;

public class MaakDomXml {
    private static final String FILENAME = "09_XML_en_JSON/1_DOM/data/piloten.xml";

    public static void main(String[] args) {
        List<Piloot1> pilootList = Piloten1.getPiloten();

        try {
            //document maken:
            Document doc = DocumentBuilderFactory.newInstance()
                    .newDocumentBuilder().newDocument();
            //root element maken:
            Element root = doc.createElement("piloten");
            doc.appendChild(root);

            //piloot elementen toevoegen:
            for (Piloot1 piloot : pilootList) {

                Element pilootEl = doc.createElement("piloot");
                //attribuut toevoegen:
                pilootEl.setAttribute("team", piloot.getNaam());

                //naam toevoegen:
                Element naamEl = doc.createElement("naam");
                naamEl.setTextContent(piloot.getNaam());
                pilootEl.appendChild(naamEl);

                //nummer toevoegen:
                Element nummerEl = doc.createElement("nummer");
                nummerEl.setTextContent(String.valueOf(piloot.getNummer()));
                pilootEl.appendChild(nummerEl);

                //wedstrijd toevoegen:
                Element wedstrijdEl = doc.createElement("wedstrijd");
                wedstrijdEl.setTextContent(piloot.getWedstrijdDatum().toString());
                pilootEl.appendChild(wedstrijdEl);

                //piloot toevoegen aan root
                root.appendChild(pilootEl);
            }
            // source voor wegschrijven
            DOMSource domSource = new DOMSource(doc);
            // schrijver
            Transformer transformer = TransformerFactory.newInstance().newTransformer();
            // pretty print
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
            //display xml:
            transformer.transform(domSource, new StreamResult(System.out));

            //save file
            transformer.transform(domSource, new StreamResult(new File(FILENAME)));

            System.out.println("File Saved!");
        } catch (ParserConfigurationException | TransformerException e) {
            e.printStackTrace();
        }
    }
}