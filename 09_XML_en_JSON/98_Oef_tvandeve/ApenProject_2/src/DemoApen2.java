import apen.Aap2;
import apen.Apen2;
import conversie.ConversieTools2;

import javax.xml.bind.JAXBException;
import javax.xml.stream.XMLStreamException;
import java.io.IOException;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class DemoApen2 {
    public static void main(String[] args) {
        try {
            Apen2 apen = ConversieTools2.JaxbReadXML("09_XML_en_JSON/98_Oef_tvandeve/ApenProject_2/AlleApen.xml");
            List<Aap2> myApenList = apen.getApenList();

            //Todo 2B:sorteer  de data in apenlist op naam, groepeer ze per soort en stop
            // ze in soortMap. druk de map af            Map<String, List<Aap2>> soortMap = null;
            Map<String, List<Aap2>> soortMap = myApenList.stream()
                    .sorted(Comparator.comparing(Aap2::getNaam))
                    .collect(Collectors.groupingBy(Aap2::getSoort));
            System.out.println("\nAlle apen alfabetisch per soort:");
            for (String s : soortMap.keySet()){
                System.out.print(s + " -> ");
                soortMap.get(s).forEach(aap -> System.out.printf("%s ", aap.getNaam()));
                System.out.println();
            }

            //Opgave 2C:Maak opnieuw gebruik van Stream en zoek de zwaarste mannetjesaap.

            Aap2 zwaarste = myApenList.stream().max(Comparator.comparing(Aap2::getGewicht))
                    .get();
            System.out.printf("\nZwaarste mannetjesaap: %s = %.2f kg\n"
                    , zwaarste.getNaam(), zwaarste.getGewicht());

            ConversieTools2.StaxWriteXML(soortMap, "09_XML_en_JSON/98_Oef_tvandeve/ApenProject_2/ApenPerSoort.xml");

        } catch (JAXBException | XMLStreamException | IOException
                e) {
            System.out.println(e.getMessage());
        }
    }
}

/**
 * GEWENSTE AFDRUK:
 * <p>
 * Alle apen alfabetisch per soort:
 * bonobo   -> Koko, Pipi
 * brulaap  -> Grompy, Shout
 * doodshoofdaap -> Bumba, Griezel
 * gorilla  -> Gust, Pinky, Sneeuwvlokje
 * leeuwaap -> Java, Monkey
 * maki     -> Banana, Nikita
 * neusaap  -> Nancy, Pinokkio
 * orang-oetan -> Kingkong, Louie, Rosa
 * <p>
 * Zwaarste mannetjesaap: Gust = 275,0 kg
 * File saved!
 */
