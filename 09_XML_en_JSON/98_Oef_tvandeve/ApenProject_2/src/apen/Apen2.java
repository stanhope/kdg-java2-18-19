package apen;


import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement(name = "APEN")
public class Apen2 {
    private List<Aap2> apenList = new ArrayList<>();

    @XmlElement(name = "AAP")
    public void setApenList(List<Aap2> apenList) {
        this.apenList = apenList;
    }

    public List<Aap2> getApenList() {
        return apenList;
    }

    public void add(Aap2 aap) {
        this.apenList.add(aap);
    }
}
