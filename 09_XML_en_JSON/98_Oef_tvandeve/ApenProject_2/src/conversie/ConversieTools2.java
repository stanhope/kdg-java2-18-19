package conversie;

import apen.Aap2;
import apen.Apen2;
import com.sun.xml.internal.txw2.output.IndentingXMLStreamWriter;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import java.io.*;
import java.util.*;

public class ConversieTools2 {
	public static Apen2 JaxbReadXML(String fileName) throws JAXBException {
		//TODO 2A lees met  jaxb de data uit filename en geeft terug als Apen2 object

		Apen2 apen = null;
		try {
			JAXBContext context = JAXBContext.newInstance(Apen2.class);
			Unmarshaller unmarshaller = context.createUnmarshaller();

			File file = new File(fileName);
			apen = (Apen2) unmarshaller.unmarshal(file);
		} catch (JAXBException e){
			e.printStackTrace();
		}
		return apen;
	}


	public static void StaxWriteXML(Map<String, List<Aap2>> myMap, String fileName) throws XMLStreamException, IOException {
		FileWriter writerXml = new FileWriter( (fileName));
		XMLOutputFactory xmlOutputFactory = XMLOutputFactory.newInstance();
		XMLStreamWriter xmlStreamWriter = xmlOutputFactory.createXMLStreamWriter(writerXml);
		//Specifieke xmlStreamWriter om indenting in de XML-output in te voegen:
		xmlStreamWriter = new IndentingXMLStreamWriter(xmlStreamWriter);
		xmlStreamWriter.writeStartDocument();

		//TODO 2D Maak een root element <apen>
		// Van elk Aap2-object schrijf je enkel de eigenschappen naam, gewicht, geboorte en kooi weg.
		xmlStreamWriter.writeStartElement("apen"); // <apen>

		for (String keySet : myMap.keySet()){
			xmlStreamWriter.writeStartElement("soort");
			xmlStreamWriter.writeCharacters(keySet); // <soort>

			for (Aap2 aap: myMap.get(keySet)){
				xmlStreamWriter.writeStartElement("aap"); // <aap>

				xmlStreamWriter.writeStartElement("naam"); // <naam>
				xmlStreamWriter.writeCharacters(aap.getNaam());
				xmlStreamWriter.writeEndElement(); // </naam>

				xmlStreamWriter.writeStartElement("gewicht"); // <gewicht>
				xmlStreamWriter.writeCharacters(String.valueOf(aap.getGewicht()));
				xmlStreamWriter.writeEndElement(); // </gewicht>

				xmlStreamWriter.writeStartElement("geboorte"); // <geboorte>
				xmlStreamWriter.writeCharacters(String.valueOf(aap.getGeboorte()));
				xmlStreamWriter.writeEndElement(); // </geboorte>

				xmlStreamWriter.writeStartElement("kooi"); // <kooi>
				xmlStreamWriter.writeCharacters(aap.getKooi());
				xmlStreamWriter.writeEndElement(); // </kooi>

				xmlStreamWriter.writeEndElement(); //</aap>
			}
			xmlStreamWriter.writeEndElement(); // </soort>
		}

		xmlStreamWriter.writeEndElement(); // </apen>
		xmlStreamWriter.writeEndDocument();
		xmlStreamWriter.close();
		writerXml.close();

		System.out.println("File saved!");
	}
}