package conversie;

import apen.Aap1;
import apen.Geslacht1;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.*;
import java.time.LocalDate;
import java.util.*;

public class ConversieTools1 {

    public static List<Aap1> readTxtFile(String bestand) throws IOException {
        List<Aap1> list = new ArrayList<>();
        String regel = "";
        try (BufferedReader br = new BufferedReader(new FileReader(bestand))) {
            while ((regel = br.readLine()) != null) {
                //Elke regel in het bestand bevat de data van een aap, gescheiden door komma's
                StringTokenizer tokenizer = new StringTokenizer(regel, ",");

                String naam = tokenizer.nextToken();
                String soort = tokenizer.nextToken();
                String familie = tokenizer.nextToken();
                Geslacht1 geslacht = tokenizer.nextToken().charAt(0) == 'M' ? Geslacht1.MAN : Geslacht1.VROUW;
                LocalDate geboorte = LocalDate.parse(tokenizer.nextToken());
                double gewicht = Double.parseDouble(tokenizer.nextToken());
                String kooi = tokenizer.nextToken();

                // Nieuwe aap maken en toevoegen aan de List:
                list.add(new Aap1(naam, soort, familie, geslacht, geboorte, gewicht, kooi));
            }
            return list;
        } catch (NoSuchElementException | NumberFormatException e1) {
            throw new IOException("Leesfout in regel: " + regel, e1);
        } catch (IOException e2) {
            throw new IOException("Het bronbestand " + bestand + " kan niet geopend worden", e2);
        }
    }

    public static String DomWriteXML(List<Aap1> myList, String fileName)  {
        //Opgave 1C
        try {
            org.w3c.dom.Document doc = DocumentBuilderFactory.newInstance()
                    .newDocumentBuilder().newDocument();
            org.w3c.dom.Element root = doc.createElement("apen");
            doc.appendChild(root);
            for (Aap1 aap : myList){
                org.w3c.dom.Element aapElement = doc.createElement("aap");

                aapElement.setAttribute("kooinummer", aap.getKooi());

                org.w3c.dom.Element naamElement = doc.createElement("naam");
                naamElement.setTextContent(aap.getNaam());
                aapElement.appendChild(naamElement);

                org.w3c.dom.Element soortElement = doc.createElement("soort");
                soortElement.setTextContent(aap.getSoort());
                aapElement.appendChild(soortElement);

                org.w3c.dom.Element geslachtElement = doc.createElement("geslacht");
                geslachtElement.setTextContent(aap.getGeslacht().name());
                aapElement.appendChild(geslachtElement);

                org.w3c.dom.Element gewichtElement = doc.createElement("gewicht");
                gewichtElement.setTextContent(String.valueOf(aap.getGewicht()));
                aapElement.appendChild(gewichtElement);

                root.appendChild(aapElement);
            }
            // source voor wegschrijven
            DOMSource domSource = new DOMSource(doc);
            // schrijver
            Transformer transformer = TransformerFactory.newInstance().newTransformer();
            // pretty print
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");

            StringBuilder xmlResult = null;
            transformer.transform(domSource, new StreamResult(System.out));
            return root.getBaseURI();
        } catch (ParserConfigurationException | TransformerConfigurationException e) {
            e.printStackTrace();
            return null;
        } catch (TransformerException e) {
            e.printStackTrace();
            return null;
        }
    }
}
