import apen.Aap3;
import conversie.ConversieTools3;
import java.io.IOException;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class DemoApen3 {
    public static void main(String[] args) {
        try {
            List<Aap3> myApenList = ConversieTools3.GsonReadList("09_XML_en_JSON/98_Oef_tvandeve/ApenProject_3/AlleApen.json");

            System.out.println("Alle apen per leeftijd:");
            //Todo 3B sorteer via stream op leeftijd en druk af met formaat
	          //  naam       --> leeftijd
            myApenList.stream()
                    .sorted(Comparator.comparing(Aap3::getLeeftijd))
                    .map(aap -> String.format("%-35s --> %d jaar", aap.getNaam(), aap.getLeeftijd()))
                    .forEach(System.out::println);

            //Todo 3C groepeer kleine en grote apen volgens de eerste letter van het
	        // kooinummer
            Map<Boolean, List<Aap3>> myMap = myApenList.stream()
                    .collect(Collectors.partitioningBy(a -> a.getKooi().startsWith("K")));//vul aan

            List<Aap3> kleineApen = myMap.get(true);
            List<Aap3> groteApen = myMap.get(false);


            ConversieTools3.GsonWriteList(myMap.get(true), "09_XML_en_JSON/98_Oef_tvandeve/ApenProject_3/KleineApen.json");

            List<Aap3> newList = ConversieTools3.GsonReadList("09_XML_en_JSON/98_Oef_tvandeve/ApenProject_3/KleineApen.json");
            System.out.println("\nIngelezen JSON data (alle kleine apen):");
            for (Aap3 aap : newList) {
                System.out.println(aap);
            }

        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }
}

/**
 * GEWENSTE AFDRUK:
 *
 * Alle apen per leeftijd:
 Griezel      --> 2 jaar
 Banana       --> 5 jaar
 Java         --> 5 jaar
 Bumba        --> 8 jaar
 Nikita       --> 8 jaar
 Pipi         --> 30 jaar
 Pinky        --> 33 jaar
 Pinokkio     --> 32 jaar
 Monkey       --> 33 jaar
 Grompy       --> 36 jaar
 Nancy        --> 36 jaar
 Shout        --> 38 jaar
 Louie        --> 23 jaar
 Rosa         --> 23 jaar
 Sneeuwvlokje --> 23 jaar
 Kingkong     --> 33 jaar
 Koko         --> 34 jaar
 Gust         --> 35 jaar

 Ingelezen JSON data (alle kleine apen):
 Banana          maki            halfapen          V   geboren: 2030     3,2 kg   kooi: K030
 Bumba           doodshoofdaap   kapucijnapen      V   geboren: 2007     0,7 kg   kooi: K008
 Griezel         doodshoofdaap   kapucijnapen      V   geboren: 2032     0,5 kg   kooi: K008
 Grompy          brulaap         grijpstaartapen   M   geboren: 3999     7,2 kg   kooi: K006
 Java            leeuwaap        klauwapen         M   geboren: 2030     0,4 kg   kooi: K034
 Monkey          leeuwaap        klauwapen         M   geboren: 2002     0,7 kg   kooi: K033
 Nancy           neusaap         bladapen          V   geboren: 3999    33,0 kg   kooi: K034
 Nikita          maki            halfapen          V   geboren: 2007     3,3 kg   kooi: K030
 Pinokkio        neusaap         bladapen          M   geboren: 2003    33,0 kg   kooi: K036
 Shout           brulaap         grijpstaartapen   M   geboren: 3997     6,0 kg   kooi: K006

 */
