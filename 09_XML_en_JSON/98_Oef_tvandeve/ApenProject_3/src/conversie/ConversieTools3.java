package conversie;

import apen.Aap3;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.*;
import java.util.*;

public class ConversieTools3 {

    public static List<Aap3> GsonReadList(String fileName) throws FileNotFoundException {
	    List<Aap3> result = null;
	    //Todo 3A parse de file en geef een list van Aap3 objecten terug

        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();
        try (BufferedReader reader = new BufferedReader(new FileReader(fileName))){
            Aap3[] apenArray = gson.fromJson(reader, Aap3[].class);
            result = Arrays.asList(apenArray);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static void GsonWriteList(List<Aap3> apenList, String fileName) throws FileNotFoundException {
        //Todo 3D schrijf apenList weg als JSON

        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.setPrettyPrinting().create();

        String jsonString = gson.toJson(apenList);
        System.out.println("jsonString: " + jsonString);
        try (PrintWriter jsonWriter = new PrintWriter(new FileOutputStream(new File(fileName)))){
            jsonWriter.write(jsonString);
        } catch (FileNotFoundException e){
            e.printStackTrace();
        }
    }
}
