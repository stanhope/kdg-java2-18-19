package be.kdg.json;

import be.kdg.model.Piloot1;
import be.kdg.model.Piloten1;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.*;
import java.util.List;

public class MaakJson {
    private static final String FILENAME = "C:\\Users\\gg\\Documents\\IntellijProjects\\P2W9_demo\\4_GSON\\data\\piloten.json";

    public static void main(String[] args) {
        List<Piloot1> piloten = Piloten1.getPiloten();

        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.setPrettyPrinting().create();

        String jsonString = gson.toJson(piloten);
        System.out.println("jsonString = " + jsonString);

        try (PrintWriter jsonWriter = new PrintWriter(new OutputStreamWriter(new FileOutputStream(FILENAME)))) {
            jsonWriter.write(jsonString);

            System.out.println("Json file saved");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}
