package be.kdg.json;

import be.kdg.model.Piloot1;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class ParseJson {
    private static final String FILENAME = "C:\\Users\\gg\\Documents\\IntellijProjects\\P2W9_demo\\4_GSON\\data\\piloten.json";

    public static void main(String[] args) {
        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();
        List<Piloot1> pilootList = null;

        try (BufferedReader reader = new BufferedReader(new FileReader(FILENAME))) {
            Piloot1[] pilootArray = gson.fromJson(reader, Piloot1[].class);
            pilootList = Arrays.asList(pilootArray);
            System.out.println("Overzicht piloten: \n");
            pilootList.forEach(System.out::println);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
