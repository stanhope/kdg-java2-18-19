package be.kdg.model;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

public class Piloten4 {
    private static final List<Piloot1> piloten = Arrays.asList(
            new Piloot1("Mercedes", "Hamilton", 44, LocalDate.of(2018, 10, 12)),
            new Piloot1("Mercedes", "Rosberg", 6, LocalDate.of(2017, 4, 1)),
            new Piloot1("Ferrari", "Vettel", 5, LocalDate.of(2018, 7, 27)),
            new Piloot1("Ferrari", "Räikkönen", 7, LocalDate.now()),
            new Piloot1("McLaren", "Bottas", 77, LocalDate.of(2018, 9, 5)),
            new Piloot1("McLaren", "Massa", 19, LocalDate.now()),
            new Piloot1("Red Bull", "Ricciarddo", 3, LocalDate.of(2016, 11, 30)),
            new Piloot1("Red Bull", "Kvyat", 26, LocalDate.now()));

    public static List<Piloot1> getPiloten() {
        return piloten;
    }
}
