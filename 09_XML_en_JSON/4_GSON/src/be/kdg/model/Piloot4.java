package be.kdg.model;

import com.google.gson.annotations.SerializedName;

import java.time.LocalDate;

public class Piloot4 {

    private String team;
    @SerializedName("name")
    private String naam;
    private int nummer;
    private LocalDate wedstrijdDatum;

    public Piloot4(String team, String naam, int nummer, LocalDate wedstrijdDatum) {
        this.team = team;
        this.naam = naam;
        this.nummer = nummer;
        this.wedstrijdDatum = wedstrijdDatum;
    }

    @Override
    public String toString() {
        return String.format("%-10s (%-8s) %2d  --> %s", naam, team, nummer, wedstrijdDatum);
    }
}
