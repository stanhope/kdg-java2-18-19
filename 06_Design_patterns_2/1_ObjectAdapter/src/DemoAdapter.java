import adapter.FilmVerzameling;
import adapter.FilmVerzameling2;
import adapter.MoviesAdapter;
import adapter.MoviesAdapter2;

/**
 * Demo om de werking van het adapter pattern te demonstreren.
 * Deze keer wordt het principe van "object-adapter" gebruikt (dus delegatie)
 */
public class DemoAdapter {
    public static void main(String[] args) {
        FilmVerzameling filmVerzameling = new MoviesAdapter();
        System.out.println("Test Adapter pattern:");

        System.out.printf("\"Boyhood\" werd uitgebracht in %d\n",
                filmVerzameling.zoekJaar("Boyhood"));
        System.out.printf("De regisseur van \"Pulp Fiction\" is: %s\n",
                filmVerzameling.zoekRegisseur("Pulp Fiction"));
        System.out.printf("De details van \"The Thin Blue Line\": ");
        filmVerzameling.drukFilmDetails("The Thin Blue Line");

    }
}

/*
OUTPUT:
Test Adapter pattern:
"Boyhood" werd uitgebracht in 2014
De regisseur van "Pulp Fiction" is: Quentin Tarantino
De details van "The Thin Blue Line": The Thin Blue Line (1988) by Errol Morris
 */
