package adapter;


import adaptee.Movies;

/**
 * Werk de adapter-klasse uit.
 * Gebruik "object-adapter" (dus delegatie)
 */
public class MoviesAdapter implements FilmVerzameling {

    private Movies myMovies = new Movies();

    @Override
    public void voegFilmToe(String titel, int jaar, String regisseur) {
        myMovies.addMovieData(titel, jaar, regisseur);
    }

    @Override
    public String zoekRegisseur(String titel) {
        return myMovies.searchMovieByTitle(titel).getDirector();
    }

    @Override
    public void drukFilmDetails(String titel) {
        myMovies.showMovie(myMovies.getMovieIndex(titel));
    }

    @Override
    public int zoekJaar(String titel) {
        return myMovies.searchMovieByTitle(titel).getYear();
    }
}
