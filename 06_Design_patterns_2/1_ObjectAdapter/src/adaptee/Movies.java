package adaptee;

import java.util.ArrayList;
import java.util.List;

/**
 * Adaptee-klasse: mag niet gewijzigd worden!
 */
public class Movies {
    private static List<Movie> movieList = new ArrayList<>();

    public Movies() {
        addSomeData();
    }

    public void addMovieData(String title, int year, String director) {
        if (searchMovieByTitle(title) == null) {
            movieList.add(new Movie(title, year, director));
        }
    }

    public void showMovie(int index) {
        System.out.println(movieList.get(index));
    }

    public int getMovieIndex(String title) {
        Movie movie = searchMovieByTitle(title);
        return (movie == null) ? -1 : movieList.indexOf(movie);
    }

    public Movie searchMovieByTitle(String title) {
        for (Movie movie : movieList) {
            if (movie.getTitle().equalsIgnoreCase(title)) {
                return movie;
            }
        }
        return null;
    }

    public Movie searchMovieByYear(int year) {
        for (Movie movie : movieList) {
            if (movie.getYear() == year) {
                return movie;
            }
        }
        return null;
    }

    public Movie searchMovieByDirector(String director) {
        for (Movie movie : movieList) {
            if (movie.getDirector().equalsIgnoreCase(director)) {
                return movie;
            }
        }
        return null;
    }

    private void addSomeData() {
        movieList.add(new Movie("Boyhood", 2014, "Richard Linklater"));
        movieList.add(new Movie("Pulp Fiction", 1994, "Quentin Tarantino"));
        movieList.add(new Movie("No Country For Old Men", 2007, "Joel and Ethan Coen"));
        movieList.add(new Movie("Moonrise Kingdom", 2012, "Wes Anderson"));
        movieList.add(new Movie("The Thin Blue Line", 1988, "Errol Morris"));
        movieList.add(new Movie("Once Upon a Time in the West", 1968, "Sergio Leone"));
    }
}
