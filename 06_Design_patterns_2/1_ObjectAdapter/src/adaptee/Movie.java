package adaptee;

public class Movie {
    private String title;
    private int year;
    private String director;

    //package private constructor:
    Movie(String title, int year, String director) {
        this.title = title;
        this.year = year;
        this.director = director;
    }

    public String getTitle() {
        return title;
    }

    public int getYear() {
        return year;
    }

    public String getDirector() {
        return director;
    }

    @Override
    public String toString() {
        return String.format("%s (%d) by %s",
                title, year, director);
    }
}
