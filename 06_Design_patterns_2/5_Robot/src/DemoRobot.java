import withEnum.Robot;

public class DemoRobot {
    public static void main(String[] args) {
        Robot robot = new Robot();
        robot.walk();
        robot.cook();
        robot.walk();
        robot.off();

        robot.walk();
        robot.off();
        robot.cook();
    }
}

/*
OUTPUT:
Walking...
Cooking...
Walking...
Robot2 is switched off
Walking...
Robot2 is switched off
Cannot cook at Off withStatePattern.
 */
