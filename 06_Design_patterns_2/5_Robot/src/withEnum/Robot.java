package withEnum;

public class Robot implements RoboticState {
    private enum RoboticState {OFF, COOK, ON}

    private RoboticState state;

    public Robot() {
        state = RoboticState.ON;
    }

    public void setRoboticState(RoboticState state) {
        this.state = state;
    }

    @Override
    public void walk() {
        if (state == RoboticState.OFF) {
            System.out.println("Walking...");
            this.state = RoboticState.ON;
        } else if (state == RoboticState.ON) {
            System.out.println("Walking...");
        } else { // RoboticState2.COOK
            System.out.println("Walking...");
            this.state = RoboticState.ON;
        }
    }

    @Override
    public void cook() {
        if (state == RoboticState.OFF) {
            System.out.println("Cannot cook at Off state.");
        } else if (state == RoboticState.ON) {
            System.out.println("Cooking...");
            this.state = RoboticState.COOK;
        } else { // RoboticState2.COOK
            System.out.println("Cooking...");
        }
    }

    @Override
    public void off() {
        if (state == RoboticState.OFF) {
            System.out.println("Already switched off...");
        } else if (state == RoboticState.ON) {
            System.out.println("Robot2 is switched off");
            this.state = RoboticState.OFF;
        } else { // RoboticState2.COOK
            System.out.println("Cannot switched off while cooking...");
        }
    }

    public RoboticState getState() {
        return state;
    }
}