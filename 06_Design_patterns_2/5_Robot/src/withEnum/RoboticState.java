package withEnum;

public interface RoboticState {
	void walk();
	void cook();
	void off();

}
