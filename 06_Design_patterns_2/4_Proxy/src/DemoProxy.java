import model.Product;
import model.WinkelWagentje;
import model.WinkelWagentjeImplementatie;

public class DemoProxy {
    public static void main(String[] args) {
        WinkelWagentjeImplementatie winkelWagentje = new WinkelWagentjeImplementatie();
        winkelWagentje.voegToe(new Product("Boter", 2.0));
        winkelWagentje.voegToe(new Product("Kaas", 3.0));
        winkelWagentje.voegToe(new Product("Eieren", 4.0));

        WinkelWagentje proxy = winkelWagentje.getProtectedWinkelWagentje();

        System.out.println("Voor toevoegen: " + proxy.getAantal());
        proxy.drukAf();
        System.out.println("***********************************************");
        //Probeer inhoud van winkelwagentje te wijzigen:
        try {
            proxy.voegToe(new Product("Spek", 5.0));
        } catch (UnsupportedOperationException e) {
            System.out.println(e);
        }
        System.out.println("Na toevoegen: " + proxy.getAantal());
        proxy.drukAf();
        System.out.println("***********************************************");
        /*
        UITDAGING: probeer eens zodanig te foefelen dat je het winkelwagentje TOCH kan wijzigen
        Verander de naam van het eerste product naar "Spek"
         */
        proxy.getProductList().get(0).setNaam("spek");
        System.out.println(proxy.getProductList().get(0));

    }
}

/* OUTPUT:
Voor toevoegen: 3
Boter                €   2,00
Kaas                 €   3,00
Eieren               €   4,00
***********************************************
java.lang.UnsupportedOperationException: Niet toegestaan!
Na toevoegen: 3
Boter                €   2,00
Kaas                 €   3,00
Eieren               €   4,00
***********************************************
 */
