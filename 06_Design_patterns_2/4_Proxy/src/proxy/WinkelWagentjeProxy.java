package proxy;



import model.Product;
import model.WinkelWagentje;

import java.util.List;

/**
 * We passen hier het principe toe van "Protection Proxy"
 * We gebruiken delegatie om het echte WinkelWagentjeImplementatie te benaderen
 */
public class WinkelWagentjeProxy implements WinkelWagentje {

    private WinkelWagentje echtWinkelWagentje;

    public WinkelWagentjeProxy(WinkelWagentje echtWinkelWagentje) {
        this.echtWinkelWagentje = echtWinkelWagentje;
    }

    @Override
    public void voegToe(Product boek) {
        throw new UnsupportedOperationException("Toevoegen van " + boek.toString() + " is niet toegestaan");
    }

    @Override
    public void verwijder(Product boek) {
        throw new UnsupportedOperationException("Verwijderen van " + boek.toString() + "is niet toegestaan");
    }

    @Override
    public int getAantal() {
        return echtWinkelWagentje.getAantal();
    }

    @Override
    public double getSaldo() {
        return echtWinkelWagentje.getSaldo();
    }

    @Override
    public void maakWagentjeLeeg() {
        throw new UnsupportedOperationException("Winkelwagentje leegmaken is niet toegestaan");
    }

    @Override
    public List<Product> getProductList() {
        return echtWinkelWagentje.getProductList();
    }

    @Override
    public void drukAf() {
        echtWinkelWagentje.drukAf();
    }
}
