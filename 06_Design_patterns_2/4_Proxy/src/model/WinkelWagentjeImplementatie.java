package model;

import proxy.WinkelWagentjeProxy;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class WinkelWagentjeImplementatie implements WinkelWagentje {
    private List<Product> wagentje;
    private double saldo;

    public WinkelWagentjeImplementatie() {
        wagentje = new ArrayList<Product>();
    }

    @Override
    public void voegToe(Product boek) {
        wagentje.add(boek);
        saldo += boek.getPrijs();
    }

    @Override
    public void verwijder(Product boek) {
        if (!wagentje.remove(boek)) {
            throw new IllegalArgumentException("Boek niet gevonden!");
        }
        saldo -= boek.getPrijs();
    }

    @Override
    public int getAantal() {
        return wagentje.size();
    }

    @Override
    public double getSaldo() {
        return saldo;
    }

    @Override
    public void maakWagentjeLeeg() {
        wagentje.clear();
        saldo = 0.0;
    }

    @Override
    public List<Product> getProductList() {
        return Collections.unmodifiableList(wagentje);
    }

    @Override
    public void drukAf() {
        for (Product product : wagentje) {
            System.out.println(product);
        }
    }

    public WinkelWagentje getProtectedWinkelWagentje() {
        return new WinkelWagentjeProxy(this);
    }
}

