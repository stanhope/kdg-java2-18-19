package model;

import java.util.List;

public interface WinkelWagentje {
    void voegToe(Product boek);

    void verwijder(Product boek);

    int getAantal();

    double getSaldo();

    void maakWagentjeLeeg();

    List<Product> getProductList();

    void drukAf();
}
