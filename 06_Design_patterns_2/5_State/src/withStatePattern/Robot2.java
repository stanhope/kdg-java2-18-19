package withStatePattern;

public class Robot2 implements RoboticState2 {
    private RoboticState2 state;

    public Robot2() {
        setRoboticState(new StateOn2(this));
    }

    public void setRoboticState(RoboticState2 state) {
        this.state = state;
    }

    public RoboticState2 getState() {
        return state;
    }

    @Override
    public void walk() {
        state.walk();
    }

    @Override
    public void cook() {
        state.cook();
    }

    @Override
    public void off() {
        state.off();
    }
}