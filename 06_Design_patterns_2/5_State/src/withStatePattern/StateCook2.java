package withStatePattern;

public class StateCook2 implements RoboticState2 {
    private final Robot2 robot;

    public StateCook2(Robot2 robot) {
        this.robot = robot;
    }

    @Override
    public void walk() {
        System.out.println("Walking...");
        robot.setRoboticState(new StateOn2(robot));
    }

    @Override
    public void cook() {
        System.out.println("Cooking...");
    }

    @Override
    public void off() {
        System.out.println("Cannot switched off while cooking...");
    }
}
