package withStatePattern;

public class StateOn2 implements RoboticState2 {
    private final Robot2 robot;

    public StateOn2(Robot2 robot) {
        this.robot = robot;
    }

    @Override
    public void walk() {
        System.out.println("Walking...");
    }

    @Override
    public void cook() {
        System.out.println("Cooking...");
        robot.setRoboticState(new StateCook2(robot));
    }

    @Override
    public void off() {
        robot.setRoboticState(new StateOff2(robot));
        System.out.println("Robot2 is switched off");
    }

}
