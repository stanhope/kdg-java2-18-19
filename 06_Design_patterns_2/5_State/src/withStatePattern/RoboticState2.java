package withStatePattern;

public interface RoboticState2 {
	void walk();
	void cook();
	void off();

}
