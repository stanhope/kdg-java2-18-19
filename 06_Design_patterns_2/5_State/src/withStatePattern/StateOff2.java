package withStatePattern;

public class StateOff2 implements RoboticState2{
	private final Robot2 robot;
	
	public StateOff2(Robot2 robot){
		this.robot = robot;
	}
	 
	@Override
	public void walk() {
		System.out.println("Walking...");
		robot.setRoboticState(new StateOn2(robot));
	}

	@Override
	public void cook() {
        System.out.println("Cannot cook at Off state.");
	}

	@Override
	public void off() {
		System.out.println("Already switched off...");
	}
}
