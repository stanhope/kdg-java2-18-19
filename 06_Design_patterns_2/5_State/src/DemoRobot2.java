import withStatePattern.Robot2;

public class DemoRobot2 {
    public static void main(String[] args) {
        Robot2 robot = new Robot2();
        robot.walk();
        robot.cook();
        robot.walk();
        robot.off();

        robot.walk();
        robot.off();
        robot.cook();
    }
}

/*
OUTPUT:
Walking...
Cooking...
Walking...
Robot2 is switched off
Walking...
Robot2 is switched off
Cannot cook at Off withStatePattern.
 */
