package composite;

public interface ProjectComponent {
    int hoursSpended();
}
