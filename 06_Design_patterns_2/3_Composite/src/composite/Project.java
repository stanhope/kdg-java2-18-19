package composite;

import java.util.ArrayList;
import java.util.List;

/**
 * De composite-klasse Project
 */
public class Project implements ProjectComponent {
    private List<ProjectComponent> projectChildren = new ArrayList<>();
    private String title;

    public Project(String title) {
        this.title = title;
    }

    public int hoursSpended() {
        int hours = 0;
        for (ProjectComponent projectChild : projectChildren) {
            hours += projectChild.hoursSpended();
        }
        return hours;
    }

    public void add(ProjectComponent component) {
        projectChildren.add(component);
    }

    public void remove(ProjectTask component) {
        projectChildren.remove(component);
    }


    public ProjectComponent getChild(int index) {
        return projectChildren.get(index);
    }

    public String getTitle() {
        return title;
    }

    @Override
    public String toString() {
        return String.format("%s: \n%s", title, projectChildren);
    }
}

