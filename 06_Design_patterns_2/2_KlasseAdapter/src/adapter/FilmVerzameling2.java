package adapter;

public interface FilmVerzameling2 {
    void voegFilmToe(String titel, int jaar, String regisseur);
    String zoekRegisseur(String titel);
    void drukFilmDetails(String titel);
    int zoekJaar(String titel);
}
