package adapter;


import adaptee.Movies;

/**
 * Werk de adapter-klasse uit.
 * Gebruik "klasse-adapter" (dus overerving)
 */
public class MoviesAdapter2 extends Movies implements FilmVerzameling2 {

    @Override
    public void voegFilmToe(String titel, int jaar, String regisseur) {
        super.addMovieData(titel, jaar, regisseur);
    }

    @Override
    public String zoekRegisseur(String titel) {
        return super.searchMovieByTitle(titel).getDirector();
    }

    @Override
    public void drukFilmDetails(String titel) {
        super.showMovie(super.getMovieIndex(titel));
    }

    @Override
    public int zoekJaar(String titel) {
        return super.searchMovieByTitle(titel).getYear();
    }
}
