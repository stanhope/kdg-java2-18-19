package be.kdg.state;

public class Ribbet implements State {
    private final Creature creature;

    public Ribbet(Creature creature) {
        this.creature = creature;
    }


    @Override
    public void kiss() {
        creature.setState(new Darling(creature));
    }

    @Override
    public String greet() {
        return "Ribbet!";
    }
}
