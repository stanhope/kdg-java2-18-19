package be.kdg.state;

public class Darling implements State {
    private final Creature creature;

    public Darling(Creature creature) {
        this.creature = creature;
    }

    @Override
    public void kiss() {

    }

    @Override
    public String greet() {
        return "Darling!";
    }
}
