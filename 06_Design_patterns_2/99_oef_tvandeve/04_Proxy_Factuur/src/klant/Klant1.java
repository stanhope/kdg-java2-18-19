package klant;

/**
 * HIER NIETS WIJZIGEN!
 */
public class Klant1 {
    private String naam;
    private Adres1 adres;
    private String klantID;

    public Klant1(String naam, Adres1 adres, String klantID) {
        this.naam = naam;
        this.adres = adres;
        this.klantID = klantID;
    }

    public String getNaam() {
        return naam;
    }

    public void setNaam(String naam) {
        this.naam = naam;
    }

    public Adres1 getAdres() {
        return adres;
    }

    public void setAdres(Adres1 adres) {
        this.adres = adres;
    }

    public String getKlantID() {
        return klantID;
    }

    public void setKlantID(String klantID) {
        this.klantID = klantID;
    }

    @Override
    public String toString() {
        return String.format("%s (%s)\n%s", naam, klantID, adres);
    }
}
