package facturatie;

import artikel.Artikel1;

/**
 * HIER NIETS WIJZIGEN!
 */
public class FactuurLijn1 {
    private Artikel1 artikel;
    private int aantal;

    public FactuurLijn1(Artikel1 artikel, int aantal) {
        this.artikel = artikel;
        this.aantal = aantal;
    }

    public Artikel1 getArtikel() {
        return artikel;
    }

    public void setArtikel(Artikel1 artikel) {
        this.artikel = artikel;
    }

    public int getAantal() {
        return aantal;
    }

    public void setAantal(int aantal) {
        this.aantal = aantal;
    }

    @Override
    public String toString() {
        return String.format("%-40s %5dx", artikel, aantal);
    }
}
