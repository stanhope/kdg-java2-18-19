package facturatie;

import artikel.Artikel1;
import klant.Klant1;

public class OnwijzigbareFactuur1 implements Factureerbaar1 {
    private Factuur1 factuur;

    public OnwijzigbareFactuur1(Factuur1 factuur) {
        this.factuur = factuur;
    }

    @Override
    public Klant1 getKlant() {
        return factuur.getKlant();
    }

    @Override
    public String getDatum() {
        return factuur.getDatum();
    }

    @Override
    public void setDatum(String datum) {
        throw new UnsupportedOperationException("factuur datum kan niet meer gewijzigd worden");
    }

    @Override
    public void setKlant(Klant1 klant) {
        throw new UnsupportedOperationException("klant kanniet meer gewijzigd worden voor deze factuur");
    }

    @Override
    public int getFactuurNr() {
        return factuur.getFactuurNr();
    }

    @Override
    public void voegLijnToe(Artikel1 artikel, int aantal) {
        throw new UnsupportedOperationException("Er kunnen geen verkooplijnen meer toegevoegd worden aan deze factuur");
    }

    @Override
    public void verwijderLijn(Artikel1 artikel) {
        throw new UnsupportedOperationException("Men kan geen verkooplijnen meer cverwijderen vandeze factuur");
    }

    @Override
    public double getTotaalExcl() {
        return factuur.getTotaalExcl();
    }

    @Override
    public double getBTW() {
        return factuur.getBTW();
    }

    @Override
    public double getTotaalIncl() {
        return factuur.getTotaalIncl();
    }

    @Override
    public void printFactuur() {
        factuur.printFactuur();
    }
}
