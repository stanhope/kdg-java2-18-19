package facturatie;

import artikel.Artikel1;
import klant.Klant1;

public interface Factureerbaar1 {
    Klant1 getKlant();

    String getDatum();

    void setDatum(String datum);

    void setKlant(Klant1 klant);

    int getFactuurNr();

    void voegLijnToe(Artikel1 artikel, int aantal);

    void verwijderLijn(Artikel1 artikel);

    double getTotaalExcl();

    double getBTW();

    double getTotaalIncl();

    void printFactuur();
}
