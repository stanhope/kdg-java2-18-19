package doos;


import lichaam.Cylinder;

public class DoosCylinder extends Cylinder implements Doos {
    public DoosCylinder(double hoogte, double lengte) {
        super(hoogte, lengte);
    }

    public double verpakkingsOppervlakte() {
        return (super.grondvlak() + super.grondvlak()) + (super.verticaleOmtrek() * super.getHoogte());
    }

    public double tapeLengte() {
        return super.verticaleOmtrek() * 2;
    }

    @Override
    public String toString() {
        return String.format("Cylindervormige doos:\n\tvolume: %3.2f m^3\n\tbenodigde verpakking: %3.2f m^2\n\ttapelengte: %3.2f m"
                , super.volume()
                , verpakkingsOppervlakte()
                , tapeLengte());
    }
}