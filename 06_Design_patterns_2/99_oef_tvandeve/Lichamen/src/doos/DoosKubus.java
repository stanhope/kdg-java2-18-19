package doos;

import lichaam.Kubus;

public class DoosKubus extends Kubus implements Doos{
	public DoosKubus(double hoogte){
		super(hoogte);
	}

	public double verpakkingsOppervlakte() {
		return super.getRibbe()*super.getRibbe();
	}

	public double tapeLengte() {
		return super.verticaleOmtrek()*2;
	}

	@Override
	public String toString() {
		return String.format("Kubusvormige doos:\n\tvolume: %3.2f m^3\n\tbenodigde verpakking: %3.2f m^2\n\ttapelengte: %3.2f m"
				, super.volume()
				, verpakkingsOppervlakte()
				, tapeLengte());
	}
}