package doos;

import lichaam.Balk;

public class DoosBalk extends Balk implements Doos{
	public DoosBalk(double hoogte, double lengte, double breedte){
        super(hoogte, lengte, breedte);

	}

    public double verpakkingsOppervlakte() {
        return 2*(super.grondvlak()) + 4*(super.getLengte()*super.getBreedte());
    }

    public double tapeLengte() {
        return 2*super.verticaleOmtrek();
    }

    @Override
    public String toString() {
        return String.format("Balkvormige doos:\n\tvolume: %3.2f m^3\n\tbenodigde verpakking: %3.2f m^2\n\ttapelengte: %3.2f m"
                , super.volume()
                , verpakkingsOppervlakte()
                , tapeLengte());
    }
}