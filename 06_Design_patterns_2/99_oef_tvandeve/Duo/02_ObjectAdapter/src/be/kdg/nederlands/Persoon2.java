package be.kdg.nederlands;

public interface Persoon2 {

    String getNaam();

    Adres2 getAdres();

}
