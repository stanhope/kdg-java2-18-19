package be.kdg.nederlands;

public interface Adres2 {

   String getStraat();

   int getPostCode();

   String getGemeente();

}
