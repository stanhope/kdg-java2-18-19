package be.kdg.english;

/**
 * Deze klasse is in productie; JE MAG ZE NIET WIJZIGEN!
 */
public class Person2 {
    private String name;
    private Address2 address;

    public Person2(String name, Address2 address) {
        this.name = name;
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public Address2 getAddress() {
        return address;
    }

    @Override
    public String toString() {
        return name + '\n' + address;
    }
}