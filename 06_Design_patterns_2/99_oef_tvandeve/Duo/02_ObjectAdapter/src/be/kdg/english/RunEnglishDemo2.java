package be.kdg.english;

/**
 * Dit programma maakt gebruik van de klassen Address2 en Person2.
 * Zie ook de klassen zelf en de uitvoer hieronder.
 *
 * Pas het Object-adapter pattern toe zodanig dat je bij uitvoering van
 * RunNederlandsDemo2 via de te schrijven klassen AdresAdapter2 en PersoonAdapter2 gebruik
 * maakt van de reeds bestaande klassen Address2 en Person2 en van de interfaces
 * Adres2 en Persoon2.
 */
public class RunEnglishDemo2 {
    public static void main(String[] args) {
        Address2 address = new Address2("2256 High Street", "Santa Cruz", "CA 95064", "California");
        Person2 person = new Person2("John Smith", address);

        System.out.println(person);
    }

}

/*
John Smith
2256 High Street
Santa Cruz
CA 95064
California
*/
