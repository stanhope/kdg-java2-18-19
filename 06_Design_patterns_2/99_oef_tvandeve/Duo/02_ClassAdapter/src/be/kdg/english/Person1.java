package be.kdg.english;

/**
 * Deze klasse is in productie; JE MAG ZE NIET WIJZIGEN!
 */
public class Person1 {
    private String name;
    private Address1 address;

    public Person1(String name, Address1 address) {
        this.name = name;
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public Address1 getAddress() {
        return address;
    }

    @Override
    public String toString() {
        return name + '\n' + address;
    }
}
