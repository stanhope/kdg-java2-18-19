package be.kdg.english;

/**
 * Dit programma maakt gebruik van de klassen Address1 en Person1.
 * Zie ook de klassen zelf en de uitvoer hieronder.
 *
 * Pas het Class-adapter pattern toe zodanig dat je bij uitvoering van
 * RunNederlandsDemo1 via de te schrijven klassen AdresAdapter1 en PersoonAdapter1 gebruik
 * maakt van de reeds bestaande klassen Address1 en Person1.
 */
public class RunEnglishDemo1 {
    public static void main(String[] args) {
        Address1 address = new Address1("1156 High Street", "Santa Cruz", "CA 95064", "California");
        Person1 person = new Person1("John Smith", address);

        System.out.println(person);
    }
}

/*
John Smith
1156 High Street
Santa Cruz
CA 95064
California
*/