package be.kdg.nederlands;

/**
 * Vul de klassen AdresAdapter1 en PersoonAdapter1 op de juiste manier aan zodat je
 * een uitvoer zoals hieronder bekomt. Maak gebruik van het Class-adapter pattern.
 */
public class RunNederlandsDemo1 {
    public static void main(String[] args) {
        //AdresAdapter1 adres = new AdresAdapter1("Hoogstraat 17", 2000, "Antwerpen");
        //Persoon1 persoon = new PersoonAdapter1("Jan Peeters", adres);

        //System.out.println(persoon);
    }
}

/*
Jan Peeters
Hoogstraat 17
2000 Antwerpen
*/