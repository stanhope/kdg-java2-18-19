package be.kdg.nederlands;

public interface Adres1 {

   String getStraat();

   int getPostCode();

   String getGemeente();

}
