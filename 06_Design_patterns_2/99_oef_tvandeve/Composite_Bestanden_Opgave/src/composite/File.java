package composite;

public class File implements Component{
  private final String name;
  private final long size;
  private Directory parent;

  public File(String name, long size){
    this.name = name;
    this.size = size;
    this.parent = null;
  }

  // Vanaf hier zelf uitwerken:

  @Override
  public long getSize() {
    return 0;
  }

  @Override
  public String getPath() {
    return null;
  }

  @Override
  public void setParent(Directory parent) {

  }

}
