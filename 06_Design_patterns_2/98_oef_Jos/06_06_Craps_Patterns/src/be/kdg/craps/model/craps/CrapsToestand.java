package be.kdg.craps.model.craps;

public interface CrapsToestand {

    CrapsToestand next(int dobbelWaarde);
    String getText();
}
