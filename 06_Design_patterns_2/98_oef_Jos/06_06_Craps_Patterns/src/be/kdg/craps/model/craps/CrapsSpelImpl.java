package be.kdg.craps.model.craps;

public class CrapsSpelImpl implements CrapsSpel {
    private int teGooienWaarde;
    private Dobbelsteen[] dobbelstenen;
    private CrapsToestand toestand;


    public CrapsSpelImpl() {
        this.dobbelstenen = new Dobbelsteen[2];
        this.dobbelstenen[0] = new DobbelsteenImpl();
        this.dobbelstenen[1] = new DobbelsteenImpl();
        reset();
    }

    public final void reset() {
        this.teGooienWaarde = 0;
        this.toestand = CrapsToestandFactory.getStateNietGegooid();
    }

    public CrapsToestand getToestand() {
        return toestand;
    }

    public int getTeGooienWaarde() {
        return teGooienWaarde;
    }

    public void gooi() {
        if (toestand.getText().startsWith("U hebt dit spel")){
            return;
        }
        for (int i = 0; i < dobbelstenen.length; i++) {
            dobbelstenen[i].gooi();
        }
        int totaleWaarde = 0;
        for (int i = 0; i < dobbelstenen.length; i++) {
            totaleWaarde += dobbelstenen[i].getWaarde();
        }

        toestand = toestand.next(totaleWaarde);
    }

    public int getDobbelsteen(int nummer) {
        return dobbelstenen[nummer].getWaarde();
    }

    void setDobbelsteen(int nummer, Dobbelsteen dobbelsteen) {
        this.dobbelstenen[nummer] = dobbelsteen;
    }
}
