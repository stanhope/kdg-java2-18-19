package be.kdg.craps.model.craps;

import java.util.Objects;

public class Gebruiker {
    private static Gebruiker user = new Gebruiker();
    private String naam;
    private String wachtwoord;

    private Gebruiker() {
        naam = "jos";
        wachtwoord = "abc";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Gebruiker gebruiker = (Gebruiker) o;
        return Objects.equals(naam, gebruiker.naam);
    }

    public String getNaam() {
        return naam;
    }

    public String getWachtwoord() {
        return wachtwoord;
    }

    public static Gebruiker getInstance(){
        return user;
    }

    @Override
    public int hashCode() {
        return Objects.hash(naam);
    }
}
