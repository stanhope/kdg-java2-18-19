package be.kdg.craps.model.craps;

public class StateNietGegooid implements CrapsToestand {
    @Override
    public CrapsToestand next(int dobbelWaarde) {
        if (dobbelWaarde == 2 || dobbelWaarde == 3 || dobbelWaarde == 12)
            return CrapsToestandFactory.getStateVerloren();
        if (dobbelWaarde == 7 || dobbelWaarde == 11)
            return CrapsToestandFactory.getStateGewonnen();
        StateGooiX t = CrapsToestandFactory.getStateGooiX();
        t.setX(dobbelWaarde);
        return t;
    }

    @Override
    public String getText() {
        return "Druk op 'gooi' om te beginnen";
    }
}
