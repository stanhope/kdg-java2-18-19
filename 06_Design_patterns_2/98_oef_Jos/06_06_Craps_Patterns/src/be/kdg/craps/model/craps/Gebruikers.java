package be.kdg.craps.model.craps;

import java.util.HashSet;

public class Gebruikers {
    private HashSet<Gebruiker> gebruikers;

    public Gebruikers() {
        gebruikers = new HashSet<>();
        gebruikers.add(Gebruiker.getInstance());
    }

    public boolean login(String naam, String wachtwoord){
        for (Gebruiker g :
                gebruikers) {
            if (g.getNaam().equals(naam) && g.getWachtwoord().equals(wachtwoord)) return true;
        }
        return false;
    }
}
