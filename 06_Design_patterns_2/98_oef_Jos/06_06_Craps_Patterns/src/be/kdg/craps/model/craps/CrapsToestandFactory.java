package be.kdg.craps.model.craps;

public class CrapsToestandFactory {

    private static StateGewonnen stateGewonnen;
    private static StateVerloren stateVerloren;
    private static StateNietGegooid stateNietGegooid;
    private static StateGooiX stateGooiX;

    private CrapsToestandFactory() {
    }

    public static StateGewonnen getStateGewonnen(){
        if (stateGewonnen == null) stateGewonnen = new StateGewonnen();
        return stateGewonnen;
    }

    public static StateVerloren getStateVerloren(){
        if (stateVerloren == null) stateVerloren = new StateVerloren();
        return stateVerloren;
    }

    public static StateNietGegooid getStateNietGegooid(){
        if (stateNietGegooid == null) stateNietGegooid = new StateNietGegooid();
        return stateNietGegooid;
    }

    public static StateGooiX getStateGooiX(){
        if (stateGooiX == null) stateGooiX = new StateGooiX();
        return stateGooiX;
    }
}
