package be.kdg.craps.model.craps;

public class BeveiligdCrapsSpel implements CrapsSpel {

    private CrapsSpel crapsSpel;
    private Gebruikers gebruikers;
    private boolean loginSucces;
    private String naam;


    public BeveiligdCrapsSpel(CrapsSpel crapsSpel, String naam, String wachtwoord) {
        this.crapsSpel = crapsSpel;
        gebruikers = new Gebruikers();
        loginSucces = gebruikers.login(naam,wachtwoord);
        this.naam = naam;
    }

    public String getNaam(){
        return naam;
    }

    @Override
    public void reset() {
        if (!loginSucces) throw new IllegalAccessError();
        crapsSpel.reset();
    }

    @Override
    public CrapsToestand getToestand() {
        if (!loginSucces) throw new IllegalAccessError();
        return crapsSpel.getToestand();
    }

    @Override
    public int getTeGooienWaarde() {
        if (!loginSucces) throw new IllegalAccessError();
        return crapsSpel.getTeGooienWaarde();
    }

    @Override
    public void gooi() {
        if (!loginSucces) throw new IllegalAccessError();
        crapsSpel.gooi();
    }

    @Override
    public int getDobbelsteen(int nummer) {
        if (!loginSucces) throw new IllegalAccessError();
        return crapsSpel.getDobbelsteen(nummer);
    }
}
