package be.kdg.craps.model.craps;

public class StateGooiX implements CrapsToestand {

    private int x;

    @Override
    public CrapsToestand next(int dobbelWaarde) {
        if (dobbelWaarde == 7) {
            return CrapsToestandFactory.getStateVerloren();
        }
        if (dobbelWaarde == x)
        return CrapsToestandFactory.getStateGewonnen();

        return this;
    }

    public void setX(int x){ this.x = x;}
    public int getX(){return x;}

    @Override
    public String getText() {
        return "Probeer " + x + " te gooien";
    }
}

