package be.kdg.craps.model.craps;

public class StateGewonnen implements CrapsToestand {
    @Override
    public CrapsToestand next(int dobbelWaarde) {
        return this;
    }

    @Override
    public String getText() {
        return "U hebt dit spel gewonnen!";
    }
}
