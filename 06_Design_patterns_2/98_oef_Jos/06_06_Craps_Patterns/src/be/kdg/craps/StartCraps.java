package be.kdg.craps;

import be.kdg.craps.model.craps.BeveiligdCrapsSpel;
import be.kdg.craps.model.craps.CrapsSpel;
import be.kdg.craps.model.craps.CrapsSpelImpl;
import be.kdg.craps.view.CrapsFrame;

public class StartCraps {
    public static void main(String[] args) {
        CrapsSpel crapsSpel = new CrapsSpelImpl();
        CrapsSpel beveiligdCrapsSpel = new BeveiligdCrapsSpel(crapsSpel, "jos", "abc");
//        new CrapsFrame(crapsSpel, "Tyrion Lannister");
        new CrapsFrame(beveiligdCrapsSpel, ((BeveiligdCrapsSpel) beveiligdCrapsSpel).getNaam());

    }
}
