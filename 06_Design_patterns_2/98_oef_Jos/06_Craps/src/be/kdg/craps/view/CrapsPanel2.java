package be.kdg.craps.view;

import be.kdg.craps.model.craps.CrapsSpel2;
import be.kdg.craps.model.craps.CrapsToestand2;

import javax.swing.*;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.*;

public class CrapsPanel2 extends JPanel {
    private DobbelsteenComponent2 dobbelsteen1;
    private DobbelsteenComponent2 dobbelsteen2;
    private JLabel berichtLabel;
    private JButton gooiButton;
    private CrapsSpel2 crapsSpel;

    public CrapsPanel2(CrapsSpel2 crapsSpel) {
        this.crapsSpel = crapsSpel;
        maakComponenten();
        layoutComponenten();
        voegListenersToe();
    }

    private void voegListenersToe() {
        gooiButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                gooi();
            }
        });
    }

    private void gooi() {
        dobbelsteen1.setVisible(true);
        dobbelsteen2.setVisible(true);
        crapsSpel.gooi();
        CrapsToestand2 toestand = crapsSpel.getToestand();
        toonToestand(toestand);
        toonDobbelstenen();
    }

    private void toonDobbelstenen() {
        dobbelsteen1.setWaarde(crapsSpel.getDobbelsteen(0));
        dobbelsteen2.setWaarde(crapsSpel.getDobbelsteen(1));
    }

    private void toonToestand(CrapsToestand2 toestand) {
        if (toestand == CrapsToestand2.NIET_GEGOOID) {
            berichtLabel.setText("Druk op 'gooi' om te beginnen");
        } else if (toestand == CrapsToestand2.GEWONNEN) {
            berichtLabel.setText("U hebt dit spel gewonnen!");
            gooiButton.setEnabled(false);
        } else if (toestand == CrapsToestand2.VERLOREN) {
            berichtLabel.setText("U hebt dit spel verloren...");
            gooiButton.setEnabled(false);
        } else {
            int teGooienWaarde = crapsSpel.getTeGooienWaarde();
            berichtLabel.setText("Probeer " + teGooienWaarde + " te gooien");
        }
    }

    private void layoutComponenten() {
        setLayout(new PartitionLayout2(65, PartitionLayout2.VERTICAL));
        JPanel dobbelsteenPanel = new JPanel(new GridLayout(1, 2));
        dobbelsteenPanel.add(dobbelsteen1);
        dobbelsteenPanel.add(dobbelsteen2);
        add(dobbelsteenPanel);
        JPanel onderstePanel = new JPanel(new GridLayout(2, 1));
        JPanel berichtPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
        berichtPanel.add(berichtLabel);
        onderstePanel.add(berichtPanel);
        JPanel knoppenPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
        knoppenPanel.add(gooiButton);
        onderstePanel.add(knoppenPanel);
        add(onderstePanel);
    }

    private void maakComponenten() {
        dobbelsteen1 = new DobbelsteenComponent2(1);
        dobbelsteen1.setVisible(false); //eerste keer nog niet zichtbaar
        dobbelsteen2 = new DobbelsteenComponent2(2);
        dobbelsteen2.setVisible(false); //eerste keer nog niet zichtbaar
        berichtLabel = new JLabel("Druk op 'gooi' om te beginnen");
        gooiButton = new JButton("gooi");
    }
}
