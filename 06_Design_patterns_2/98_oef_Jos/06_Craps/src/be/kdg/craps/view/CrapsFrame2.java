package be.kdg.craps.view;

import be.kdg.craps.model.craps.CrapsSpel2;

import javax.swing.*;
import java.awt.*;

public class CrapsFrame2 extends JFrame {
    private CrapsSpel2 crapsSpel;

    private JLabel welkomLabel;
    private CrapsPanel2 crapsPanel;

    public CrapsFrame2(CrapsSpel2 crapsSpel, String gebruikersnaam) {
        this.crapsSpel = crapsSpel;
        setTitle("Craps " + gebruikersnaam);

        maakComponenten(gebruikersnaam);
        layoutComponenten();
        voegListenersToe();

        pack();
        setVisible(true);
    }

    private void voegListenersToe() {
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    private void layoutComponenten() {
        Container contentPane = getContentPane();
        contentPane.setLayout(new BorderLayout());
        contentPane.add(crapsPanel, BorderLayout.CENTER);
        JPanel welkomPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
        welkomPanel.add(welkomLabel);
        contentPane.add(welkomPanel, BorderLayout.NORTH);
    }

    private void maakComponenten(String gebruikersnaam) {
        crapsPanel = new CrapsPanel2(crapsSpel);
        welkomLabel = new JLabel("Welkom " + gebruikersnaam);
    }
}
