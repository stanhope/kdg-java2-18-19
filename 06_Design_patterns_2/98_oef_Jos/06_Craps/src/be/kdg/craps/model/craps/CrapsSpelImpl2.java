package be.kdg.craps.model.craps;

public class CrapsSpelImpl2 implements CrapsSpel2 {
    private int teGooienWaarde;
    private Dobbelsteen2[] dobbelstenen;
    private CrapsToestand2 toestand;

    public CrapsSpelImpl2() {
        this.dobbelstenen = new Dobbelsteen2[2];
        this.dobbelstenen[0] = new DobbelsteenImpl2();
        this.dobbelstenen[1] = new DobbelsteenImpl2();
        reset();
    }

    public final void reset() {
        this.teGooienWaarde = 0;
        this.toestand = CrapsToestand2.NIET_GEGOOID;
    }

    public CrapsToestand2 getToestand() {
        return toestand;
    }

    public int getTeGooienWaarde() {
        return teGooienWaarde;
    }

    public void gooi() {
        if (toestand == CrapsToestand2.GEWONNEN || toestand == CrapsToestand2.VERLOREN) {
            return;
        }
        for (int i = 0; i < dobbelstenen.length; i++) {
            dobbelstenen[i].gooi();
        }
        int totaleWaarde = 0;
        for (int i = 0; i < dobbelstenen.length; i++) {
            totaleWaarde += dobbelstenen[i].getWaarde();
        }
        if (toestand == CrapsToestand2.NIET_GEGOOID) {
            if (totaleWaarde == 2 || totaleWaarde == 3 || totaleWaarde == 12) {
                toestand = CrapsToestand2.VERLOREN;
            } else if (totaleWaarde == 7 || totaleWaarde == 11) {
                toestand = CrapsToestand2.GEWONNEN;
            } else {
                teGooienWaarde = totaleWaarde;
                toestand = CrapsToestand2.GOOI_X;
            }
        } else if (toestand == CrapsToestand2.GOOI_X) {
            if (totaleWaarde == 7) {
                toestand = CrapsToestand2.VERLOREN;
            } else if (totaleWaarde == teGooienWaarde) {
                toestand = CrapsToestand2.GEWONNEN;
            }
        }
    }

    public int getDobbelsteen(int nummer) {
        return dobbelstenen[nummer].getWaarde();
    }

    void setDobbelsteen(int nummer, Dobbelsteen2 dobbelsteen) {
        this.dobbelstenen[nummer] = dobbelsteen;
    }
}
