package be.kdg.craps.model.craps;

public interface CrapsSpel2 {
    void reset();

    CrapsToestand2 getToestand();

    int getTeGooienWaarde();

    void gooi();

    int getDobbelsteen(int nummer);
}
