package be.kdg.craps.model.craps;

import java.util.Random;

public class DobbelsteenImpl2 implements Dobbelsteen2 {
    private int waarde = 0;
    private Random random = new Random();

    public void gooi() {
        this.waarde = random.nextInt(6) + 1;
    }

    public int getWaarde() {
        return waarde;
    }
}
