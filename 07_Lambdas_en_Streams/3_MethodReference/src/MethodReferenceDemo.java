import model.Student;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class MethodReferenceDemo {
    public static void main(String[] args) {
        List<Student> studentList = new ArrayList<>();

        studentList.add(new Student(9999, "Charlotte Vermeulen", LocalDate.of(2000, 1, 24), "Antwerpen"));
        studentList.add(new Student(666, "Donald Trump", LocalDate.of(1946, 6, 14), "Washington"));
        studentList.add(new Student(123, "Sam Gooris", LocalDate.of(1973, 4, 10), "Antwerpen"));
        studentList.add(new Student(333, "Koen Schram", LocalDate.of(1967, 5, 15), "Merksem"));

        System.out.println("studentList volgens naam:");
        Collections.sort(studentList, Comparator.comparing(Student::getNaam));
        studentList.forEach(System.out::println);
        System.out.println("\nstudentList volgens leeftijd (jongste eerst):");
        //met anonieme klasse
        studentList.sort(new Comparator<Student>() {
            @Override
            public int compare(Student o1, Student o2) {
                return o2.getGeboorte().compareTo(o1.getGeboorte());
            }
        });
        //met kweni wa
       studentList.sort(Comparator.comparing(Student::getGeboorte).reversed());
       studentList.forEach(System.out::println);

        System.out.println("\nstudentList volgens woonplaats en dan per studnr:");
        studentList.sort(Comparator.comparing(Student::getWoonplaats).thenComparing(Student::getStudNr));
        studentList.forEach(System.out::println);
    }

}

