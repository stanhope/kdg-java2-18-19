package model;

import java.time.LocalDate;

public class Student {
    private final int studNr;  //uniek
    private final String naam;
    private final LocalDate geboorte;
    private String woonplaats;

    public Student(int studNr, String naam, LocalDate geboorte, String woonplaats) {
        this.studNr = studNr;
        this.naam = naam;
        this.geboorte = geboorte;
        this.woonplaats = woonplaats;
    }

    public Student(int studNr, String naam, LocalDate geboorte) {
        this(studNr, naam, geboorte, "");
    }

    public int getStudNr() {
        return studNr;
    }

    public String getNaam() {
        return naam;
    }

    public LocalDate getGeboorte() {
        return geboorte;
    }

    public String getWoonplaats() {
        return woonplaats;
    }

    public void setWoonplaats(String woonplaats) {
        this.woonplaats = woonplaats;
    }

    @Override
    public String toString() {
        return String.format("%-6d %-20s (°%s) --> %s", studNr, naam, geboorte, woonplaats);
    }
}
