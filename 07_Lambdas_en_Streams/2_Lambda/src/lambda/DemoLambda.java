package lambda;

import java.util.*;

public class DemoLambda {
    public static void main(String[] args) {
        String[] stringArray = {"Charlie", "Beta", "Alfa", "Delta", "Echo", "Foxtrot"};
        List<String> myList = new ArrayList<>(Arrays.asList(stringArray));

        System.out.println("alfabetisch:");
        Collections.sort(myList);
        for (String s : myList) {
            System.out.print(s + " ");
        }

        System.out.println("\nvolgens lengte:");
        // met anonieme methode
        Collections.sort(myList, new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                return o1.length() - o2.length();
            }
        });

        //met lambda
        Collections.sort(myList, (o1, o2) -> o1.length() - o2.length());

        for (String s : myList) {
            System.out.print(s + " ");
        }

        System.out.println("\nomgekeerd alfabetisch:");
        //methode 1
        Collections.sort(myList, new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                return o2.compareTo(o1);
            }
        });

        //methode 2
        myList.sort(Comparator.reverseOrder());
        for (String s : myList) {
            System.out.print(s + " ");
        }

        System.out.println("\nvolgens laatste karakter:");
        //met anonieme methode
        myList.sort(new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                return o1.charAt(o1.length() - 1) - o2.charAt(o2.length() - 1);
            }
        });

        //met lambda
        myList.sort((o1, o2) -> o1.charAt(o1.length() - 1) - o2.charAt(o2.length() - 1));
        for (String s : myList) {
            System.out.print(s + " ");
        }

    }
}
