package predicate;

import data.Artikel2;
import data.ArtikelData2;

import java.util.List;
import java.util.function.Predicate;

public class PredicateDemo {
    public static <T> T firstMatch(List<T> candidates, Predicate<T> matchFunction) {
        for (T candidate : candidates) {
            if (matchFunction.test(candidate)) {
                return candidate;
            }
        }
        return null;
    }

    public static void main(String[] args) {
        List<Artikel2> lijst = ArtikelData2.getArtikels();

        System.out.print("\nEerste artikel met 'L': ");

        //met anonieme methode
        System.out.println(firstMatch(lijst, new Predicate<Artikel2>() {
            @Override
            public boolean test(Artikel2 artikel) {
                return artikel.getMerk().startsWith("L");
            }
        }));
        //met lambda
        System.out.println(firstMatch(lijst, artikel -> artikel.getMerk().startsWith("L")));
        System.out.print("\nEerste artikel met nr 4: ");

        System.out.println(firstMatch(lijst, artikel -> artikel.getNummer() == 4));

        System.out.print("\nEerste artikel met prijs > 500: ");

        System.out.println(firstMatch(lijst, artikel -> artikel.getPrijs() > 4));


        //een predicate is een methode die een boolean teruggeeft
        //consumers zijn methoden die een void teruggeven
    }
}
