package data;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ArtikelData {
    final static private List<Artikel2> artikels = Arrays.asList(
            new Artikel2(2, "Asus", "R556LA-XX1116H", 399),
            new Artikel2(6, "HP", "Pavilion 15-p268nb", 649),
            new Artikel2(7, "Asus", "EeeBook X205TA", 239),
            new Artikel2(1, "Lenovo", "IdeaPad G50-80", 549),
            new Artikel2(3, "Lenovo", "IdeaPad Z70-80", 499),
            new Artikel2(10, "Asus", "K555LJ-DM706T", 849),
            new Artikel2(9, "Lenovo", "IdeaPad S21e-20", 199),
            new Artikel2(5, "MSI", "GP72Qe-016BE", 1199),
            new Artikel2(4, "Toshiba", "Satelite Pro R50-B-109", 399),
            new Artikel2(8, "Toshiba", "Satelite L50D-B-1CE", 649)
     );


    public static List<Artikel2> getArtikels() {
        return new ArrayList<>(artikels);
    }
}
