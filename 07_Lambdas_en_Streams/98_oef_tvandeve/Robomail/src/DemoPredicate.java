import employee.Data;
import employee.Employee;
import employee.Gender;
import employee.Role;
import robomail.RoboMailNewStyle;
import java.util.List;

public class DemoPredicate {
    public static void main(String[] args) {
        List<Employee> employeeList = Data.getDataAsList();
        RoboMailNewStyle robo = new RoboMailNewStyle();

        System.out.println("\n==== RoboMail Tests 01 ====");
        System.out.println("\n=== Members of HR ===");
        robo.mail(employeeList, employee -> employee.getDept().equalsIgnoreCase("HR"));

        System.out.println("\n=== All Sales ===");
        robo.mail(employeeList, employee -> employee.getDept().equalsIgnoreCase("sales"));

        System.out.println("\n=== All Sales Executives ===");
        robo.mail(employeeList, employee -> employee.getRole() == Role.EXECUTIVE && employee.getDept().equalsIgnoreCase("sales"));

        System.out.println("\n=== All Sales Employees over 50 ===");
        robo.mail(employeeList, employee -> employee.getAge() >= 50 && employee.getDept().equalsIgnoreCase("sales"));

        System.out.println("\n=== All Female Employees ===");
        robo.mail(employeeList, employee -> employee.getGender() == Gender.FEMALE);
    }
}

/* Gewenste afdruk:
==== RoboMail Tests 01 ====

=== Members of HR ===
Emailing: John Doe age 28 at john.doe@example.com
Emailing: Nancy Smith age 55 at phil.smith@examp;e.com

=== All Sales ===
Emailing: Jane Doe age 25 at jane.doe@example.com
Emailing: John Adams age 52 at john.adams@example.com
Emailing: Betty Jones age 65 at betty.jones@example.com

=== All Sales Executives ===
Emailing: Betty Jones age 65 at betty.jones@example.com

=== All Sales Employees over 50 ===
Emailing: John Adams age 52 at john.adams@example.com
Emailing: Betty Jones age 65 at betty.jones@example.com

=== All Female Employees ===
Emailing: Jane Doe age 25 at jane.doe@example.com
Emailing: Nancy Smith age 55 at phil.smith@example.com
Emailing: Betty Jones age 65 at betty.jones@example.com
 */