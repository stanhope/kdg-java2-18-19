package lambda;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

public class LambdaButton extends Application {
    private Button button = new Button("Klik hier!");

    public static void main(String[] args) {
        Application.launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        primaryStage.setScene(new Scene(new BorderPane(button)));
        primaryStage.setHeight(80);
        primaryStage.show();

        //1 met een aparte klasse
        class MyButtonHandler implements EventHandler {

            @Override
            public void handle(Event event) {
                System.out.println("KLIK");
            }
        }
        button.setOnAction(new MyButtonHandler());

        //2 met anonieme klasse
        button.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                System.out.println("KLIK");
            }
        });

        //3 met lambda
        button.setOnAction(event -> System.out.println("KLIK"));
    }
}