package employee;

import java.util.Arrays;
import java.util.List;

public class Data2 {
    private static Employee2[] employeeArray = {
            new Employee2("Bob", "Baker", 23, Gender2.MALE, Role2.STAFF, "ENG", "bob.baker@example.com"),
            new Employee2("Jane", "Doe", 25, Gender2.FEMALE, Role2.STAFF, "Sales", "jane.doe@example.com"),
            new Employee2("John", "Doe", 28, Gender2.MALE, Role2.MANAGER, "HR", "john.doe@example.com"),
            new Employee2("James", "Johnson", 45, Gender2.MALE, Role2.MANAGER, "Eng", "james.johnson@example.com"),
            new Employee2("John", "Adams", 52, Gender2.MALE, Role2.MANAGER, "Sales", "john.adams@example.com"),
            new Employee2("Joe", "Bailey", 62, Gender2.MALE, Role2.EXECUTIVE, "Eng", "joebob.bailey@example.com"),
            new Employee2("Nancy", "Smith", 55, Gender2.FEMALE, Role2.EXECUTIVE, "HR", "phil.smith@example.com"),
            new Employee2("Betty", "Jones", 65, Gender2.FEMALE, Role2.EXECUTIVE, "Sales", "betty.jones@example.com")
    };

    public static List<Employee2> getDataAsList() {
        return Arrays.asList(employeeArray);
    }
}
