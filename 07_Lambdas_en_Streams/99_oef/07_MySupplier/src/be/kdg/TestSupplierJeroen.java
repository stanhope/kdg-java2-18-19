package be.kdg;

import be.kdg.supplier.MySupplierJeroen;

public class TestSupplierJeroen {
    public static void main(String[] args) {
        MySupplierJeroen supplier = new MySupplierJeroen();

        System.out.println(supplier.returnPiloot());
        // Vervang de puntjes door de juiste lambda expression
        // zodat je de verwachte afdruk bekomt.
        System.out.println(supplier.groet(() -> "Jeroen"));
    }
}
