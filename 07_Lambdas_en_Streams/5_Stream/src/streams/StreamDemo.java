package streams;

import data.Artikel2;
import data.ArtikelData2;

import java.util.Comparator;
import java.util.List;

public class StreamDemo {
    public static void main(String[] args) {
        List<Artikel2> artikels = ArtikelData2.getArtikels();

        System.out.println("De 2 goedkoopste Lenovo toestellen:");
        artikels.stream()
                /*.peek(a -> System.out.println("PEEK: " + a))*/
                .filter(a -> a.getMerk().equalsIgnoreCase("Lenovo"))
                .sorted(Comparator.comparing(Artikel2::getPrijs))
                .limit(2)
                .forEach(System.out::println);

    }
}

/*
De 2 goedkoopste Lenovo toestellen:
  9 - Lenovo  - IdeaPad S21e-20         - €199,00
  3 - Lenovo  - IdeaPad Z70-80          - €499,00
*/