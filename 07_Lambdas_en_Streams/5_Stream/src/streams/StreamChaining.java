package streams;

import data.Artikel2;
import data.ArtikelData2;

import java.util.*;
import java.util.stream.Collectors;

public class StreamChaining {
    public static void main(String[] args) {
        List<Artikel2> artikels = ArtikelData2.getArtikels();

        System.out.println("Alle merken in omgekeerde volgorde:");
        //artikels.stream().sorted((o1, o2) -> o2.getMerk().compareTo(o1.getMerk())).forEach(System.out::println);
        artikels.stream()
                .sorted(Comparator.comparing(Artikel2::getMerk)
                        .reversed())
                .map(Artikel2::getMerk)//map want anders wordt de distinct gedaan op het hele artikel, nu op merk
                .distinct()
                .forEach(System.out::println);

        System.out.println("\nDe gemiddelde prijs van de Asus toestellen:");
        double gemiddelde = artikels.stream().filter(a -> a.getMerk().equalsIgnoreCase("Asus"))
                .mapToDouble(Artikel2::getPrijs)
                .average()
                .getAsDouble();

        System.out.printf("%.2f€\n", gemiddelde);

        System.out.println("\nEen List met de 5 duurste toestellen:");
        List<Artikel2> myList = artikels.stream()
                .sorted(Comparator.comparing(Artikel2::getPrijs)
                        .reversed())
                .limit(5)
                .collect(Collectors.toList());
        myList.forEach(a -> System.out.println(a.getMerk() + "(" + a.getPrijs() + ")"));

        System.out.println("\nEen map met alle artikels alfabetisch per merk en daarin per nummer:");
        Map<String, List<Artikel2>> anotherMap = new TreeMap<>(
                artikels.stream()
                .sorted(Comparator.comparing(Artikel2::getMerk)
                        .thenComparing(Artikel2::getNummer))
                .collect(Collectors.groupingBy(Artikel2::getMerk)));

        for (String s : anotherMap.keySet()) {
            System.out.println(s);
            anotherMap.get(s).forEach(a -> System.out.println("\t" + a));
        }
    }
}

/*
Alle merken in omgekeerde volgorde:
Toshiba
HP
Asus

De gemiddelde prijs van de Asus toestellen:
495,67€

Een List met de 5 duurste toestellen:
MSI(1199.0)
Asus(849.0)
HP(649.0)
Toshiba(649.0)
Lenovo(549.0)

Een map met alle artikels alfabetisch per merk en daarin per nummer:
Asus
	  2 - Asus    - R556LA-XX1116H          - €399,00
	  7 - Asus    - EeeBook X205TA          - €239,00
	 10 - Asus    - K555LJ-DM706T           - €849,00
HP
	  6 - HP      - Pavilion 15-p268nb      - €649,00
Lenovo
	  1 - Lenovo  - IdeaPad G50-80          - €549,00
	  3 - Lenovo  - IdeaPad Z70-80          - €499,00
	  9 - Lenovo  - IdeaPad S21e-20         - €199,00
MSI
	  5 - MSI     - GP72Qe-016BE            - €1199,00
Toshiba
	  4 - Toshiba - Satelite Pro R50-B-109  - €399,00
	  8 - Toshiba - Satelite L50D-B-1CE     - €649,00
 */