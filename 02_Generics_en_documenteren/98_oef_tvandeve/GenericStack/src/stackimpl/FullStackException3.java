package stackimpl;

/**
 * Hier moet niets gewijzigd worden
 */
public class FullStackException3 extends RuntimeException {

    public FullStackException3() {
        this("Stack is full");
    }

    public FullStackException3(String message) {
        super(message);
    }
}

