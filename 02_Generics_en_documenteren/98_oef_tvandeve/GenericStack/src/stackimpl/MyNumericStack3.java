package stackimpl;

public class MyNumericStack3<T extends Number> {
        private LimitedStack3<T> limitedStack;

    public MyNumericStack3() {
        this.limitedStack = new LimitedStack3<>();
    }

    public MyNumericStack3(int size) {
        this.limitedStack = new LimitedStack3<>(size);
    }

    public void push(T nieuw) {
    limitedStack.push(nieuw);
    }

    public T pop() {
       return limitedStack.pop();
    }

    public T top() {
       return limitedStack.top();
    }

    public int capacity() {
        return limitedStack.capacity();
    }

    public int size() {
        return limitedStack.size();
    }

    @Override
    public String toString() {
        return limitedStack.toString();
    }

}
