package stackimpl;

/**
 * Hier moet niets gewijzigd worden
 */
public class EmptyStackException3 extends RuntimeException {
    public EmptyStackException3() {
        this("Stack is full");
    }

    public EmptyStackException3(String message) {
        super(message);
    }
}
