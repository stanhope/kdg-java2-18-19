package stackimpl;

import java.util.ArrayList;
import java.util.List;

/**
 * Maak de klasse generiek, zodat ze kan gebruikt worden voor elk type.
 * Werk de methoden push, pop en top uit.
 */
public class LimitedStack3<T> {
    private final int capacity;
    private int count;
    private List<T> elements;

    public LimitedStack3() {
        this(10); // default stack size
    }

    public LimitedStack3(int size) {
        this.capacity = size > 0 ? size : 10;
        count = 0; // stack is aanvankelijk leeg
        elements = new ArrayList<>(size);
    }

    /**
     * Maak de methode generic. Voeg het element toe aan de stack, maar test eerst op
     * de capaciteit en doe een eventuele throw van een FullStackException3.
     */
    public void push(T nieuw) {
        if (size() < capacity()){
            elements.add(nieuw);
            count++;
        } else {
            throw new FullStackException3(String.format("Stack is full; can not push %s", nieuw));
        }
    }

    /**
     * Maak de methode generic. Neem het meest recente element weg, maar test eerst op
     * het aantal en doe een eventuele throw van een EmptyStackException3.
     */
    public T pop() {
        T popElement = null;
        if (size() == 0) {
            throw new EmptyStackException3();
        } else {
            popElement = elements.get(size() - 1);
            elements.remove(size() - 1);
            count--;
            return popElement;
        }
    }

    /**
     * Maak de methode generic. Retourneer het meest recente element, maar test eerst op
     * het aantal en doe een eventuele throw van een EmptyStackException3.
     */
    public T top() {
        if (size() == 0){
            throw new EmptyStackException3();
        } else {
            return elements.get(size() - 1);
        }
    }

    public int capacity() {
        return capacity;
    }

    public int size() {
        return count;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("[");
        for (int i = 0; i < capacity; i++) {
            if (i < count) {
                sb.append(elements.get(i).toString());
            } else {
                sb.append("?");
            }
            if (i < capacity - 1) {
                sb.append(", ");
            } else {
                sb.append("]");
            }
        }
        return sb.toString();
    }
}
