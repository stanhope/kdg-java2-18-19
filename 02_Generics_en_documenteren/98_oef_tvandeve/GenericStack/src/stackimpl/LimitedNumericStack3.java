package stackimpl;

public class LimitedNumericStack3<T extends Number> extends LimitedStack3<T> {
    public LimitedNumericStack3() {
    }

    public LimitedNumericStack3(int size) {
        super(size);
    }

    @Override
    public void push(T nieuw) {
        super.push(nieuw);
    }

    @Override
    public T pop() {
        return super.pop();
    }

    @Override
    public T top() {
        return super.top();
    }
}
