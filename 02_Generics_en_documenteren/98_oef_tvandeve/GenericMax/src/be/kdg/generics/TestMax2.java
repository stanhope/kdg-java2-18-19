package be.kdg.generics;

import java.util.*;

/*
Dit testprogramma runt, maar alleen voor het type List<String>.
Maak de methode max in MaxUtil2 generiek zodat wanneer je de commentaartekens
verwijdert je de uitvoer bekomt zoals onderaan.
 */

public class TestMax2 {
    public static void main(String[] args) {
        List<String> arrayList = Arrays.asList("peer", "appel", "banaan", "sinaasappel", "citroen");
        String maxString = MaxUtil2.max(arrayList);
        System.out.println(maxString);

        List<Long> linkedList = new LinkedList<>();
        linkedList.add(16L);
        linkedList.add(32L);
        linkedList.add(24L);
        long maxLong = MaxUtil2.max(linkedList);
        System.out.println(maxLong);

        Set<Character> hashSet = new HashSet<>();
        hashSet.add('r');
        hashSet.add('s');
        hashSet.add('i');
        char maxCharacter = MaxUtil2.max(hashSet);
        System.out.println(maxCharacter);

        Set<Dier2> dierSet = new HashSet<>();
        dierSet.add(new Dier2("aap"));
        dierSet.add(new Dier2("tijger"));
        dierSet.add(new Dier2("leeuw"));
        Dier2 maxDier = MaxUtil2.max(dierSet);
        System.out.println(maxDier);

        Set<Integer> intSet = new TreeSet<>();
        intSet.add(5);
        intSet.add(8);
        intSet.add(3);
        int maxInt = MaxUtil2.max(intSet);
        System.out.println(maxInt);

    }

}

/*
sinaasappel
32
s
tijger
8
 */