package be.kdg.generics;

public class Dier2 implements Comparable<Dier2> {
    private String naam;

    public Dier2(String naam) {
        this.naam = naam;
    }

    @Override
    public String toString() {
        return naam;
    }

    @Override
    public int compareTo(Dier2 ander) {
        return naam.compareTo(ander.naam);
    }
}
