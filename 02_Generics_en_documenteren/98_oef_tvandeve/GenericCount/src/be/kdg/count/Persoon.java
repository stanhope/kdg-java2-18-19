package be.kdg.count;

import java.util.Collection;

public class Persoon<T> {
    private String naam;
    private int leeftijd;

    public Persoon(String naam, int leeftijd) {
        this.naam = naam;
        this.leeftijd = leeftijd;
    }

    public static<T> int telFrequentie(Collection<T> collection, T teTellenElement) {
        int frequentie = 0;
        for (T item : collection){
            if (item.toString().contains(teTellenElement.toString())){
                frequentie++;
            }
        }
        return frequentie;
    }

    @Override
    public String toString() {
        return String.format("%s %d", naam, leeftijd);
    }
}
