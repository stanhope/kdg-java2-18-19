package be.kdg.generics;

import java.util.Collection;

/**
 * Vul hier eerst de methode aan zodat alle elementen uit de collection na elkaar gescheiden door
 * de separator in de string komen. Tip: Je kunt een for each lus gebruiken.
 */
public class MyUtil <T> {
    public static<T> String join(Collection<T> collection, String seperator) {
        String string = "";
        for (T item : collection){
            string += item.toString();
            string += seperator;
            string += " ";
        }

        return string;
    }
}
