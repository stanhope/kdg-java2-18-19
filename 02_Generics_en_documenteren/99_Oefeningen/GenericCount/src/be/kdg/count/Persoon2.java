package be.kdg.count;

public class Persoon2 {
    private String naam;
    private int leeftijd;

    public Persoon2(String naam, int leeftijd) {
        this.naam = naam;
        this.leeftijd = leeftijd;
    }

    @Override
    public String toString() {
        return naam + " " + leeftijd;
    }
}
