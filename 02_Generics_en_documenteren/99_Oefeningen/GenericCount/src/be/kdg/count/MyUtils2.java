package be.kdg.count;

import java.util.Collection;

// Op dit moment zijn beide methoden alleen geldig voor het type String
// en moeten ze beiden nog aangevuld worden met het eigenlijke tellen.

public class MyUtils2<T> {
    public static  <T> int telFrequentie(T[] reeks, T teTellenElement) {
        int frequentie = 0;
        for (int i = 0; i < reeks.length ; i++) {
            if (teTellenElement.equals(reeks[i]))
            {
                frequentie++;
            }
        }
        return frequentie;
    }

    public static <T> int telFrequentie(Collection<T> collection, T teTellenElement) {
        int frequentie = 0;

            for (T s: collection) {
                if (teTellenElement.toString().equals(s.toString()))
                {
                    frequentie++;
                }
            }


        return frequentie;
    }
}
