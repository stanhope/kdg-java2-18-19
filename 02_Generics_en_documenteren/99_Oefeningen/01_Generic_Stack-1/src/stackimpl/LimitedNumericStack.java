package stackimpl;

public class LimitedNumericStack<N extends Number> extends LimitedStack {

    public LimitedNumericStack(int size) {
        super(size);
    }
    public LimitedNumericStack() {
    }

    public void push(N nieuw) {
        super.push(nieuw);
    }

    public N pop() {
        return (N) super.pop();
    }

    public N top() {
        return (N) super.top();
    }
}
