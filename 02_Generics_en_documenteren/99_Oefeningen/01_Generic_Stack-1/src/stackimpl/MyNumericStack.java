package stackimpl;

public class MyNumericStack<N extends Number> {

    private LimitedStack<N> myStack;

    public MyNumericStack(int size) {
        myStack = new LimitedStack<>(size);
    }

    public MyNumericStack() {
        myStack = new LimitedStack<>();
    }

    public void push(N nieuw) {
        myStack.push(nieuw);
    }

    public N pop() {
        return (N) myStack.pop();
    }

    public N top() {
        return (N) myStack.top();
    }

    public int capacity() {
        return myStack.capacity();
    }

    public int size() {
        return myStack.size();
    }

    @Override
    public String toString() {
        return myStack.toString();
    }
}
