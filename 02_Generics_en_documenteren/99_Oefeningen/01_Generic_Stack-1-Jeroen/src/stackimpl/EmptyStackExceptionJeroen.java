package stackimpl;

/**
 * Hier moet niets gewijzigd worden
 */
public class EmptyStackExceptionJeroen extends RuntimeException {
    public EmptyStackExceptionJeroen() {
        this("Stack is full");
    }

    public EmptyStackExceptionJeroen(String message) {
        super(message);
    }
}
