package stackimpl;

public class MyNumericStackJeroen<K extends Number> {
    LimitedNumericStackJeroen<K> myStack;

    public MyNumericStackJeroen() {
        myStack = new LimitedNumericStackJeroen();
    }

    public void push(K nieuw) {
        myStack.push(nieuw);
    }

    public K pop() {
        return myStack.pop();
    }

    public K top() {
        return myStack.top();
    }

    public int capacity() {
        return myStack.capacity();
    }

    public int size() {
        return myStack.size();
    }

    @Override
    public String toString() {
        return myStack.toString();
    }
}
