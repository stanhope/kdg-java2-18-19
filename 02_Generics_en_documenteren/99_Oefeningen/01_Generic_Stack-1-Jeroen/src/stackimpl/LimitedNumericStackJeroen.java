package stackimpl;

public class LimitedNumericStackJeroen<N extends Number> extends LimitedStackJeroen<N> {
    public LimitedNumericStackJeroen() {
    }

    public LimitedNumericStackJeroen(int size) {
        super(size);
    }

    @Override
    public void push(N nieuw) {
        super.push(nieuw);
    }

    @Override
    public N pop() {
        return super.pop();
    }

    @Override
    public N top() {
        return super.top();
    }
}
