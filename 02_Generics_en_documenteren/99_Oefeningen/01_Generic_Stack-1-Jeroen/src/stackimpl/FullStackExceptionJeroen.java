package stackimpl;

/**
 * Hier moet niets gewijzigd worden
 */
public class FullStackExceptionJeroen extends RuntimeException {

    public FullStackExceptionJeroen() {
        this("Stack is full");
    }

    public FullStackExceptionJeroen(String message) {
        super(message);
    }
}

