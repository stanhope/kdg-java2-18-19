package be.kdg.racelambda;

import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

public class StartRace {
    private static final Random random = new Random();

    public static void main(String[] args) {
        Runnable myRunnable = () -> {
            ThreadLocalRandom random = ThreadLocalRandom.current();
            String naam = Thread.currentThread().getName();

            System.out.println(naam + " START");
            for (int i = 0; i < 10; i++) {
                System.out.println(naam + " ronde " + (i + 1));
                try {
                    Thread.sleep(random.nextInt(1000));
                } catch (InterruptedException e) {
                    // negeer
                }
            }
            System.out.println(naam + " AANGEKOMEN");
        };

        Thread racerEen = new Thread(myRunnable, "Peter");
        Thread racerTwee = new Thread(myRunnable, "Julie");

        System.out.println("De deelnemers staan klaar");
        racerEen.start();
        racerTwee.start();
        System.out.println("De race is begonnen");
    }
}
