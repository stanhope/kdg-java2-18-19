package be.kdg.model;

import java.util.HashSet;
import java.util.Set;

public class Movie {
    private String title ;
    private int releaseYear ;
    private Set<Actor> actors = new HashSet<>() ;
    
    public Movie(String title, int releaseYear) {
        this.title = title;
        this.releaseYear = releaseYear;
    }
    
    public String getTitle() {
        return this.title ;
    }
    
    public int getReleaseYear() {
        return this.releaseYear ;
    }
    
    public void addActor(Actor actor) {
        this.actors.add(actor) ;
    }
    
    public Set<Actor> getActors() {
        return this.actors ;
    }
    
    @Override
    public String toString() {
        return "Movie{" + "Title=" + title + ", ReleaseYear=" + releaseYear + ", Actors=" + actors + '}';
    }
}
