package reflection;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

public class Reflector {

    public static void introspection(Class aClass) {
        System.out.println("Naam van de klasse: ");
        System.out.println(aClass.getName());
        System.out.println(aClass.getSimpleName());

        System.out.println("\nMethods:");
        for (Method method : aClass.getMethods()) {
            System.out.print(method.getName() + " ");
        }

        System.out.println("\nFields:");
        for (Field field : aClass.getDeclaredFields()) {
            System.out.print(field.getName() + " ");
        }

        System.out.println();
    }

    public static void modification(Class aClass) {
        Object obj = null;

        try {
            obj = aClass.newInstance();

            System.out.println("Voor modification: " + obj);
            System.out.println("Alle getters:");

            for (Method method : aClass.getDeclaredMethods()) {
                if (method.getName().startsWith("get")) {
                    Object result = method.invoke(obj);
                    System.out.println(method.getName() + "geeft: " + result);
                }
            }

            for (Field field : aClass.getDeclaredFields()) {
                field.setAccessible(true);
                if (field.getType().getSimpleName().equals("String")) {
                    field.set(obj, "bla");
                } else if (field.getType().getSimpleName().equals("int")) {
                    field.set(obj, 666);
                } else {
                    field.set(obj, null);
                }
            }

            System.out.println("Na modification: " + obj);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void annotation(Class aClass) {
        System.out.println("Annotated methods:");
        for (Method method : aClass.getDeclaredMethods()) {
            System.out.printf("\t%s (returns: %s, parameters: %d)\n", method.getName(), method.getReturnType(), method.getParameterCount());
        }
    }
}
