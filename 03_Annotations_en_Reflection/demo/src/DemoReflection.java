import model.Student;
import reflection.Reflector;

public class DemoReflection {
    public static void main(String[] args) throws ClassNotFoundException {

        Reflector.introspection(Student.class);
/*
        Reflector.introspection(Class.forName("Student"));
*/

        System.out.println("**************************************************************************");
        Reflector.introspection(Integer.class);
        System.out.println("**************************************************************************");
        Reflector.modification(Student.class);
        System.out.println("**************************************************************************");
        Reflector.annotation(Student.class);

    }
}
