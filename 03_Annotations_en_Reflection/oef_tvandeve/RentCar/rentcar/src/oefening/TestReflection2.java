package oefening;

import oefening.reflection.RentCar2;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class TestReflection2 {
    public static void main(String[] args) {
        Class<RentCar2> rental = RentCar2.class;

        // 1) Naam van de volledige package:
        System.out.printf("Package naam: %s\n", rental.getPackage());

        // 2)Afdrukken van de header van de constructor die een int als parameter heeft:
        for (Constructor constructor : rental.getConstructors()) {
            if (constructor.getParameterCount() == 1 && constructor.getParameterTypes()[0].getSimpleName().equals("int")) {
                System.out.printf("Constructor: %s\n", constructor);
            }
        }

        // 3) afdrukken van alle methoden (ook die van de superklasse):
        System.out.print("Methoden: ");
        for (Method method : rental.getMethods()){
            System.out.printf("%s ", method.getName());
        }
        System.out.println();

        // 4) Afdrukken van alle attributen van de klasse RentCar2:
        System.out.print("Attributen: ");
        for (Field field : rental.getDeclaredFields()){
            System.out.printf("%s ", field.getName());
        }
        System.out.println();
        System.out.println();

        // 5) Een constructor object maken en via dit object 'car'
        // en haal je de methode 'computerRetalCost' op en voer je ze uit:
        try {
            Object car = rental.newInstance();
            for (Method method : rental.getDeclaredMethods()){
                if (method.getName().contains("computeRentalCost")){
                    method.invoke(car, 4);
                }
            }

            // 6) Het attribuut type van het car-object naar het type 'extra large' veranderen
            String type = null;
            for (Method method : rental.getDeclaredMethods()){
                if (method.getName().contains("getType")){
                    type = (String) method.invoke(car);
                    if (!type.equals("extra large")){
                        for (Method setter : rental.getDeclaredMethods()){
                            if (setter.getName().contains("setType")){
                                setter.invoke(car, "extra large");
                            }
                        }
                        System.out.printf("Waarde van type: %s", method.invoke(car));

                    }
                }
            }
        } catch (IllegalAccessException | InstantiationException | InvocationTargetException e) {
            e.printStackTrace();
        }



    }
}

/*
    Package naam: package oefening.reflection
    Constructor: public oefening.reflection.RentCar2(int)
    Methoden: getType setRate setType getRate computeRentalCost wait wait wait equals toString hashCode getClass notify notifyAll
    Attributen: rate type price

    The cost of your rental car is €180
    Waarde van type: extra large
*/
