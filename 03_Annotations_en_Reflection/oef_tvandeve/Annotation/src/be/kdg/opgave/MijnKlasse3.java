package be.kdg.opgave;

@Wat3(omschrijving = "Een annotatie testklasse")
@MijnAnnotatie3(string = "be.kdg.opgave.Demo3", waarde = 99)
public class MijnKlasse3 {
    @Wat3(omschrijving = "Een annotation testmethode")
    @MijnAnnotatie3(string = "Test", waarde = 100)
    public void demoMethode() {
    }
}