package be.kdg.opgave;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;

public class Demo3 {
    public void demoMethode()  {

        MijnKlasse3 mijnKlasse = new MijnKlasse3();
        //TODO Toon alle annotaties voor de klasse be.kdg.opgave.MijnKlasse3.
//        toonClassAnnotations(mijnKlasse);
        System.out.println("Alle annotaties voor MijnKlasse3");
        for (Annotation annotation : MijnKlasse3.class.getDeclaredAnnotations()) {
            System.out.println(annotation.toString());

        }
        //TODO Toon de waarden van de Class-annotaties
//        toonWaardenClassAnnotation(mijnKlasse);
        System.out.println("Alle parameters:");
        Wat3 wat = MijnKlasse3.class.getAnnotation(Wat3.class);
        System.out.println(wat.omschrijving());
        MijnAnnotatie3 annotatie = MijnKlasse3.class.getAnnotation(MijnAnnotatie3.class);
        System.out.println(annotatie.string() + " " + annotatie.waarde());

        //TODO Toon alle annotaties voor demoMethode uit be.kdg.opgave.MijnKlasse3 .
//        toonMethodeAnnotations(mijnKlasse);
        System.out.println("Alle annotaties voor demoMethode:");
        for (Method method : MijnKlasse3.class.getDeclaredMethods()){
            if (method.getName().contains("demoMethode")){
                for (Annotation annotation : method.getDeclaredAnnotations()){
                    System.out.println(annotation.toString());
                }
            }
        }
        //TODO Toon alle waarden van de Methode-annotaties uit be.kdg.opgave.MijnKlasse3
//        toonWaardenMethodeAnnotation(mijnKlasse);
        System.out.println("Alle parameters:");
        for (Method method : MijnKlasse3.class.getDeclaredMethods()){
            if (method.getName().contains("demoMethode")){
                Wat3 watAnnotatie = method.getAnnotation(Wat3.class);
                System.out.println(watAnnotatie.omschrijving());
                MijnAnnotatie3 demoAnnotatie = method.getAnnotation(MijnAnnotatie3.class);
                System.out.println(demoAnnotatie.string() + " " + demoAnnotatie.waarde());

            }
        }
    }
}