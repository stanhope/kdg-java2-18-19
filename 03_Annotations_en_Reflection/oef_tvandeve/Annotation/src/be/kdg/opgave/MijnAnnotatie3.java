package be.kdg.opgave;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
@interface MijnAnnotatie3 {
    String string();

    int waarde();
}

@Retention(RetentionPolicy.RUNTIME)
@interface Wat3 {
    String omschrijving();
}