import reflection.Boek4;
import reflection.ReflectionRunner4;

// HIER MAG JE NIETS WIJZIGEN!

public class TestReflection4 {
    public static void main(String[] args) {
        ReflectionRunner4.reflect(Boek4.class);
    }
}

// Verwachte afdruk:
/*
Voor reflection: Titel: "Onbekend" (Anoniem) €0,00
equals toString hashCode getAuteur setAuteur getPrijs getTitel
Na reflection: Titel: "Onbekend" (Jan de Rijke) €0,00
*/