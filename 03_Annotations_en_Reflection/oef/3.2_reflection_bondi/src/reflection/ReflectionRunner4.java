package reflection;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class ReflectionRunner4 {
    public static void reflect(Class myClass) {
        try {
// Vervang de null door het nodige.
            //- Maak met de variabele object een instantie van de klasse die binnenkomt. Druk het object af.
            Object object = myClass.newInstance();
            System.out.println("Afdruk van het object:\n"+object);

            //- Maak met de variabele object een instantie van de klasse die binnenkomt. Druk het object af.
            System.out.println("Methodes:");
            for (Method method : myClass.getMethods()) {
                System.out.println("\t-"+method.getName());
            }

            //- De methode waarboven de annotation @Changeable4 staat die voer je uit met het object. Geef als
            //parameter je voor- en achternaam mee (bijvoorbeeld: “Jan de Rijke”).
            for (Method method : myClass.getMethods()) {
                if (method.isAnnotationPresent(Changeable4.class)){
                    method.invoke(object,"Jeroen Verstraete");
                }
            }

            //- Druk tenslotte het object opnieuw af.
            System.out.println("Nieuwe afdruk van het object:");
            System.out.println(object);
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        //  Vul hier verder aan!
    }
}

