package be.kdg.opgave;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;

public class Demo {
    public void demoMethode() {

        MijnKlasse mijnKlasse = new MijnKlasse();
        // Toon alle annotaties voor de klasse be.kdg.opgave.MijnKlasse.
        toonClassAnnotations(mijnKlasse);
        // Toon de waarden van de Class-annotaties
        toonWaardenClassAnnotation(mijnKlasse);
        // Toon alle annotaties voor demoMethode uit be.kdg.opgave.MijnKlasse .
        toonMethodeAnnotations(mijnKlasse);
        // Toon alle waarden van de Methode-annotaties uit be.kdg.opgave.MijnKlasse
        toonWaardenMethodeAnnotation(mijnKlasse);
    }

    public static void toonClassAnnotations(MijnKlasse mijnKlasse) {
        System.out.println("Alle annotaties voor MijnKlasse:");
        for (Annotation declaredAnnotation : mijnKlasse.getClass().getDeclaredAnnotations()) {
            System.out.println(declaredAnnotation.toString());
        }
    }

    public static void toonWaardenClassAnnotation(MijnKlasse mijnKlasse) {
        System.out.println("Alle parameters: ");
        for (Annotation declaredAnnotation : mijnKlasse.getClass().getDeclaredAnnotations()) {
            try {
                MijnAnnotatie mijnAnnotatie = (MijnAnnotatie) declaredAnnotation;
                System.out.println(mijnAnnotatie.string() + " " + mijnAnnotatie.waarde());
            } catch (Exception e) {/*ignore*/}
            try {
                Wat wat = (Wat) declaredAnnotation;
                System.out.println(wat.omschrijving());
            } catch (Exception e) {/*ignore*/}
        }
    }

    public static void toonMethodeAnnotations(MijnKlasse mijnKlasse) {
        for (Method declaredMethod : mijnKlasse.getClass().getDeclaredMethods()) {
            System.out.println("Alle annotaties voor " + declaredMethod.getName());
            for (Annotation declaredAnnotation : declaredMethod.getDeclaredAnnotations()) {
                System.out.println(declaredAnnotation.toString());
            }
        }
    }

    public static void toonWaardenMethodeAnnotation(MijnKlasse mijnKlasse) {
        for (Method declaredMethod : mijnKlasse.getClass().getDeclaredMethods()) {
            System.out.println("Alle parameters: ");
            for (Annotation declaredAnnotation : declaredMethod.getDeclaredAnnotations()) {
                try {
                    MijnAnnotatie mijnAnnotatie = (MijnAnnotatie) declaredAnnotation;
                    System.out.println(mijnAnnotatie.string() + " " + mijnAnnotatie.waarde());
                } catch (Exception e) {/*ignore*/}
                try {
                    Wat wat = (Wat) declaredAnnotation;
                    System.out.println(wat.omschrijving());
                } catch (Exception e) {/*ignore*/}
            }
        }
    }
}