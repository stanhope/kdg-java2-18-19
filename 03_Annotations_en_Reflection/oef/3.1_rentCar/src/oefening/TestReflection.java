package oefening;

import oefening.reflection.RentCar;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;

public class TestReflection {
    public static void main(String[] args) {
        Class<RentCar> rental = RentCar.class;

        System.out.println(RentCar.class.getName());
        System.out.println(RentCar.class.getConstructors()[1]);
        Arrays.stream(RentCar.class.getMethods()).forEach(m -> System.out.print(m.getName() + " "));
        System.out.println();
        Arrays.stream(RentCar.class.getDeclaredFields()).forEach(f -> System.out.print(f.getName() + " "));
//        for (Field f :
//                RentCar.class.getDeclaredFields()) {
//            System.out.print(f.getName() + " ");
//        }
        System.out.println();
        System.out.println();
        try {
            RentCar car = RentCar.class.newInstance();
            car.computeRentalCost(4);
            Field rateField = car.getClass().getDeclaredField("type");
                    rateField.setAccessible(true);
                    rateField.set(car,"extra large");
            System.out.println(car.getType());
        } catch (InstantiationException | IllegalAccessException | NoSuchFieldException e) {
            e.printStackTrace();
        }

        System.out.println("\nExtra:");
        Arrays.stream(RentCar.class.getDeclaredMethods()).forEach(m -> System.out.print(m.getName() + " "));

        try {
            Constructor c = RentCar.class.getConstructor(int.class);
            RentCar car2 = (RentCar) c.newInstance(500);
            System.out.println();
            car2.computeRentalCost(4);

        } catch (InstantiationException | IllegalAccessException | NoSuchMethodException | InvocationTargetException e) {
            e.printStackTrace();
        }
    }
}

/*
    Package naam: package oefening.reflection
    Constructor: public oefening.reflection.RentCar(int)
    Methoden: getType setRate setType getRate computeRentalCost wait wait wait equals toString hashCode getClass notify notifyAll
    Attributen: rate type price

    The cost of your rental car is €180
    Waarde van type: extra large
*/
