package be.kdg.opgave;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
@interface MijnAnnotatieJos {
    String string();

    int waarde();
}

@Retention(RetentionPolicy.RUNTIME)
@interface WatJos {
    String omschrijving();
}