package be.kdg.opgave;

@WatJos(omschrijving = "Een annotatie testklasse")
@MijnAnnotatieJos(string = "be.kdg.opgave.DemoJos", waarde = 99)
public class MijnKlasseJos {
    @WatJos(omschrijving = "Een annotation testmethode")
    @MijnAnnotatieJos(string = "Test", waarde = 100)
    public void demoMethode() {
    }
}