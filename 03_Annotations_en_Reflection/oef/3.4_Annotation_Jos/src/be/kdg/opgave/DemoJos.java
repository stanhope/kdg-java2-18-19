package be.kdg.opgave;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;

public class DemoJos {
    public void demoMethode() {

        MijnKlasseJos mijnKlasse = new MijnKlasseJos();
        // Toon alle annotaties voor de klasse be.kdg.opgave.MijnKlasseJos.
        toonClassAnnotations(mijnKlasse);
        // Toon de waarden van de Class-annotaties
        toonWaardenClassAnnotation(mijnKlasse);
        // Toon alle annotaties voor demoMethode uit be.kdg.opgave.MijnKlasseJos .
        toonMethodeAnnotations(mijnKlasse);
        // Toon alle waarden van de Methode-annotaties uit be.kdg.opgave.MijnKlasseJos
        toonWaardenMethodeAnnotation(mijnKlasse);
    }

    public void toonClassAnnotations(MijnKlasseJos mijnKlasse){
        System.out.println("Alle annotaties voor MijnKlasseJos:");
        for (Annotation a :
                mijnKlasse.getClass().getAnnotations()) {
            System.out.println(a);
        }
    }

    public void toonWaardenClassAnnotation(MijnKlasseJos mijnKlasse){
        System.out.println("Alle parameters:");
        WatJos wat = mijnKlasse.getClass().getAnnotation(WatJos.class);
        System.out.println(wat.omschrijving());
        MijnAnnotatieJos mijnAnnotatie = mijnKlasse.getClass().getAnnotation(MijnAnnotatieJos.class);
        System.out.println(mijnAnnotatie.string() + " " + mijnAnnotatie.waarde());
    }

    public void toonMethodeAnnotations(MijnKlasseJos mijnKlasse){
        System.out.println("Alle annotaties voor demoMethode:");
        for (Method m :
                mijnKlasse.getClass().getDeclaredMethods()) {
            for (Annotation a :
                    m.getAnnotations()) {
                System.out.println(a);
            }
        }
    }

    public void toonWaardenMethodeAnnotation(MijnKlasseJos mijnKlasse){
        System.out.println("Alle parameters:");
        for (Method m :
                mijnKlasse.getClass().getDeclaredMethods()) {
            WatJos wat = m.getAnnotation(WatJos.class);
            System.out.println(wat.omschrijving());
            MijnAnnotatieJos mijnAnnotatie = m.getAnnotation(MijnAnnotatieJos.class);
            System.out.println(mijnAnnotatie.string() + " " + mijnAnnotatie.waarde());
        }
    }
}