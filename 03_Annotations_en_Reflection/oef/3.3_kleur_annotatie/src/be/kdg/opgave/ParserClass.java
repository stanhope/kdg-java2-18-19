package src.be.kdg.opgave;

import java.lang.reflect.*;

public class ParserClass {
    public static void execute(Class<?> aClass) {
        try {
            Object anObject = aClass.newInstance();

            for (Method method : aClass.getDeclaredMethods()) {
                Kleur kleurAnnot = method.getAnnotation(Kleur.class);
                if (kleurAnnot != null) {

                    String  waarde = kleurAnnot.kleurNaam();
                    Method setter = MyAnnotations.class.getDeclaredMethod("setKleur", String.class);

                    setter.invoke(anObject,waarde);

                    method.invoke(anObject);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();

        }
    }
}


/*
*
* */