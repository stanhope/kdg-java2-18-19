package reflection;

import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class ReflectionRunner {
    public static void reflect(Class myClass) throws IllegalAccessException, InstantiationException, InvocationTargetException {
        Object object = myClass.newInstance();

        Method toRunMethod = null;

        System.out.print("Voor reflection: ");
        System.out.println(object.toString());
        for (Method method : myClass.getDeclaredMethods()) {
            System.out.print(method.getName() + " ");

            for (Annotation methodAnnotation : method.getDeclaredAnnotations()) {
                if (methodAnnotation.annotationType() == Changeable.class) {
                    toRunMethod = method;
                }
            }
        }
        System.out.println(" ");

        if (toRunMethod != null) {
            toRunMethod.invoke(object, "Jan de Rijke");
        }
        System.out.print("Na reflection: ");
        System.out.println(object.toString());
    }
}

