package concurrency;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class ReverseDemo {
    private static final String[] words = {
            "een", "twee", "drie",
            "vier", "vijf", "zes",
            "zeven", "acht", "negen"
    };

    public static void main(String[] args) throws Exception {
        ExecutorService pool = Executors.newFixedThreadPool(3);
        List<Future<String>> myList = new ArrayList<>();
        for (String word : words) {
            myList.add(pool.submit(() -> new StringBuilder(word).reverse().toString()));
        }

        pool.shutdown();

        myList.forEach(stringFuture -> {
            try {
                System.out.println(stringFuture.get());
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }
}

/*
The sum of lengths is 36
*/

