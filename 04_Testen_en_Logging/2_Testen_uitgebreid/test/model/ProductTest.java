package model;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

public class ProductTest {
    Product2 p1, p2;

    @Before
    public void setUp() throws Exception {
        p1 = new Product2(111, "kiwi", 0.45);
        p2 = new Product2(111, "banaan", 0.15);
    }

    @After
    public void tearDown() throws Exception {
        p1 = null;
    }

    @Test
    public void testGetters() {
        assertEquals("Naam komt niet overeen", "kiwi", p1.getNaam());
        assertEquals(0.45, p1.getPrijs(), 0.0);
        assertEquals(111, p1.getArtNr());
    }

    @Test
    public void testEqualsHashCode() {
        assertTrue("2 producten met zelfde artnr moeten gelijk zijn", p1.equals(p2));
        assertEquals("2 producten met zelfde artnr moeten zelfde hashCode opleveren", p1.hashCode(), p2.hashCode());
    }

    @Test
    public void setNaam() {
        try {
            p1.setNaam("");
            fail("een lege naam moet een exception opleveren");
        } catch (IllegalArgumentException e) {
            //test gelukt
        }
    }

    @Test(expected = IllegalArgumentException.class)
    public void setPrijs() {
        p1.setPrijs(-1);
        fail("Een prijs lager dan 1 moet een exception opleveren");
    }
}

