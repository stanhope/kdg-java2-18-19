/*
package model;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.fail;

public class WinkelWagentjeTest {
    private WinkelWagentjeImplementatie wagentje;
    private Product3 p1, p2;

    @Before
    public void setUp() {
//        System.out.println("@Before");
        wagentje = new WinkelWagentjeImplementatie();
        p1 = new Product3(111, "kiwi", 0.45);
        p2 = new Product3(222, "banaan", 0.15);
        wagentje.voegToe(p1);
        wagentje.voegToe(p2);
    }

    @After
    public void tearDown() {
//        System.out.println("@After");
        wagentje = null;
    }

    @BeforeClass
    public static void blabla() {
//        System.out.println("@BeforeClass");
    }

    @Test
    public void testWagentje() {
        List<Product3> lijst = wagentje.getProductenList();
        assertEquals("Het aantal producten moet <2> zijn", 2, wagentje.getAantal());
        assertSame("Het eerste product is verkeerd", lijst.get(0), p1);
        assertEquals("Het saldo moet <0.60> zijn", 0.60, wagentje.getSaldo(), 0.0);
    }

    @Test
    public void testWagentjeLeegMaken() {
        wagentje.maakWagentjeLeeg();
        assertEquals("Het wagentje moet leeg zijn", 0, wagentje.getAantal());
        assertEquals("Het saldo moet <0> zijn", 0.0, wagentje.getSaldo(), 0.0);
        assertEquals("De list met producten moet leeg zijn", 0, wagentje.getProductenList().size());
    }

    @Test
    public void testVerwijder() {
        wagentje.verwijder(p1);
        assertEquals("Het aantal moet <1> zijn", 1, wagentje.getAantal());
        wagentje.verwijder(p2);
        assertEquals("Het aantal moet <0> zijn", 0, wagentje.getAantal());
    }

    @Test(expected = IllegalArgumentException.class)
    public void verwijderNietAanwezig() {
        Product3 p3 = new Product3(666,"Dummy", 4.95);
        wagentje.verwijder(p3);
        fail("Het verwijderen van een ontbrekend product moet een exception veroorzaken");
    }

    //TODO: werk uit
    public void testUnmodifiable() {

    }
}
*/
