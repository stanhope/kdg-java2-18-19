package model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class WinkelWagentje2 {
    private List<Product3> wagentje;
    private double saldo;

    public WinkelWagentje2() {
        wagentje = new ArrayList<>();
    }

    public void voegToe(Product3 product) {
        wagentje.add(product);
        saldo += product.getPrijs();
    }

    public void verwijder(Product3 product) throws IllegalArgumentException {
        if (!wagentje.remove(product)) {
            throw new IllegalArgumentException("Product3 " + product.getNaam() + " niet gevonden!");
        }
        saldo -= product.getPrijs();
    }

    public int getAantal() {
        return wagentje.size();
    }

    public double getSaldo() {
        return saldo;
    }

    public void maakWagentjeLeeg() {
        wagentje.clear();
        saldo = 0.0;
    }

    public List<Product3> getProductenList() {
        return Collections.unmodifiableList(wagentje);
    }
}

