package model;

import java.util.Objects;

public class Product2 {
    private final int artNr; //uniek
    private String naam;
    private double prijs;

    public Product2(int artNr, String naam, double prijs) {
        this.artNr = artNr;
        setNaam(naam);
        setPrijs(prijs);
    }

    public int getArtNr() {
        return artNr;
    }

    public String getNaam() {
        return naam;
    }

    public double getPrijs() {
        return prijs;
    }

    public void setNaam(String naam) {
        if (naam == null || naam.isEmpty()) {
            throw new IllegalArgumentException("Foutieve waarde voor naam");
        }
        this.naam = naam;
    }

    public void setPrijs(double prijs) {
        if (prijs <= 0.0) {
            throw new IllegalArgumentException("Ongeldige waarde voor prijs");
        }
        this.prijs = prijs;
    }

    @Override
    public String toString() {
        return String.format("%6d %-20s(%.2f€)", artNr, naam, prijs);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Product2 product = (Product2) o;
        return artNr == product.getArtNr();
    }

    @Override
    public int hashCode() {
        return Objects.hash(artNr);
    }
}

