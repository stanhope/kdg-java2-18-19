package model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Logger;

public class WinkelWagentje3 {
    private List<Product3> wagentje;
    private double saldo;
    private static final Logger logger = Logger.getLogger("model.week4.Testen_1.model.WinkelWagentjeImplementatie");

    public WinkelWagentje3() {
        wagentje = new ArrayList<>();
    }

    public void voegToe(Product3 product) {
        wagentje.add(product);
        saldo += product.getPrijs();
        logger.fine(String.format("Product3 %s toegevoegd", product.getNaam()));
    }

    public void verwijder(Product3 product) throws IllegalArgumentException {
        if (!wagentje.remove(product)) {
            logger.warning(String.format("Product3 %s niet gevonden", product.getNaam()));
        }
        saldo -= product.getPrijs();
        logger.fine(String.format("Product3 %s verwijderd", product.getNaam()));
    }

    public int getAantal() {
        return wagentje.size();
    }

    public double getSaldo() {
        return saldo;
    }

    public void maakWagentjeLeeg() {
        wagentje.clear();
        saldo = 0.0;
        logger.fine(String.format("Wagentje leeg gemaakt"));
    }

    public List<Product3> getProductenList() {
        return Collections.unmodifiableList(wagentje);
    }
}

