package model;

import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Product3 {
    private final int artNr; //uniek
    private String naam;
    private double prijs;
    private static final Logger logger = Logger.getLogger("model.week4.Testen_1.model.Product3");


    public Product3(int artNr, String naam, double prijs) {
        this.artNr = artNr;
        setNaam(naam);
        setPrijs(prijs);
    }

    public int getArtNr() {
        return artNr;
    }

    public String getNaam() {
        return naam;
    }

    public double getPrijs() {
        return prijs;
    }

    public void setNaam(String naam) {
        if (naam == null || naam.isEmpty()) {
            logger.log(Level.WARNING, String.format("%s = foutieve waarde", naam));
//            throw new IllegalArgumentException("Foutieve waarde voor naam");
        }
        this.naam = naam;
    }

    public void setPrijs(double prijs) {
        if (prijs <= 0.0) {
            logger.warning(String.format("%f = Ongeldige waarde voor prijs", prijs));
//            throw new IllegalArgumentException("Ongeldige waarde voor prijs");
        }
        this.prijs = prijs;
    }

    @Override
    public String toString() {
        return String.format("%6d %-20s(%.2f€)", artNr, naam, prijs);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Product3 product = (Product3) o;
        return artNr == product.getArtNr();
    }

    @Override
    public int hashCode() {
        return Objects.hash(artNr);
    }
}

