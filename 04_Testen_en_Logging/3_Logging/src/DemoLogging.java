
import model.Product3;
import model.WinkelWagentje3;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.logging.LogManager;

public class DemoLogging {
    public static void main(String[] args) {
        loadLoggingConfiguration();

        // 2 foutieve producten aanmaken:
        Product3 p1 = new Product3(1, "", 1.0);
        Product3 p2 = new Product3(2, "Koekjes", -5.50);

        WinkelWagentje3 wagentje = new WinkelWagentje3();
        wagentje.voegToe(p1);
        wagentje.voegToe(p2);
        wagentje.verwijder(p2);
        wagentje.maakWagentjeLeeg();
    }

    private static void loadLoggingConfiguration() {
        URL configURL = DemoLogging.class.getResource("logging.properties");
        if (configURL != null) {
            try (InputStream is = configURL.openStream()) {
                LogManager.getLogManager().readConfiguration(is);
            } catch (IOException e) {
                System.err.println("Configuratiebestand is corrupt");
            }
        } else {
            System.err.println("Configuratiebestand NIET GEVONDEN");
        }
    }
}
