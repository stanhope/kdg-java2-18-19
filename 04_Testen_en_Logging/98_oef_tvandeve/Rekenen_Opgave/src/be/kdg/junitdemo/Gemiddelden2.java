package be.kdg.junitdemo;

import java.util.ArrayList;

/**
 * In deze klasse vind je een aantal logische fouten terug.
 * Verbeter ze aan de hand van de bijgevoegde testklasse TestGemiddelden.
 * Zorg ervoor dat je voor alle testen een groene balk krijgt!
 */
public class Gemiddelden2 {
    private ArrayList<Double> getallen;
    private double som = 0;
    private double product = 0;

    public Gemiddelden2() {
        this.getallen = new ArrayList<>();
    }

    public void voegGetalToe(double getal) {
        getallen.add(getal);
    }

    public void maakLeeg() {
        this.getallen = new ArrayList<>();
    }

    // Klassieke berekening van het gemiddelde
    public double rekenkundigGemiddelde() {
        som = 0.0;
        if (getallen.size() == 0){
            throw new ArithmeticException("je kan niet het gemiddelde van 0 berekenen");
        } else {
            for (Double getal : getallen) {
                som += getal;
            }
        }
        return som / getallen.size();
    }

    // Zie onder ander http://nl.wikipedia.org/wiki/Meetkundig_gemiddelde
    // en uiteraard Javadoc voor de pow-methode.
    public double meetkundigGemiddelde() {

        product = 1;
            for (Double getal : getallen) {
                product *= getal;
            }


        return Math.pow(product,1/getallen.size());

    }
}
