package bankaccount;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Testklasse voor JUnit, test de klasse BankAccount.
 */
public class TestBankAccount {


	/**
	 * Todo 1: Test de toString methode  van BankAccount.
	 */

	private BankAccount bankAccount;

	@After
	public void after(){
		bankAccount = null;
	}

	@Test
	public void testToString(){
		bankAccount = new BankAccount("BE65409407376196");
		assertEquals( "ToString zou de bankrekening moeten splitsen!", "BE65 4094 0737 6196", bankAccount.toString());
	}

}

