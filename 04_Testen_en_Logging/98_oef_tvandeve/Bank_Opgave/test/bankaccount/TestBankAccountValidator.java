package bankaccount;

import org.junit.Before;
import org.junit.Test;

public class TestBankAccountValidator {
    // Testdata:
    private String[] accounts = {
            "BE65409407376196", "BE39230033988719", //juiste nummers
            "BE2873402525022", "BA28734025250220", "BE2873402525o220", "BE29734025250220"}; //foutieve nummers

    /**
     * TODO 2: Test BankAccountValidator: Test de validatie van twee goede
		 * rekeningnummers.
     * Er mag geen exception optreden.
     */

    @Test
    public void testBankAccountValidatorCorrect(){

    }


    /**
     * TODO 3: Test de validatie van een rekeningnummer met te weinig cijfers.
     * Er moet een IllegalArgumentException optreden.
     */
    @Test(expected = IllegalArgumentException.class)
    public void testValidatorMetKorteNummer(){
        BankAccountValidator.validateAccount(accounts[2]);
    }


    /**
     * TODO 4: Test de validatie van een rekeningnummer met een verkeerde landcode.
     * Er moet een IllegalArgumentException optreden.
     */

    @Test(expected = IllegalArgumentException.class)
    public void testValidatorVerkeerdeLandcodes(){
        BankAccountValidator.validateAccount(accounts[3]);
    }


    /**
     * TODO 5: Test de validatie van een rekeningnummer dat niet numerieke tekens bevat.
     * Er moet een IllegalArgumentException optreden.
     */
    @Test(expected = IllegalArgumentException.class)
    public void testValidatorNietNumeriekeNummmer(){
        BankAccountValidator.validateAccount(accounts[4]);
    }


    /**
     * TODO 6: Test de validatie van een niet geldig rekeningnummer.
     * Er moet een IllegalArgumentException optreden.
     */
    @Test(expected = IllegalArgumentException.class)
    public void testValidatorNietGeldigNummer(){
        BankAccountValidator.validateAccount(accounts[5]);
    }

}