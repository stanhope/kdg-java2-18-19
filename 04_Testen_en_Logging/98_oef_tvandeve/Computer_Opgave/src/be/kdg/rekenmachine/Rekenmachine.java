package be.kdg.rekenmachine;

import be.kdg.rekenmachine.plugins.Plugin;

import java.util.logging.Level;
import java.util.logging.Logger;

public class Rekenmachine {

    private Plugin[] plugins = new Plugin[100];
    private int aantal;
    private static final Logger logger = Logger.getLogger("be.kdg.rekenmachine.Rekenmachine");

    public void installeer(Plugin plugin) {
        plugins[aantal++] = plugin;
    }

    public void toonPlugins() {
        System.out.print("Plugins: ");
        for (int i = 0; i < aantal; i++) {
            System.out.print(plugins[i].getCommand() + " ");
        }
        System.out.println();
        logger.fine("Toon plugings");
    }

    public double bereken(String operator, int x, int y) {
        double resultaat = 0.0;
        for (int i = 0; i < aantal; i++) {
            if (plugins[i].getCommand().equals(operator)) {
                return plugins[i].bereken(x, y);
            }
        }
        logger.info(String.format("Berekening met %s operator", operator));
        return resultaat;
    }
}
