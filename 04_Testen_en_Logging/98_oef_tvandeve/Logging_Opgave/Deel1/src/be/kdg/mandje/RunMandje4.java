package be.kdg.mandje;


import be.kdg.mandje.demo.Artikel3;
import be.kdg.mandje.demo.Mandje3;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.logging.LogManager;

public class RunMandje4 {
    public static void main(String[] args) {
        Mandje3 mandje = new Mandje3();
        loadLoggingConfiguration();

        mandje.voegToe(new Artikel3("Druiven", 2.0));
        mandje.voegToe(new Artikel3("Passievruchten", 2.35));
        mandje.voegToe(new Artikel3("Mandarijnen", 2.50));
        mandje.voegToe(new Artikel3("Pruimen", 2.49));
        Artikel3 kiwis = new Artikel3("Kiwis", 4.95);
        mandje.voegToe(kiwis);
        mandje.voegToe(new Artikel3("Druiven", 1.95));
        mandje.voegToe(new Artikel3("Peren", 1.99));
        mandje.voegToe(new Artikel3("Appels", 2.20));
        mandje.verwijder(kiwis);

/*        mandje.toon();

        System.out.println("\nGesorteerd volgens naam:");
        mandje.sorteerVolgensNaam();

        System.out.println("\nGesorteerd volgens prijs:");
        mandje.sorteerVolgensPrijs();
        mandje.toon();*/
    }

    private static void loadLoggingConfiguration() {
        URL configURL = RunMandje4.class.getResource("logging.properties");
        if (configURL != null) {
            try (InputStream is = configURL.openStream()) {
                LogManager.getLogManager().readConfiguration(is);
            } catch (IOException e) {
                System.err.println("Configuratiebestand is corrupt");
            }
        } else {
            System.err.println("Configuratiebestand NIET GEVONDEN");
        }
    }

}
