package be.kdg.mandje.demo;

import java.util.Objects;

public class Artikel4 implements Comparable<Artikel4> {
    private String naam;
    private double prijs;

    public Artikel4(String naam, double prijs) {
        this.naam = naam;
        this.prijs = prijs;
    }

    String getNaam() {
        return naam;
    }

    double getPrijs() {
        return prijs;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Artikel4 artikel = (Artikel4) o;
        return Objects.equals(naam, artikel.naam);
    }

    @Override
    public int hashCode() {
        return Objects.hash(naam);
    }

    @Override
    public int compareTo(Artikel4 anderArtikel) {
        return this.naam.compareTo(anderArtikel.naam);
    }
}
