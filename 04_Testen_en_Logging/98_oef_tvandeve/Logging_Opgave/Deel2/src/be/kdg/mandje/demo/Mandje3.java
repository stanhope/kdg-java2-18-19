package be.kdg.mandje.demo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Mandje3 {
    private List<Artikel3> mandje;

    public Mandje3() {
        mandje = new ArrayList<>();
    }

    public void voegToe(Artikel3 artikel) {
        if (!mandje.contains(artikel)) {
            mandje.add(artikel);
        }
    }

    public void verwijder(Artikel3 artikel) {
        mandje.remove(artikel);
    }

    public void sorteerVolgensNaam() {
        Collections.sort(mandje);
    }

    public void sorteerVolgensPrijs() {
        mandje.sort(new Comparator<Artikel3>() {
            @Override
            public int compare(Artikel3 artikel, Artikel3 anderArtikel) {
                return Double.compare(artikel.getPrijs(), anderArtikel.getPrijs());
            }
        });
    }

    public void toon() {
        for (Artikel3 artikel : mandje) {
            System.out.format("%-16s €%4.2f\n", artikel.getNaam(), artikel.getPrijs());
        }
    }
}

