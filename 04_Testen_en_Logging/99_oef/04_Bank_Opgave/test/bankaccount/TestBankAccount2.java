package bankaccount;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Testklasse voor JUnit, test de klasse BankAccount2.
 */
public class TestBankAccount2 {
	private BankAccount2 account;
	/**
	 * Todo 1: Test de toString methode  van BankAccount2.
	 */
	@Before
	public void before(){
		//feel free to donate
		account = new BankAccount2("BE03001515846884");
	}

	@Test
	public void testToString(){
		assertEquals(account.toString(),"BE03 0015 1584 6884");
	}

	@After
	public void after(){
		account = null;
	}

}

