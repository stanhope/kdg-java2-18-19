package be.kdg.mandje.demo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.logging.Logger;

public class Mandje {
    private List<Artikel3> mandje;
    private static final Logger logger = Logger.getLogger("be.kdg.mandje.demo.MandjeLogger");

    public Mandje() {
        mandje = new ArrayList<>();
        /*logger.setLevel(Level.ALL);
        ConsoleHandler handler = new ConsoleHandler();
        handler.setFormatter(new SimpleFormatter());
        handler.setLevel(Level.ALL);
        logger.addHandler(handler);*/
    }

    public void voegToe(Artikel3 artikel) {
        if (!mandje.contains(artikel)) {
            mandje.add(artikel);
            logger.info("Artikel3 " + artikel.toString() + " toegevoegd");
        } else {
            logger.warning("Artikel3 " + artikel.toString() + " NIET toegevoegd");
        }
    }

    public void verwijder(Artikel3 artikel) {
        mandje.remove(artikel);
        logger.warning("Artikel3 " + artikel + " verwijderd");
    }

    public void sorteerVolgensNaam() {
        Collections.sort(mandje);
    }

    public void sorteerVolgensPrijs() {
        mandje.sort(new Comparator<Artikel3>() {
            @Override
            public int compare(Artikel3 artikel, Artikel3 anderArtikel) {
                return Double.compare(artikel.getPrijs(), anderArtikel.getPrijs());
            }
        });
    }

    public void toon() {
        for (Artikel3 artikel : mandje) {
            System.out.format("%-16s €%4.2f\n", artikel.getNaam(), artikel.getPrijs());
        }
    }
}

