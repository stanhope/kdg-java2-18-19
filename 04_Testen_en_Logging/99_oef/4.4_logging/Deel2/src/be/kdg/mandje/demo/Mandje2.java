package be.kdg.mandje.demo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.logging.ConsoleHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

public class Mandje2 {
    private List<Artikel2> mandje;
    private static final Logger logger = Logger.getLogger("be.kdg.Mandje2.demo.MandjeLogger");

    public Mandje2() {
        mandje = new ArrayList<>();
        /*logger.setLevel(Level.ALL);
        ConsoleHandler handler = new ConsoleHandler();
        handler.setFormatter(new SimpleFormatter());
        handler.setLevel(Level.ALL);
        logger.addHandler(handler);*/
    }

    public void voegToe(Artikel2 artikel) {
        if (!mandje.contains(artikel)) {
            mandje.add(artikel);
            logger.info("Artikel2 " + artikel.toString() + " toegevoegd");
        } else {
            logger.warning("Artikel2 " + artikel.toString() + " NIET toegevoegd");
        }
    }

    public void verwijder(Artikel2 artikel) {
        mandje.remove(artikel);
        logger.warning("Artikel2 " + artikel + " verwijderd");
    }

    public void sorteerVolgensNaam() {
        Collections.sort(mandje);
    }

    public void sorteerVolgensPrijs() {
        mandje.sort(new Comparator<Artikel2>() {
            @Override
            public int compare(Artikel2 artikel, Artikel2 anderArtikel) {
                return Double.compare(artikel.getPrijs(), anderArtikel.getPrijs());
            }
        });
    }

    public void toon() {
        for (Artikel2 artikel : mandje) {
            System.out.format("%-16s €%4.2f\n", artikel.getNaam(), artikel.getPrijs());
        }
    }
}

