package be.kdg.mandje;


import be.kdg.mandje.demo.Artikel2;
import be.kdg.mandje.demo.Mandje2;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.logging.LogManager;

public class RunMandje2 {
    public static void main(String[] args) {
        loadLoggingConfiguration();
        Mandje2 mandje = new Mandje2();

        mandje.voegToe(new Artikel2("Druiven", 2.0));
        mandje.voegToe(new Artikel2("Passievruchten", 2.35));
        mandje.voegToe(new Artikel2("Mandarijnen", 2.50));
        mandje.voegToe(new Artikel2("Pruimen", 2.49));
        Artikel2 kiwis = new Artikel2("Kiwis", 4.95);
        mandje.voegToe(kiwis);
        mandje.voegToe(new Artikel2("Druiven", 1.95));
        mandje.voegToe(new Artikel2("Peren", 1.99));
        mandje.voegToe(new Artikel2("Appels", 2.20));
        mandje.verwijder(kiwis);

/*        mandje.toon();

        System.out.println("\nGesorteerd volgens naam:");
        mandje.sorteerVolgensNaam();

        System.out.println("\nGesorteerd volgens prijs:");
        mandje.sorteerVolgensPrijs();
        mandje.toon();*/
    }

    private static void loadLoggingConfiguration() {
        Path path = Paths.get("04_Testen_en_Logging/99_oef/4.4_logging/Deel2/recources/loggingOef.properties");
        URL configURL = null;
        try {
            configURL = path.toUri().toURL();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
       // URL configURL = RunMandje3.class.getResource("99_oef/4.4_logging/Deel2/recources/loggingOef.properties");
        if (configURL != null) {
            try (InputStream is = configURL.openStream()) {
                LogManager.getLogManager().readConfiguration(is);
            } catch (IOException e) {
                System.err.println("Configuratiebestand is corrupt");
            }
        } else {
            System.err.println("Configuratiebestand NIET GEVONDEN");
        }
    }
}
