import data.OptredenData2;
import model.Optreden2;

import java.util.ArrayList;
import java.util.Map;
import java.util.TreeMap;

/**
 * Vervolledig de code volgens onderstaande instructies.
 * Vergelijk met de verwachte afdruk helemaal onderaan.
 */
public class DemoMap2 {
    public static void main(String[] args) {
        // 1) Vervolledig de methode maakMap in de klasse OptredenData1
        TreeMap<String, ArrayList<Optreden2>> optredenMap = OptredenData2.maakMap();

        // 2) Druk de gegevens af en controleer
        System.out.println("OVERZICHT per podium:");
        for (Map.Entry<String, ArrayList<Optreden2>> stringArrayListEntry : optredenMap.entrySet()) {
            System.out.println("\n"+stringArrayListEntry.getKey());
            for (Optreden2 optreden : stringArrayListEntry.getValue()) {
                System.out.println(optreden);
            }
        }
        // lus...

    }
}

/*
Verwachte afdruk:
OVERZICHT per podium:

Main Stage:
Faithless (Main Stage, 14u30)--> *****
Editors (Main Stage, 16u)--> ****
Muse (Main Stage, 19u)--> *****
Pink (Main Stage, 20u30)--> **

Marquee:
The Specials (Marquee, 13u10)--> *****
Florence and the Machine (Marquee, 18u45)--> ***
Editors (Marquee, 21u20)--> ****

Pyramid Marquee:
Faithless (Pyramid Marquee, 19u)--> *****
Absynthe Minded (Pyramid Marquee, 21u45)--> *****
Empire of the Sun (Pyramid Marquee, 23u45)--> *****

*/