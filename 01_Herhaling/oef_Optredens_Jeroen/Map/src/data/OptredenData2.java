package data;

import model.Optreden2;

import java.util.*;

public class OptredenData2 {
    private static Optreden2[] optredens = {
            new Optreden2("Editors", "Main Stage", 16, 0, 4),
            new Optreden2("Empire of the Sun", "Pyramid Marquee", 23, 45, 5),
            new Optreden2("Florence and the Machine", "Marquee", 18, 45, 3),
            new Optreden2("The Specials", "Marquee", 13, 10, 5),
            new Optreden2("Muse", "Main Stage", 19, 0, 5),
            new Optreden2("Faithless", "Main Stage", 14, 30, 5),
            new Optreden2("Absynthe Minded", "Pyramid Marquee", 21, 45, 5),
            new Optreden2("Pink", "Main Stage", 20, 30, 2),
            new Optreden2("Editors", "Marquee", 21, 20, 4),
            new Optreden2("Faithless", "Pyramid Marquee", 19, 0, 5)
    };

    /**
     * Maak vanuit de statische array optredens een TreeMap.
     * We willen de optredens mappen op de podia.
     * De TreeMap is gesorteerd op podium.
     * Dus als key neem je het podium, als value een ArrayList van optredens die
     * doorgaan op dat podium. De List van optredens moet gesorteerd zijn op tijdstip.
     *
     * @return TreeMap van optredens per podium
     */
    public static TreeMap<String, ArrayList<Optreden2>> maakMap() {
        TreeMap<String, ArrayList<Optreden2>> optredenMap = new TreeMap<>();
        for (Optreden2 optreden : optredens) {
            if (optredenMap.get(optreden.getPodium())==null) {
                ArrayList<Optreden2> list = new ArrayList<>();
                list.add(optreden);
                optredenMap.put(optreden.getPodium(),list);
            }
            else{
                ArrayList<Optreden2> getlist = optredenMap.get(optreden.getPodium());
                getlist.add(optreden);
                Collections.sort(getlist);
                optredenMap.put(optreden.getPodium(),getlist);
            }
        }
        return optredenMap;
    }
}

