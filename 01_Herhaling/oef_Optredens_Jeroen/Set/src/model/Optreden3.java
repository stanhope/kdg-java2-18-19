package model;

public class Optreden3 implements Comparable<Optreden3> {
    private String naam;
    private String podium;
    private int uur;
    private int min;
    private int sterren;

    public Optreden3(String naam, String podium, int uur, int min, int sterren) {
        this.naam = naam;
        this.podium = podium;
        this.uur = uur;
        this.min = min;
        this.sterren = sterren;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder(naam);
        sb.append(" (");
        sb.append(podium);
        sb.append(", ");
        sb.append(uur);
        sb.append("u");
        if (min > 0) sb.append(min);
        sb.append(")--> ");
        for (int i = 0; i < sterren; i++) {
            sb.append("*");
        }
        return sb.toString();
    }

    public String getNaam() {
        return naam;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj.getClass()==this.getClass()){
            if (this==obj){
                return true;
            }
            Optreden3 optreden = (Optreden3)obj;
            return this.naam.equals(optreden.getNaam());
        }
        return false;
    }

    @Override
    public int hashCode() {
        return naam.hashCode();
    }

    @Override
    public int compareTo(Optreden3 o) {
        return this.naam.compareTo(o.getNaam());
    }
}
