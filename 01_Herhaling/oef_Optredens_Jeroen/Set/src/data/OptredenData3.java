package data;

import model.Optreden3;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class OptredenData3 {
    private static Optreden3[] optredens = {
            new Optreden3("Editors", "Main Stage", 16, 0, 4),
            new Optreden3("Empire of the Sun", "Pyramid Marquee", 23, 45, 5),
            new Optreden3("Florence and the Machine", "Marquee", 18, 45, 3),
            new Optreden3("The Specials", "Marquee", 13, 10, 5),
            new Optreden3("Muse", "Main Stage", 19, 0, 5),
            new Optreden3("Faithless", "Main Stage", 14, 30, 5),
            new Optreden3("Absynthe Minded", "Pyramid Marquee", 21, 45, 5),
            new Optreden3("Pink", "Main Stage", 20, 30, 2),
            new Optreden3("Editors", "Marquee", 21, 20, 4),
            new Optreden3("Faithless", "Pyramid Marquee", 19, 0, 5)
    };

    /**
     * Maak vanuit de statische array optredens een HashSet.
     *
     * @return HashSet van optredens
     */
    public static Set<Optreden3> maakSet() {
        return new HashSet<>(Arrays.asList(optredens));
    }
}
