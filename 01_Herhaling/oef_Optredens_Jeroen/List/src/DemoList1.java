import data.OptredenData1;
import model.NaamComparator1;
import model.Optreden1;

import java.util.Comparator;
import java.util.List;

/**
 * Vervolledig de code volgens onderstaande instructies.
 * Vergelijk met de verwachte afdruk helemaal onderaan.
 */
public class DemoList1 {
    public static void main(String[] args) {
        // 1) Vervolledig de methode maakList in de klasse OptredenData1
        List<Optreden1> optredenList = OptredenData1.maakList();

        // 2) Controleer de afdruk
        System.out.println("ONGESORTEERD:");
        for (Optreden1 optreden : optredenList) {
            System.out.println(optreden);
        }

        // 3) Sorteer op sterren (asterisken). Pas daartoe de klasse Optreden1 aan
        optredenList.sort(new Comparator<Optreden1>() {
            @Override
            public int compare(Optreden1 o1, Optreden1 o2) {
                return o1.compareTo(o2);
            }
        });

        // 4) Controleer de afdruk
        System.out.println("\nGESORTEERD op sterren:");
        for (Optreden1 optreden : optredenList) {
            System.out.println(optreden);
        }

        // 5) Sorteer op naam. Gebruik een aparte NaamComparator1-klasse
        optredenList.sort(new NaamComparator1());

        // 6) Controleer de afdruk
        System.out.println("\nGESORTEERD op naam:");
        for (Optreden1 optreden : optredenList) {
            System.out.println(optreden);
        }
    }
}

/* Verwachte afdruk:
ONGESORTEERD:
Editors (Main Stage, 16u)--> ****
Empire of the Sun (Pyramid Marquee, 23u45)--> *****
Florence and the Machine (Marquee, 18u45)--> ***
The Specials (Marquee, 13u10)--> *****
Muse (Main Stage, 19u)--> *****
Faithless (Main Stage, 14u30)--> *****
Absynthe Minded (Pyramid Marquee, 21u45)--> *****
Pink (Main Stage, 20u30)--> **
Editors (Marquee, 21u20)--> ****
Faithless (Pyramid Marquee, 19u)--> *****

GESORTEERD op sterren:
Empire of the Sun (Pyramid Marquee, 23u45)--> *****
The Specials (Marquee, 13u10)--> *****
Muse (Main Stage, 19u)--> *****
Faithless (Main Stage, 14u30)--> *****
Absynthe Minded (Pyramid Marquee, 21u45)--> *****
Faithless (Pyramid Marquee, 19u)--> *****
Editors (Main Stage, 16u)--> ****
Editors (Marquee, 21u20)--> ****
Florence and the Machine (Marquee, 18u45)--> ***
Pink (Main Stage, 20u30)--> **

GESORTEERD op naam:
Absynthe Minded (Pyramid Marquee, 21u45)--> *****
Editors (Main Stage, 16u)--> ****
Editors (Marquee, 21u20)--> ****
Empire of the Sun (Pyramid Marquee, 23u45)--> *****
Faithless (Main Stage, 14u30)--> *****
Faithless (Pyramid Marquee, 19u)--> *****
Florence and the Machine (Marquee, 18u45)--> ***
Muse (Main Stage, 19u)--> *****
Pink (Main Stage, 20u30)--> **
The Specials (Marquee, 13u10)--> *****
*/