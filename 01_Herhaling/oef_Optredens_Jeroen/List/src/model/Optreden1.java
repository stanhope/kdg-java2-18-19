package model;

public class Optreden1 implements Comparable<Optreden1>{
    private String naam;
    private String podium;
    private int uur;
    private int min;
    private int sterren;

    public Optreden1(String naam, String podium, int uur, int min, int sterren) {
        this.naam = naam;
        this.podium = podium;
        this.uur = uur;
        this.min = min;
        this.sterren = sterren;
    }



    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder(naam);
        sb.append(" (");
        sb.append(podium);
        sb.append(", ");
        sb.append(uur);
        sb.append("u");
        if (min > 0) sb.append(min);
        sb.append(")--> ");
        for (int i = 0; i < sterren; i++) {
            sb.append("*");
        }
        return sb.toString();
    }

    public String getNaam() {
        return naam;
    }

    @Override
    public int compareTo(Optreden1 o) {
        return this.sterren-o.sterren;
    }
}
