package model;

public class NaamComparator1 implements java.util.Comparator<Optreden1> {
    @Override
    public int compare(Optreden1 o1, Optreden1 o2) {
        return o1.getNaam().compareTo(o2.getNaam());
    }
}
