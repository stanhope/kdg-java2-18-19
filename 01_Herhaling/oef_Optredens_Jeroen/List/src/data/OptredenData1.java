package data;

import model.Optreden1;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class OptredenData1 {
    private static Optreden1[] optredens = {
            new Optreden1("Editors", "Main Stage", 16, 0, 4),
            new Optreden1("Empire of the Sun", "Pyramid Marquee", 23, 45, 5),
            new Optreden1("Florence and the Machine", "Marquee", 18, 45, 3),
            new Optreden1("The Specials", "Marquee", 13, 10, 5),
            new Optreden1("Muse", "Main Stage", 19, 0, 5),
            new Optreden1("Faithless", "Main Stage", 14, 30, 5),
            new Optreden1("Absynthe Minded", "Pyramid Marquee", 21, 45, 5),
            new Optreden1("Pink", "Main Stage", 20, 30, 2),
            new Optreden1("Editors", "Marquee", 21, 20, 4),
            new Optreden1("Faithless", "Pyramid Marquee", 19, 0, 5)
    };

    /**
     * Maak vanuit de statische array optredens een ArrayList.
     *
     * @return ArrayList van optredens
     */
    public static List<Optreden1> maakList() {
        return new ArrayList<Optreden1>(Arrays.asList(optredens));
    }
}
