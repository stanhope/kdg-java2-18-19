

import be.kdg.model.Student;

import java.time.LocalDate;
import java.util.*;

public class DemoStudent {
    public static void main(String[] args) {
        List<Student> studentList = new ArrayList<>();

        studentList.add(new Student(9999, "Charlotte Vermeulen", LocalDate.of(2000, 1, 24), "Antwerpen"));
        studentList.add(new Student(666, "Donald Trump", LocalDate.of(1946, 6, 14), "Washington"));
        studentList.add(new Student(12345, "Sam Gooris", LocalDate.of(1973, 4, 10)));
        studentList.add(new Student(12345, "Koen Schram", LocalDate.of(1967, 5, 15), "Merksem"));

        System.out.println("studentList volgens studnr:");
        Collections.sort(studentList);
        for (Student student : studentList) {
            System.out.println(student.toString());
        }

        System.out.println("\nstudentList volgens leeftijd:");
        studentList.sort((o1, o2) -> (o1.getGeboorte().compareTo(o2.getGeboorte())));
        studentList.forEach(student -> System.out.println(student.toString()));

        //Overzetten naar HashSet:
        System.out.println("\nHashSet:");
        Set<Student> mySet = new HashSet<>(studentList);
        mySet.forEach(student -> System.out.println(student.toString()));

        //Overzetten naar TreeSet:
        System.out.println("\nTreeSet:");
        Set<Student> treeSet = new TreeSet<>(studentList);
        treeSet.forEach(student -> System.out.println(student.toString()));

        //Overzetten naar TreeMap:
        System.out.println("\nTreeMap:");
        Map<Student, Double> treeMap = new TreeMap<>();
        Random ram = new Random();
        for (Student student : studentList) {
            treeMap.put(student, ram.nextDouble()*20);
        }

        treeMap.forEach((student, aDouble) -> System.out.println(student.toString() + " " + aDouble));

        for (Map.Entry<Student, Double> entry : treeMap.entrySet()) {
            System.out.println("Student " + entry.getKey().getNaam() + " behaalde " + entry.getValue() + "/20");
        }
    }
}
