package be.kdg.team;

public interface Showable {

    void show();
}
