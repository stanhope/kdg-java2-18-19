package be.kdg.team;

public class ProTeam extends Team {
    private String sponsorNaam;
    private SponsorSoort sponsorSoort;

    public ProTeam(String naam, String sponsorNaam, SponsorSoort sponsorSoort) {
        super(naam);
        this.sponsorNaam = sponsorNaam;
        this.sponsorSoort = sponsorSoort;
    }

    public String getSponsorNaam() {
        return sponsorNaam;
    }

    public SponsorSoort getSponsorSoort() {
        return sponsorSoort;
    }

    public void show() {
        System.out.println("Naam: " + super.getNaam());
        System.out.println("Sponser: " + getSponsorNaam() + " (" + getSponsorSoort() + ")");
        System.out.println("Leden:");
        super.getSpelers().forEach(System.out::println);
    }

    //TODO  6.4  Vul de methode verhaspelSponsorNaam aan.
    // De methode dient de volgorde van de letters van de sponsornaam willekeurig door elkaar te halen
    // en als String terug te geven.
    public String verhaspelSponsorNaam() {
        String shuffledString = getSponsorNaam();

        return shuffledString;
    }
}
