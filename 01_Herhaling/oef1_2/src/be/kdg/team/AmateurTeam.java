package be.kdg.team;

public class AmateurTeam extends Team implements Showable{
    // geen extra attributen

    public AmateurTeam(String naam) {
        super(naam);
    }


    public void show() {
        System.out.println("Naam: " + super.getNaam());
        System.out.println("Leden:");
        super.getSpelers().forEach(System.out::println);
    }
}
