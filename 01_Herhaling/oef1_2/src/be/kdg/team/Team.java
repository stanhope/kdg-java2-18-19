package be.kdg.team;

import be.kdg.herhaling.Speler;

import java.util.ArrayList;
import java.util.List;

public abstract class Team implements Showable {
    private String naam;
    private List<Speler> spelers;

    // De volgende regel mag je niet wijzigen
    public Team() {
    }

    public Team(String naam) {
        this.naam = naam;
        this.spelers = new ArrayList<>();
    }

    public Speler zoekSpeler(int rugNummer) {
        for (Speler speler : spelers) {
            if (speler.getRugNummer() == rugNummer) return speler;
        }
        return null;
    }

   public void voegToe(Speler toeTeVoegenSpeler) {
        if (zoekSpeler(toeTeVoegenSpeler.getRugNummer()) == null) {
            this.spelers.add(toeTeVoegenSpeler);
        }
    }

     public void sort() {
        spelers.sort(Speler::compareTo);
    }

    public String getNaam() {
        return naam;
    }

    public List<Speler> getSpelers() {
        return spelers;
    }
}
