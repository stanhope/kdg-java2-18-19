package be.kdg.herhaling;

public class Speler implements Comparable<Speler>{
    private int rugNummer;
    private String naam;
    private Adres adres;

    public Speler(int rugNummer, String naam, Adres adres) {
        this.rugNummer = rugNummer;
        this.naam = naam;
        this.adres = adres;
    }

    public Speler(int rugNummer, String naam, String straat, int postnummer, String gemeente) {
        this(rugNummer, naam, new Adres(straat, postnummer, gemeente));
    }

    public int getRugNummer() {
        return rugNummer;
    }

    public String getNaam() {
        return naam;
    }

    public Adres getAdres() {
        return adres;
    }

    @Override
    public String toString() {
        return String.format("%2d %-20s %s", rugNummer, naam, adres.toString());
    }

    @Override
    public int compareTo(Speler s) {
        return this.naam.compareTo(s.naam);
    }

}
