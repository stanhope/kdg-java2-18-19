package be.kdg;

import be.kdg.herhaling.AdresJeroen;
import be.kdg.herhaling.SpelerJeroen;

import java.util.LinkedList;
import java.util.List;

public class TestSpelerJeroen {
    //2.4 verwijder commentaar, voer main uit en vergelijk resultaat
    public static void main(String[] args) {
        SpelerJeroen eenSpeler = new SpelerJeroen(10, "Cornelis Clem", "Nationalestraat 5", 2000, "Antwerpen");
        SpelerJeroen nogEenSpeler = new SpelerJeroen(7, "Gils Jos", new AdresJeroen("Kouterdreef 181", 2970, "Schilde"));
        List<SpelerJeroen> spelers = new LinkedList<>();
        spelers.add(eenSpeler);
        spelers.add(nogEenSpeler);

        for (SpelerJeroen speler : spelers) {
            System.out.println(speler);
        }
    }

}

/*
10 Cornelis Clem        Nationalestraat 5    2000 Antwerpen
 7 Gils Jos             Kouterdreef 181      2970 Schilde
*/