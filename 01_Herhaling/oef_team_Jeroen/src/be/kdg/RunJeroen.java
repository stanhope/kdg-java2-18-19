package be.kdg;

import be.kdg.herhaling.*;

public class RunJeroen {
    //6.5 verwijder commentaar, voer main uit en vergelijk resultaat
    public static void main(String[] args) {
        TeamJeroen teams[] = {
                new AmateurTeamJeroen("De Kampioenen"),
                new ProTeamJeroen("PSV Eindhoven", "Philips", SponsorSoortJeroen.INTERNATIONAAL),
                new ProTeamJeroen("SK Laarne", "Brouwerij Walrave", SponsorSoortJeroen.LOKAAL)
        };

        SpelerJeroen[] spelers = {
                new SpelerJeroen(13, "Goosens Erik", "Heieinde 36", 9200, "Appels"),
                new SpelerJeroen(21, "Vermeersch Marcel", new AdresJeroen("Hoogstraat 25", 9000, "Gent")),
                new SpelerJeroen(2, "Cornelis Clem", "Nationalestraat 5", 2000, "Antwerpen"),
                new SpelerJeroen(13, "Gils Jos", new AdresJeroen("Kouterdreef 181", 2970, "Schilde")),
                new SpelerJeroen(14, "De Vos Eric", "Hoogstraat 104", 2000, "Antwerpen"),
                new SpelerJeroen(6, "De Wilde Etiënne", "Vaartstraat 61", 9270, "Laarne"),
                new SpelerJeroen(18, "Van Damme Albert", "Eekhoekstraat 15", 9270, "Laarne"),
                new SpelerJeroen(7, "Raman Modest", "Quastraat 4", 9270, "Laarne")
        };

        for (int i = 0; i < 5; i++) {
            teams[0].voegToe(spelers[i]);
        }
        teams[0].sort();

        for (int i = 5; i < spelers.length; i++) {
            teams[2].voegToe(spelers[i]);
        }
        teams[2].sort();

        System.out.println("TeamJeroen 1:");
        teams[0].show();
        System.out.println("\nTeamJeroen 2:");
        teams[2].show();


        System.out.println("\nNaam: " + new ProTeamJeroen("SK Laarne", "Brouwerij Walrave",
                SponsorSoortJeroen.LOKAAL).verhaspelSponsorNaam());
    }

}

/*
TeamJeroen 1:
Naam: De Kampioenen
Leden:
 2 Cornelis Clem        Nationalestraat 5    2000 Antwerpen
14 De Vos Eric          Hoogstraat 104       2000 Antwerpen
13 Goosens Erik         Heieinde 36          9200 Appels
21 Vermeersch Marcel    Hoogstraat 25        9000 Gent

TeamJeroen 2:
Naam: SK Laarne
Sponsor: Brouwerij Walrave (Lokaal)
Leden:
 6 De Wilde Etiënne     Vaartstraat 61       9270 Laarne
 7 Raman Modest         Quastraat 4          9270 Laarne
18 Van Damme Albert     Eekhoekstraat 15     9270 Laarne

Naam: uWraelarverowiB j
*/