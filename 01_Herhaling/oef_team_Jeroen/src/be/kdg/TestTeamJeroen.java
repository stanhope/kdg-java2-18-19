package be.kdg;

import be.kdg.herhaling.*;

public class TestTeamJeroen {
    //3.6 verwijder commentaar, voer main uit en vergelijk resultaat
    public static void main(String[] args) {
        SpelerJeroen[] spelers = {
                new SpelerJeroen(13, "Goosens Erik", "Heieinde 36", 9200, "Appels"),
                new SpelerJeroen(21, "Vermeersch Marcel", new AdresJeroen("Hoogstraat 25", 9000, "Gent")),
                new SpelerJeroen(2, "Cornelis Clem", "Nationalestraat 5", 2000, "Antwerpen"),
                new SpelerJeroen(13, "Gils Jos", new AdresJeroen("Kouterdreef 181", 2970, "Schilde")),

        };

        TeamJeroen myTeam = new TeamJeroen("De Kampioenen") {
            public void show() {
            }
        };

        for (SpelerJeroen speler : spelers) {
            myTeam.voegToe(speler);
        }
        myTeam.sort();

        for (SpelerJeroen speler : myTeam.getSpelers()) {
            System.out.println(speler);
        }

        System.out.print("\nZoek speler nummer 13: ");
        System.out.println(myTeam.zoekSpeler(13));
        System.out.print("Zoek speler nummer 14: ");
        System.out.println(myTeam.zoekSpeler(14));
    }

}

/*
 2 Cornelis Clem        Nationalestraat 5    2000 Antwerpen
13 Goosens Erik         Heieinde 36          9200 Appels
21 Vermeersch Marcel    Hoogstraat 25        9000 Gent

Zoek speler nummer 13: 13 Goosens Erik         Heieinde 36          9200 Appels
Zoek speler nummer 14: null
 */
