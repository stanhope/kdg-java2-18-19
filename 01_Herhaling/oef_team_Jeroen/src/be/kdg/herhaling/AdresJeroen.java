package be.kdg.herhaling;

public class AdresJeroen {
    private String straat;
    private int postNummer;
    private String gemeente;

    // 1.1 Maak de constructor + getters
    public AdresJeroen(String straat, int postNummer, String gemeente) {
        this.straat = straat;
        this.postNummer = postNummer;
        this.gemeente = gemeente;
    }

    public String getStraat(){
        return straat;
    }

    public String getGemeente(){
        return gemeente;
    }

    public int getPostNummer(){
        return postNummer;
    }

    // 1.2 Override de toString methode
    @Override
    public String toString() {
        return String.format("%-20s %d %s",straat,postNummer,gemeente);
    }

}
