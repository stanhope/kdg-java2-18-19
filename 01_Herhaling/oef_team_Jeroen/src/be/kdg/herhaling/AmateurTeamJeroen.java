package be.kdg.herhaling;

public class AmateurTeamJeroen extends TeamJeroen {
    // geen extra attributen

    // 5.1 Laat de klasse van de klasse TeamJeroen overerven

    //5.2 Schrijf de nodige constructor (+ een getter voor naam?!)
    public AmateurTeamJeroen(String naam) {
        super(naam);
    }

    @Override
    public String getNaam() {
        return super.getNaam();
    }

    // 5.3 Implementeer hier de methode van de interface (show	),
    // ze toont alle informatie in verband met het team (zie TestTeamJeroen voor een voorbeeld)
    public void show() {
        System.out.printf("TeamJeroen %s\nLeden:\n",getNaam());
        for (SpelerJeroen speler :super.getSpelers() ) {
            System.out.println(speler);
        }
    }
}
