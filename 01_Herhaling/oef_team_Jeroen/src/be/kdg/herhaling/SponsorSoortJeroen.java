package be.kdg.herhaling;

public enum SponsorSoortJeroen {
    // 4.1 Vul de enum aan, bij het afdrukken van de constanten LOKAAL en INTERNATIONAAL
    // moet je 'Lokaal' en 'Internationaal' bekomen.
    LOKAAL,INTERNATIONAAL;
    public String toString() {
        return name().charAt(0) + name().substring(1).toLowerCase();
    }
}
