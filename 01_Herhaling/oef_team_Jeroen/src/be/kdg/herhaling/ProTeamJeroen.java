package be.kdg.herhaling;

import java.util.ArrayList;
import java.util.Collections;

public class ProTeamJeroen extends TeamJeroen {
    private String sponsorNaam;
    private SponsorSoortJeroen sponsorSoort;

    //6.1 Laat deze klasse van de klasse TeamJeroen overerven

    // 6.2 Schrijf de nodige constructor + getters voor beide attributen


    public ProTeamJeroen(String naam, String sponsorNaam, SponsorSoortJeroen sponsorSoort) {
        super(naam);
        this.sponsorNaam = sponsorNaam;
        this.sponsorSoort = sponsorSoort;
    }

    public String getSponsorNaam() {
        return sponsorNaam;
    }

    public SponsorSoortJeroen getSponsorSoort() {
        return sponsorSoort;
    }

    //6.3 Implementeer hier de methode van de interface (show),
    // ze toont alle informatie in verband met het team (zie verder voor een voorbeeld)
    public void show() {
        System.out.printf("TeamJeroen %s\nSponsor: %s (%s)\nLeden:\n",getNaam(),getSponsorNaam(),getSponsorSoort());
        for (SpelerJeroen speler :super.getSpelers() ) {
            System.out.println(speler);
        }
    }

    //6.4  Vul de methode verhaspelSponsorNaam aan.
    // De methode dient de volgorde van de letters van de sponsornaam willekeurig door elkaar te halen
    // en als String terug te geven.
    public String verhaspelSponsorNaam() {
        StringBuilder stringBuilder = new StringBuilder();
        ArrayList<Character> sponsornaam = new ArrayList<>();
        for (int i =0;i<getSponsorNaam().length();i++){
            sponsornaam.add(getSponsorNaam().charAt(i));
        }
        Collections.shuffle(sponsornaam);
        for (Character character : sponsornaam) {
            stringBuilder.append(character);
        }
        return stringBuilder.toString();
    }
}
