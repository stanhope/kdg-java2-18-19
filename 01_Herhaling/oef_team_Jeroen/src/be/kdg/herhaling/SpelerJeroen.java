package be.kdg.herhaling;

public class SpelerJeroen implements Comparable<SpelerJeroen>{
    private int rugNummer;
    private String naam;
    private AdresJeroen adres;

    // 2.1 Maak beide constructors + een getter voor het rugNummer

    public SpelerJeroen(int rugNummer, String naam, AdresJeroen adres) {
        this(rugNummer,naam,adres.getStraat(),adres.getPostNummer(),adres.getGemeente());
    }

    public SpelerJeroen(int rugNummer, String naam, String straat, int postNummer, String gemeente) {
        this.rugNummer = rugNummer;
        this.naam = naam;
        this.adres = new AdresJeroen(straat,postNummer,gemeente);
    }

    public int getRugNummer(){
        return rugNummer;
    }
    //2.2 Override de toString methode (zie gewenste uitvoer - werk met kolommen)

    @Override
    public String toString() {
        return String.format("%2d %-20s %s",rugNummer,naam,adres);
    }

    //2.3 Implementeer de Comparable interface, zodat spelers gesorteerd kunnen worden op naam
    @Override
    public int compareTo(SpelerJeroen o) {
        return naam.compareTo(o.naam);
    }
}
