package be.kdg.herhaling;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

//  3.2 Schrijf een interface met de naam ShowableJeroen met daarin één methode, de methode void show()
// Implementeer de interface in deze klasse (Let op! De methode zelf wordt hier niet uitgewerkt!)

public abstract class TeamJeroen implements ShowableJeroen {
    private String naam;
    private List<SpelerJeroen> spelers;

    // De volgende regel mag je niet wijzigen
    public TeamJeroen() {}

    //3.1 Schrijf de nodige constructor (de spelers worden achteraf toegevoegd) + een getter voor elk attribuut
    public TeamJeroen(String naam) {
        this.naam = naam;
        spelers = new LinkedList<>();
    }

    public String getNaam() {
        return naam;
    }

    public List<SpelerJeroen> getSpelers() {
        return spelers;
    }

    //3.3 Geef de speler met het gegeven rugNummer terug, geef null terug als de
	// speler niet gevonden is
	public SpelerJeroen zoekSpeler(int rugNummer) {
        for (SpelerJeroen speler : spelers) {
            if (speler.getRugNummer()==rugNummer){
                return speler;
            }
        }
		return null;
	}
    //3.4 Voeg een speler aan de arraylist toe, maar alleen als het rugNummer nog
		// niet voorkomt
    public void voegToe(SpelerJeroen toeTeVoegenSpeler) {
        for (SpelerJeroen speler : spelers) {
            if (toeTeVoegenSpeler.getRugNummer()==speler.getRugNummer()){
                return;
            }
        }
        spelers.add(toeTeVoegenSpeler);
    }

    //3.5 Sorteer de spelers volgens naam
    public void sort() {
        Collections.sort(spelers);
    }


}
