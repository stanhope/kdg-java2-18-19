package be.kdg.deelbaar;

public class Woord implements Deelbaar {
    private String str;

    public Woord(String str) {
        this.str = str;
    }

    public String toString() {
        return str;
    }

    @Override
    public Deelbaar getHelft() {
        return new Woord(str.substring(0, str.length() / 2));
    }
}
