package be.kdg.deelbaar;

public abstract class Getal implements Deelbaar {

    public abstract void increment(int step);

    public void decrement(int step) {
        increment(-step);
    }
}


