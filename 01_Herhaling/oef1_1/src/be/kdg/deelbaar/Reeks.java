package be.kdg.deelbaar;

public class Reeks implements Deelbaar {
    private int[] array;

    public Reeks(int[] array) {
        this.array = array;
    }

    public String toString() {
        StringBuilder str = new StringBuilder("[");
        for (int i = 0; i < array.length; i++) {
            str.append(array[i]);
            if (i < array.length - 1) str.append(", ");
        }
        return str + "]";
    }

    @Override
    public Deelbaar getHelft() {
        int[] halfArray = new int[this.array.length / 2];
        System.arraycopy(this.array, 0, halfArray, 0, halfArray.length);

        return new Reeks(halfArray);
    }
}
