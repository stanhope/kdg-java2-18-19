package be.kdg;

import be.kdg.persistence.Foto;
import be.kdg.persistence.Portfolio;
import be.kdg.persistence.data.Data;

import java.util.List;

public class RunDeel3 {
    private static final String JSON_BESTAND = "fotos.json";
    private static final String XML_BESTAND = "fotos.xml";

    public static void main(String[] args) {
        List<Foto> data = new Data().getList();
        Portfolio portfolio = new Portfolio();
        portfolio.setFotoLijst(data); // Vult lijst in


        // TODO (3.1)


        // TODO (3.2)


        // TODO (3.3)


        // TODO (3.4)
    }
}

/*
00053 A  2,8 1/1000  200
00041 P  4,0 1/200   100
00026 A 11,0 1/500   400
00053 A  2,8 1/1000  200
00127 P  1,8 1/1000  100
00547 A  8,0 1/1000  200
00013 P  3,5 1/1000  200
00016 A  5,6 1/60   1600
00045 A  5,6 1/125   200

00016 A  5,6 1/60   1600
00026 A 11,0 1/500   400
00053 A  2,8 1/1000  200
00045 A  5,6 1/125   200
00547 A  8,0 1/1000  200
 */