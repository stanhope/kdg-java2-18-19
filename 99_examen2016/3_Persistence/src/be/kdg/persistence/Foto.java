package be.kdg.persistence;


// Aan deze klasse mag je GEEN compareTo toevoegen!
// Andere aanvullingen zijn noodzakelijk!

public class Foto {
    public enum Soort {
        ACTION, PORTRAIT
    }

    private int nummer;
    private Soort soort;
    private double diafragma;
    private String sluitertijd;
    private int iso;

    public Foto(int nummer, Soort soort, double diafragma, String sluitertijd, int iso) {
        this.nummer = nummer;
        this.soort = soort;
        this.diafragma = diafragma;
        this.sluitertijd = sluitertijd;
        this.iso = iso;
    }

    public int getNummer() {
        return nummer;
    }

    public Soort getSoort() {
        return soort;
    }

    public double getDiafragma() {
        return diafragma;
    }

    public String getSluitertijd() {
        return sluitertijd;
    }

    public int getIso() {
        return iso;
    }

    @Override
    public String toString() {
        return String.format("%05d %s %4.1f %-6s %4d", nummer, (soort == Soort.ACTION ? "A" : "P"), diafragma, sluitertijd, iso);
    }
}
