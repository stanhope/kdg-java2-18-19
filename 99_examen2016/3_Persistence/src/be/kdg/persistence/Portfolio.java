package be.kdg.persistence;

// TODO (3.5)

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement(name = "portfolio")
public class Portfolio {
    private  List<Foto> fotoLijst;

    @XmlElement(name = "foto")
    public List<Foto> getFotoLijst() {
        return fotoLijst;
    }

    public void setFotoLijst(List<Foto> fotoLijst) {
        this.fotoLijst = fotoLijst;
    }
}



