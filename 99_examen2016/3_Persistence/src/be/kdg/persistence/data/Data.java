package be.kdg.persistence.data;


import be.kdg.persistence.Foto;

import java.util.ArrayList;
import java.util.List;

/**
 * HIER MAG JE NIETS WIJZIGEN
 */
public class Data {
    private List<Foto> list = new ArrayList<>();

    public List<Foto> getList() {
        list.add(new Foto(53, Foto.Soort.ACTION, 2.8, "1/1000", 200));
        list.add(new Foto(41, Foto.Soort.PORTRAIT, 4.0, "1/200", 100));
        list.add(new Foto(26, Foto.Soort.ACTION, 11.0, "1/500", 400));
        list.add(new Foto(53, Foto.Soort.ACTION, 2.8, "1/1000", 200));
        list.add(new Foto(127, Foto.Soort.PORTRAIT, 1.8, "1/1000", 100));
        list.add(new Foto(547, Foto.Soort.ACTION, 8.0, "1/1000", 200));
        list.add(new Foto(13, Foto.Soort.PORTRAIT, 3.5, "1/1000", 200));
        list.add(new Foto(16, Foto.Soort.ACTION, 5.6, "1/60", 1600));
        list.add(new Foto(45, Foto.Soort.ACTION, 5.6, "1/125", 200));
        return list;
    }
}
