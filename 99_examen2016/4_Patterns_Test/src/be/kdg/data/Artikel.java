package be.kdg.data;

import java.util.Objects;

// AAN DEZE KLASSE MAG JE NIETS WIJZIGEN
public class Artikel {
    private int nummer;
    private String naam;
    private int inStock;

    public Artikel(int nummer, String naam, int inStock) {
        this.nummer = nummer;
        this.naam = naam;
        this.inStock = inStock;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Artikel artikel = (Artikel) o;
        return nummer == artikel.nummer;
    }

    @Override
    public int hashCode() {
        return Objects.hash(nummer);
    }

    @Override
    public String toString() {
        return String.format("%4d %-10s %4d", nummer, naam, inStock);
    }

 }
