package be.kdg.data;

import java.util.ArrayList;
import java.util.List;

// AAN DEZE KLASSE MAG JE NIETS WIJZIGEN!
public class Artikels {
    private List<Artikel> list;

    public Artikels() {
        list = new ArrayList<>();
    }

    public void fillList() {
        addToList(new Artikel(1000, "Bloem", 200));
        addToList(new Artikel(1012, "Suiker", 500));
        addToList(new Artikel(1000, "Peper", 300));
        addToList(new Artikel(1004, "Boter", 300));
    }

    public List<Artikel> getList() {
        return list;
    }

    public boolean addToList(Artikel artikel) {
        if (list.contains(artikel)) {
            return false;
        } else {
            list.add(artikel);
        }
        return true;
    }

    public void clearList() {
        list.clear();
    }

    public int getSize() { // test aantal
        return list.size();
    }

    public List<Artikel> makeCopy() {
        List<Artikel> newList = new ArrayList<>();
        newList.addAll(list);
        return newList;
    }
}
