package be.kdg;

import be.kdg.data.Artikels;
import be.kdg.pattern.ArtikelObserver;
import be.kdg.pattern.ArtikelsObservable;

public class RunDeel4 {
    public static void main(String[] args) {
        Artikels artikels = new Artikels();
        artikels.fillList();
        System.out.println("Originele Artikellijst: ");
    // TODO (4.4)


        System.out.println("\nWerking observer:");
        ArtikelObserver observer = new ArtikelObserver();
        ArtikelsObservable artikelsObservable = new ArtikelsObservable();
    // TODO (4.4)

    // TODO verwijder de commentaar bij de volgende 2 regels
//        artikelsObservable.fillList();
//        artikelsObservable.clearList();
    }
}
