package be.kdg.concurrency;

/*
 * AAN DEZE KLASSE MAG JE NIETS WIJZIGEN! DUS OOK GEEN TOSTRING TOEVOEGEN!
 */
public class Klant {
     public enum Soort { ENKELVOUDIG, DAG_NACHT};

    private String naam;
    private Soort soort;
    private int klantNummer;
    private int meterstandDagOud;
    private int meterstandNachtOud;
    private int meterstandDagNieuw;
    private int meterstandNachtNieuw;

    public Klant(String naam, Soort soort, int klantNummer, int meterstandDagOud,
                 int meterstandNachtOud, int meterstandDagNieuw, int meterstandNachtNieuw) {
        this.naam = naam;
        this.soort = soort;
        this.klantNummer = klantNummer;
        this.meterstandDagOud = meterstandDagOud;
        this.meterstandNachtOud = meterstandNachtOud;
        this.meterstandDagNieuw = meterstandDagNieuw;
        this.meterstandNachtNieuw = meterstandNachtNieuw;
    }

    public String getNaam() {
        return naam;
    }

    public Soort getSoort() {
        return soort;
    }

    public int getKlantNummer() {
        return klantNummer;
    }

    int getMeterstandDagOud() {
        return meterstandDagOud;
    }

    int getMeterstandNachtOud() {
        return meterstandNachtOud;
    }

    int getMeterstandDagNieuw() {
        return meterstandDagNieuw;
    }

    int getMeterstandNachtNieuw() {
        return meterstandNachtNieuw;
    }
}
