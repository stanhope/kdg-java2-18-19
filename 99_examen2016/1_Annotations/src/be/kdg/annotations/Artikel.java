package be.kdg.annotations;

/*
 * HIER MAG JE NIETS AAN WIJZIGEN!
 * Het rood verdwijnt automatisch als je de annotation geschreven hebt!
 */

//@MyInfo(versie = 1.0)
public class Artikel {
    private String naam;
    private long voorraad;

    public Artikel(String naam, long voorraad) {
        this.naam = naam;
        this.voorraad = voorraad;
    }

    public String getNaam() {
        return naam;
    }

    public long getVoorraad() {
        return voorraad;
    }

    // ...

    //@MyInfo(naam = "Jos", versie = 2.0)
    public void doeIets() {

    }

    //@MyInfo(naam = "Hubert", versie = 1.5)
    public void doeIetsAnders() {

    }

}
