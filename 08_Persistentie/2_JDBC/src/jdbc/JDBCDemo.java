package jdbc;


import model.Student2;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class JDBCDemo {
    public static void main(String[] args) {
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        try {
            // STAP 1. Maak een connection met de database
            connection = DriverManager.getConnection("jdbc:hsqldb:file:08_Persistentie/data/JDBC/studenten", "sa", "");

            // STAP 2. Maak een statement klaar
            statement = connection.createStatement();
            statement.execute("DROP TABLE IF EXISTS studenten");
            String createQuery = "CREATE TABLE studenten " +
                    "(id INTEGER NOT NULL IDENTITY," +
                    "studnr INTEGER NOT NULL, " +
                    "naam VARCHAR(30) NOT NULL," +
                    "geboorte DATE," +
                    "woonplaats VARCHAR(40))";
            statement.execute(createQuery);

            // STAP3. Execute queries
            //zoek hieronder het foutje in een van de queries:
            String query1 = "INSERT INTO studenten VALUES (NULL, 111, 'Kasper', '"
                    + Date.valueOf("2000-8-15") + "', 'Berchem')";
            System.out.println("query1 = " + query1);
            int rowsAffected = statement.executeUpdate(query1);
            String query2 = "INSERT INTO studenten VALUES (NULL, 222, 'Melchior', '"
                    + Date.valueOf("1999-10-19") + "', 'Antwerpen')";
            System.out.println("query2 = " + query2);
            rowsAffected += statement.executeUpdate(query2);
            String query3 = "INSERT INTO studenten VALUES (NULL, 333, 'Balthazar', '"
                    + Date.valueOf("2001-3-2") + "', 'Kortrijk')";
            System.out.println("query3 = " + query3);
            rowsAffected += statement.executeUpdate(query3);
            System.out.println("rowsAffected: " + rowsAffected);

            rowsAffected = statement.executeUpdate("DELETE FROM studenten WHERE lower(woonplaats)='antwerpen';");
            System.out.println(rowsAffected);
            resultSet = statement.executeQuery("SELECT * FROM studenten");

            rowsAffected = statement.executeUpdate("UPDATE studenten SET naam='Random' WHERE id=0;");
            System.out.println(rowsAffected);

            // STAP 4. Verwerk de opgehaalde data
            List<Student2> myList = new ArrayList<>();
            resultSet = statement.executeQuery("SELECT * FROM studenten");
            while (resultSet.next()) {
                Student2 newStudent = new Student2(
                        resultSet.getInt("id"),
                        resultSet.getInt("studnr"),
                        resultSet.getString("naam"),
                        resultSet.getDate("geboorte").toLocalDate(),
                        resultSet.getString("woonplaats")
                );
                myList.add(newStudent);
            }
            System.out.println("opgehaalde data:");
            myList.forEach(System.out::println);

            // STAP 5. Verbreek de verbinding met de database
            resultSet.close();
            statement.close();
            connection.close();

        } catch (SQLException e) {
            System.out.println("SQLException: " + e.getMessage());
            System.out.println("SQLState = " + e.getSQLState());
            System.out.println("Error code = " + e.getErrorCode());
        } finally {
            try {
                if (resultSet != null) resultSet.close();
                if (statement != null) statement.close();
                if (connection != null) connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}


