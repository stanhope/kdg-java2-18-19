package model;

import java.time.LocalDate;

public class Student2 {

    private int id; //pk
    private int studNr;
    private String naam;
    private LocalDate geboorte;
    private String woonplaats;

    public Student2(int id, int studNr, String naam, LocalDate geboorte, String woonplaats) {
        this.id = id;
        this.studNr = studNr;
        this.naam = naam;
        this.geboorte = geboorte;
        this.woonplaats = woonplaats;
    }


    public Student2(int studNr, String naam, LocalDate geboorte, String woonplaats) {
        this(-1, studNr, naam, geboorte, woonplaats);
    }

    @Override
    public String toString() {
        return String.format("%-6d %-20s (°%s) --> %s", studNr, naam, geboorte, woonplaats);
    }
}