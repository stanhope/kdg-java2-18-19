package model;

import java.io.Serializable;
import java.time.LocalDate;

public class Student1 implements Serializable{
    private int studNr;
    private String naam;
    private LocalDate geboorte;
    private String woonplaats;

    public Student1(int studNr, String naam, LocalDate geboorte, String woonplaats) {
        this.studNr = studNr;
        this.naam = naam;
        this.geboorte = geboorte;
        this.woonplaats = woonplaats;
    }

    public Student1(int studNr, String naam, LocalDate geboorte) {
        this(studNr, naam, geboorte, "");
    }

    @Override
    public String toString() {
        return String.format("%-6d %-20s (°%s) --> %s", studNr, naam, geboorte, woonplaats);
    }
}
