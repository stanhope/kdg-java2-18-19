package be.kdg.patterns;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Deze klasse bevat algemene bewerkingen op de database,
 * zoals het laden van de driver, het leggen van een connectie,
 * aanmaken statement en afsluiten.
 * De andere dao-klassen erven hiervan over.
 *
 * Je moet hier in principe niets veranderen!
 */

public class HsqlDao {

    protected Connection maakConnectie() {
        Connection connection = null;
        try {
            connection = DriverManager.getConnection("jdbc:hsqldb:file:08_Persistentie/oef_tvandeve/DAO_Opgave_2/db/mijnDatabase", "sa", "");
        } catch (SQLException e) {
            System.err.println("Fatal error: cannot get a connection to the database");
            System.exit(1);
        }
        return connection;
    }

    protected Statement maakStatement(Connection connection) {
        Statement statement = null;
        try {
            statement = connection.createStatement();
        } catch (SQLException e) {
            System.err.println("Fatal error: cannot create statement");
            try {
                connection.close();
            } catch (SQLException e1) {    // empty
            }
            System.exit(1);
        }
        return statement;
    }

    protected void sluitStatementEnConnectie(Statement statement, Connection connection) {
        try {
            statement.execute("CHECKPOINT");
            statement.close();
            connection.close();
        } catch (SQLException e) {
            // do nothing, we don't need the connection anymore
        }
    }

    public void close() {
        try {
            Connection connection = maakConnectie();
            Statement statement = maakStatement(connection);
            statement.execute("SHUTDOWN COMPACT");
            sluitStatementEnConnectie(statement, connection);
        } catch (SQLException e) {
            // do nothing, database is not needed anymore
        }
    }
}
