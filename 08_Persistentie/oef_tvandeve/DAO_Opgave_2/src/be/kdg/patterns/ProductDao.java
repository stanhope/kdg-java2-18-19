package be.kdg.patterns;

import java.math.BigDecimal;
import java.sql.*;
import java.util.Set;
import java.util.HashSet;
import java.util.Collections;

/**
 * Deze klasse erft over van HsqlDao en kan dus de methoden van de superklasse oproepen.
 * De meeste methoden zijn klaar, maar een aantal moet je nog uitwerken.
 */
public class ProductDao extends HsqlDao {

    public ProductDao() {
        Connection connection = maakConnectie();
        Statement statement = maakStatement(connection);
        maakTabel(statement);
        sluitStatementEnConnectie(statement, connection);
    }

    private void maakTabel(Statement statement) {
        try {
            statement.execute("CREATE TABLE producten (id INTEGER IDENTITY, omschrijving CHAR(10), prijs DOUBLE, categorieId INTEGER)");
        } catch (SQLException e) {
            // no problem, table already exists
        }
    }

    public boolean clear() {
        Connection connection = null;
        Statement statement = null;
        try {
            connection = maakConnectie();
            statement = maakStatement(connection);
            statement.execute("DROP TABLE producten IF EXISTS");
            maakTabel(statement);
            sluitStatementEnConnectie(statement, connection);
            return true;
        } catch (SQLException e) {
            sluitStatementEnConnectie(statement, connection);
            return false;
        }
    }

    public boolean create(Product product) {
        Connection connection = null;
        Statement statement = null;
        try {
            if (product.getId() >= 0) return false;
            connection = maakConnectie();
            statement = maakStatement(connection);
            String omschrijving = product.getOmschrijving();
            double prijs = product.getPrijs();
            int categorieId = product.getCategorieId();
            int rowsAffected = statement.executeUpdate("INSERT INTO producten VALUES (NULL, '" + omschrijving + "', '" + prijs + "', " + categorieId + ")");
            if (rowsAffected != 1) {
                sluitStatementEnConnectie(statement, connection);
                return false;
            }
            ResultSet resultSet = statement.executeQuery("CALL IDENTITY()");
            if (!resultSet.next()) {
                sluitStatementEnConnectie(statement, connection);
                return false;
            }
            int id = resultSet.getInt(1);
            product.setId(id);
            sluitStatementEnConnectie(statement, connection);
            return true;
        } catch (SQLException e) {
            sluitStatementEnConnectie(statement, connection);
            return false;
        }
    }

    public boolean update(Product product) {
        Connection connection = null;
        Statement statement = null;
        try {
            if (product.getId() < 0) return false;
            connection = maakConnectie();
            statement = maakStatement(connection);
            int id = product.getId();
            String omschrijving = product.getOmschrijving();
            double prijs = product.getPrijs();
            int categorieId = product.getCategorieId();
            statement.executeUpdate("UPDATE producten SET omschrijving = '" + omschrijving + "' WHERE id=" + id );
            statement.executeUpdate("UPDATE producten SET prijs = '" + prijs + "' WHERE id=" + id );
            statement.executeUpdate("UPDATE producten SET categorieId = " + categorieId + " WHERE id=" + id );
            sluitStatementEnConnectie(statement, connection);
            return true;
        } catch (SQLException e) {
            sluitStatementEnConnectie(statement, connection);
            return false;
        }
    }

    public Product retrieve(int id) {
        Connection connection = null;
        Statement statement = null;
        try {
            connection = maakConnectie();
            statement = maakStatement(connection);
            ResultSet resultSet = statement.executeQuery("SELECT omschrijving, prijs, categorieId FROM producten WHERE id=" + id);
            if (!resultSet.next()) {
                sluitStatementEnConnectie(statement, connection);
                return null;
            }
            String omschrijving = resultSet.getString("omschrijving");
            double prijs = resultSet.getDouble("prijs");
            int categorieId = resultSet.getInt("categorieId");
            Product product = new Product(id, omschrijving, prijs, categorieId);
            sluitStatementEnConnectie(statement, connection);
            return product;
        } catch (SQLException e) {
            sluitStatementEnConnectie(statement, connection);
            return null;
        }
    }

    /**
     * WERK UIT
     * Zoek alle producten op die gelinkt zijn aan een bepaalde categorie.
     *
     * @param categorieId De categorie identifier
     * @return een set van producten
     */
    public Set<Product> retrieveByCategorie(int categorieId) {

        return  Collections.emptySet();
    }

    /**
     * WERK UIT
     * Verwijder het product met de meegegeven id.
     *
     * @param id de unieke identifier van het product
     * @return true indien succesvol verwijderd
     */
    public boolean delete(int id) {

        return false;
    }
}
