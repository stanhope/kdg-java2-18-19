package be.kdg.patterns;

import java.util.Collections;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * Hier mag je in principe niets wijzigen!
 */
public class Categorie {
    private int id;
    private String naam;
    private Set<Product> producten; //Elke categorie bevat een set gerelateerde producten

    public Categorie(String naam) {
        this(-1, naam);
    }

    Categorie(int id, String naam) {  //package-toegang
        this.id = id;
        this.naam = naam;
        this.producten = new HashSet<>();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNaam() {
        return naam;
    }

    public void setNaam(String naam) {
        this.naam = naam;
    }

    public void voegProductToe(Product product) {
        producten.add(product);
    }

    public void verwijderProduct(Product product) {
        producten.remove(product);
    }

    public Set<Product> getProducten() {
        return Collections.unmodifiableSet(producten);
    }

    void setProducten(Set<Product> products) {
        this.producten = products;
    }

    public String toString() {
        StringBuilder result = new StringBuilder("Categorie[" + id + ", " + naam + "]");
        for (Product product : producten) {
            result.append("\n\t").append(product);
        }
        return result.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Categorie categorie = (Categorie) o;
        return id == categorie.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
