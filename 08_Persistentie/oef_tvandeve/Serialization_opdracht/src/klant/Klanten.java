package klant;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

/**
 * Werk in deze klasse de methode serialize en deserialize uit.
 * Vergeet ook niet dat bepaalde klassen de Serializable interface moeten implementeren!
 */
public class Klanten implements Serializable{
    private static final String FILENAME = "08_Persistentie/oef_tvandeve/Serialization_opdracht/data/klantendata.txt";
    private Map<Integer, Klant> klantenMap = new HashMap<>();

    public void voegToe(Klant nieuw) {
        if (klantenMap.containsKey(nieuw.getKlantNr())) {
            System.out.println("KlantenNr is niet uniek");
            return;
        }
        klantenMap.put(nieuw.getKlantNr(), nieuw);

    }

    public Klant verwijderKlant(Klant oud) {
        return klantenMap.remove(oud.getKlantNr());
    }

    public void verwijderAlles() {
        klantenMap = new HashMap<>();
    }

    public Klant zoekKlant(Klant zoek) {
        return klantenMap.get(zoek.getKlantNr());   //null indien niet gevonden
    }

    public void updateKlant(Klant klant) {
        if (!klantenMap.containsKey(klant.getKlantNr())) {
            System.out.println("Klant komt niet voor!");
            return;
        }
        klantenMap.put(klant.getKlantNr(), klant); // overschrijven bestaande klantgegevens
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder("Klanten:\n");
        for (Klant k : klantenMap.values()) {
            builder.append("\t");
            builder.append(k.toString());
            builder.append("\n");
        }
        return builder.toString();
    }

    public void serialize() {
        // uitwerken, maak gebruik van de constante FILENAME
        try {
            ObjectOutputStream oos = new ObjectOutputStream(
                    new FileOutputStream(FILENAME));
            oos.writeObject(klantenMap);
            oos.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void deserialize() {
        // uitwerken, maak gebruik van de constante FILENAME
        try {
            ObjectInputStream ois = new ObjectInputStream(
                    new FileInputStream(FILENAME));
            klantenMap = (HashMap) ois.readObject();
            ois.close();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
