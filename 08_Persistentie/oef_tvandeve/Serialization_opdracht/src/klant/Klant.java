package klant;

import klant.Adres;

import java.io.Serializable;

public class Klant implements Serializable {
    private String naam;
    private Adres adres;
    private int klantNr;
    private static final long serialVersionUID= 123L;

    public Klant(String naam, Adres adres, int klantNr) {
        this.naam = naam;
        this.adres = adres;
        this.klantNr = klantNr;
    }

    public int getKlantNr() {
        return klantNr;
    }

    @Override
    public String toString() {
        return String.format("%d %s (%s)", klantNr, naam, adres.toString());
    }
}
