package be.kdg.laptop;

import java.util.*;

public final class LaptopMap {
    public enum SortCriterium {
        OP_NAAM, OP_PRIJS
    }
    private Map<String, Laptop> map;

    public LaptopMap() {
        map = new TreeMap<>();
    }

    public int getAantal() {
        return map.size();
    }

    public void voegToe(Laptop nieuw) {
        if (map.containsValue(nieuw)) {
            throw new IllegalArgumentException(nieuw.getNaam() + " komt reeds voor in map");
        }
        map.put(nieuw.getNaam(), nieuw);
    }

    public void verwijder(String naam) {
        Laptop laptop = getLaptop(naam);
        if (laptop != null) {
            map.remove(laptop.getNaam());
        }
    }

    public Laptop getLaptop(String naam) {
        return map.get(naam);
    }

    public List<Laptop> getList(SortCriterium criterium) {
        //Opdracht 4
        List<Laptop> laptopList = new ArrayList<>(map.values());

        if (criterium.equals(SortCriterium.OP_PRIJS)){
            Collections.sort(laptopList);
        } else {
            Collections.sort(laptopList, new PrijsComparator());
        }
        return Collections.unmodifiableList(laptopList);
    }

    
}
