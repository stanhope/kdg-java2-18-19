package patterns;

/**
 * Examen Programmeren 2 - Java
 * januari 2018
 */
public  class Dobbelsteen extends GetallenGenerator {
    private int id;

    protected Dobbelsteen(int id) {
        super(1, 6);
        this.id = id;
    }

    public int werp() {
        setChanged();
        notifyObservers("Dobbelsteen " + this.id + "geeft worp " + super.genereer());

        return super.genereer();
    }

    public int getId() {
        return id;
    }
}
