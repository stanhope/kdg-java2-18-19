package patterns;

/**
 * Examen Programmeren 2 - Java
 * januari 2018
 */
public class DobbelsteenFactory {
    public static final int MAX_AANTAL = 5;
    public static  int getal = 0;
    //Opgave 3.2

    public DobbelsteenFactory() {
    }

    public  static Dobbelsteen newDobbelSteen(int id){
        if(getal != MAX_AANTAL){
            getal++;
            return  new Dobbelsteen(id);


        }else{
            return null;
        }

}


}
