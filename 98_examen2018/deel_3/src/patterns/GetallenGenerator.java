package patterns;

import java.util.Observable;
import java.util.Random;

/**
 * Examen Programmeren 2 - Java
 * januari 2018
 */
public abstract class GetallenGenerator extends Observable {
    private Random random = new Random();
    private int min;
    private int max;

    public GetallenGenerator(int min, int max) {
        if (max >= min) {
            this.max = max;
            this.min = min;
        }
    }

    public int genereer() {
        return random.nextInt(max - min) + min;
    }
}