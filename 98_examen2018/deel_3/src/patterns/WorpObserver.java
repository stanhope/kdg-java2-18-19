package patterns;

import java.util.Observable;
import java.util.Observer;

/**
 * Examen Programmeren 2 - Java
 * januari 2018
 */
public class WorpObserver implements Observer  {
    //Opgave 3.3
    private Dobbelsteen dobbelsteen;

    public WorpObserver(Dobbelsteen dobbelsteen) {
        this.dobbelsteen= dobbelsteen;
    }

    public void update(Observable observable, Object object) {
     System.out.println(object );

    }
}
