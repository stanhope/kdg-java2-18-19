import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import patterns.Dobbelsteen;
import patterns.DobbelsteenFactory;
import patterns.WorpObserver;

import java.util.ArrayList;
import java.util.List;

/**
 * Examen Programmeren 2 - Java
 * januari 2018
 */
public class Test_3 {
    private static List<Dobbelsteen> myList = new ArrayList<>();

    //Opgave 3.4
    @BeforeClass
    public static void init() {
        myList.add(DobbelsteenFactory.newDobbelSteen(1));
        myList.add(DobbelsteenFactory.newDobbelSteen(2));
        myList.add(DobbelsteenFactory.newDobbelSteen(3));
        myList.add(DobbelsteenFactory.newDobbelSteen(4));
        myList.add(DobbelsteenFactory.newDobbelSteen(5));
    }

    @Test
    public void testFactory() {
        myList.add(DobbelsteenFactory.newDobbelSteen(6));
        Dobbelsteen d = myList.get(5);
        Assert.assertNull(d);
    }

    @Test
    public void testObserver() {

        for (Dobbelsteen d : myList) {
            if (d != null) {
                WorpObserver observer = new WorpObserver(d);
                d.addObserver(observer);
                d.werp();
                Assert.assertNotNull(d);
            }

        }

    }
}
