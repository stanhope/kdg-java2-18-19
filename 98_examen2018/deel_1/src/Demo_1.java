import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import data.Data;
import data.PilotenDao;
import model.F1Piloot;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.stream.Collectors.partitioningBy;

/**
 * Examen Programmeren 2 - Java
 * januari 2018
 * NAAM: Leonora Cunaj
 * KLASGROEP: INF201A
 */
public class Demo_1 {
    private static final String JSON_FILE = "98_examen2018/deel_1/files/piloten.json";
    private static final String JDBC_FILE = "98_examen2018/deel_1/files/db_piloten";

    public static void main(String[] args) {
        List<F1Piloot> myList = new ArrayList<>();
        //Opgave 1.1
       /* GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();

        try(BufferedReader data = new BufferedReader(new FileReader(JSON_FILE))){
            F1Piloot[] pap = gson.fromJson(data,F1Piloot[].class);
            List<F1Piloot> papegaaiList = Arrays.asList(pap);
            for(F1Piloot papegaai : papegaaiList){
                myList.add(papegaai);
            }
        } catch (IOException e){
            e.printStackTrace();
        }*/

     try {
          BufferedReader data = new BufferedReader(new FileReader(JSON_FILE));

          GsonBuilder builder = new GsonBuilder();
          Gson gson = builder.create();
          F1Piloot[] aapArray = gson.fromJson(data, F1Piloot[].class);

          for (F1Piloot papegaai : aapArray) {
              System.out.println(papegaai);
              myList.add(papegaai);
          }
      }  catch (IOException e){
        e.printStackTrace();
    }

       myList.forEach(System.out::println);

      //  myList = Data.getList();
        System.out.println("Na inlezen JSON:");
        //Opgave 1.2
        myList.forEach(System.out::println);
        //Opgave 1.3
        double gemiddelde = myList.stream().filter(x -> x.getAantalGewonnen()>=1).mapToDouble(F1Piloot::getLeeftijd).average().getAsDouble();
        System.out.printf("\nGemiddelde leeftijd van de winnaars: %.1f jaar\n", gemiddelde);

        //Opgave 1.4
  myList.stream().filter(x-> x.getTeam().toString().charAt(0) ==  'M').sorted((x,y)-> x.getNaam().compareTo(y.getNaam()))
               .collect(Collectors.groupingBy(F1Piloot::getTeam)).values().forEach(System.out::println);

    /*  myList.stream().filter(x-> x.getTeam().toString().charAt(0) ==  'M').sorted((x,y) -> x.getTeam().compareTo(y.getTeam()))
              .forEach(System.out::println);*/
        PilotenDao pilotenDao = new PilotenDao(JDBC_FILE);
        pilotenDao.voegPilotenToe(myList);
        pilotenDao.close();
    }
}
