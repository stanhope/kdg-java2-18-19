package model;

/**
 * Examen Programmeren 2 - Java
 * januari 2018
 * HIER MOET JE NIETS AANPASSEN
 */
public enum Teams {
    FER, FOR, HAA, MAN, MCL, MER, RED, REN, TOR, SAU, WIL;

    @Override
    public String toString() {
        switch (this) {
            case FER:
                return "Ferrari";
            case MCL:
                return "McLaren";
            case MER:
                return "Mercedes";
            case RED:
                return "Red Bull Racing";
            case FOR:
                return "Force India";
            case WIL:
                return "Williams";
            case TOR:
                return "Torro Rosso";
            case HAA:
                return "Haas";
            case REN:
                return "Renault";
            case MAN:
                return "Manor Racing";
            case SAU:
                return "Sauber";
        }
        return null;
    }
}
