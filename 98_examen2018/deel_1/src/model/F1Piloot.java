package model;

import java.time.LocalDate;
import java.time.Period;

/**
 * Examen Programmeren 2 - Java
 * januari 2018
 *
 * HIER MOET JE NIETS AANPASSEN
 */
public class F1Piloot {
    private String naam;
    private Teams team;
    private LocalDate geboorteDatum;
    private int aantalGewonnen;
    private String land;

    public F1Piloot(String naam, Teams team, LocalDate geboorteDatum, int aantalGewonnen, String land) {
        this.naam = naam;
        this.team = team;
        this.geboorteDatum = geboorteDatum;
        this.aantalGewonnen = aantalGewonnen;
        this.land = land;
    }

    public String getNaam() {
        return naam;
    }

    public Teams getTeam() {
        return team;
    }

    public LocalDate getGeboorteDatum() {
        return geboorteDatum;
    }

    public int getAantalGewonnen() {
        return aantalGewonnen;
    }

    public String getLand() {
        return land;
    }

    public int getLeeftijd() {
        return Period.between(geboorteDatum, LocalDate.now()).getYears();
    }

    @Override
    public String toString() {
        return String.format("Naam: %-20s Team: %-15s°%8s   Wins: %2d  Herkomst: %-15s"
                , naam, team, geboorteDatum, aantalGewonnen, land);
    }
}
