package data;

import model.F1Piloot;

import java.sql.*;
import java.util.List;

/**
 * Examen Programmeren 2 - Java
 * januari 2018
 */
public class PilotenDao {
    private Connection connection;

    public PilotenDao(String databasePath) {
        maakConnectie(databasePath);
        maakTabel();
    }

    private void maakConnectie(String databasePath) {
        try {
            connection = DriverManager.getConnection("jdbc:hsqldb:file:" + databasePath, "sa", "");
            //System.out.println("Connection gemaakt");
        } catch (SQLException e) {
            System.err.println("Kan geen connectie maken met database " + databasePath);
            System.exit(1);
        }
    }

    private void maakTabel() {
        try {
            Statement statement = connection.createStatement();
            statement.execute("DROP TABLE pilotTable IF EXISTS");
            statement.execute("CREATE TABLE pilotTable (id INTEGER IDENTITY, naam VARCHAR(30), team VARCHAR(20), aantalGewonnen INTEGER)");
            System.out.println("Database aangemaakt");
        } catch (SQLException e) {
            String message = e.getMessage();
            if (message.contains("Tabel bestaat al")) return;
            System.err.println("Onverwachte fout bij aanmaken tabel: " + e.getMessage());
            System.exit(1);
        }
    }

    public void close() {
        if (connection == null) return;
        try {
            Statement statement = connection.createStatement();
            statement.execute("SHUTDOWN COMPACT");
            statement.close();
            connection.close();
            System.out.println("Database gesloten");
        } catch (SQLException e) {
            System.out.println("Probleem bij sluiten van database: " + e.getMessage());
        }
    }

    public void voegPilotenToe(List<F1Piloot> myList) {

            //Opgave 1.5
            try (PreparedStatement prep = connection.prepareStatement
                    ("INSERT INTO pilotTable(id,naam ,team,aantalGewonnen) VALUES(NULL,?,?,?)")) {
for(F1Piloot pap : myList){
    prep.setString(1, pap.getNaam());
    prep.setString(2, pap.getTeam().name());
    prep.setInt(3, pap.getAantalGewonnen());
    prep.executeUpdate();
}

        } catch (SQLException e) {
            System.err.println("Fout bij toevoegen: " + e);
        }
    }
}
