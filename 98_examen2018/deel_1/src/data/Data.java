package data;

import model.F1Piloot;
import model.Teams;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * Examen Programmeren 2 - Java
 * januari 2018
 *
 * HIER MOET JE NIETS AANPASSEN
 */
public class Data {

    private static List<F1Piloot> pilotList = new ArrayList<>();

    public static List<F1Piloot> getList() {
        pilotList.add(new F1Piloot("Nico Rosberg", Teams.MER, LocalDate.of(1985,6,27), 22, "Duitsland"));
        pilotList.add(new F1Piloot("Lewis Hamilton", Teams.MER, LocalDate.of(1985,1,7), 49, "Verenigd Koninkrijk"));
        pilotList.add(new F1Piloot("Daniel Ricciardo", Teams.RED, LocalDate.of(1989,7,1), 4, "Australië"));
        pilotList.add(new F1Piloot("Kimi Räikkönen", Teams.FER, LocalDate.of(1979,10,17), 20, "Finland"));
        pilotList.add(new F1Piloot("Sebastian Vettel", Teams.FER, LocalDate.of(1987,7,3), 42, "Duitsland"));
        pilotList.add(new F1Piloot("Max Verstappen", Teams.RED, LocalDate.of(1997,9,30), 1, "België"));
        pilotList.add(new F1Piloot("Valtteri Bottas", Teams.WIL, LocalDate.of(1989,8,28), 0, "Finland"));
        pilotList.add(new F1Piloot("Sergio Perez", Teams.FOR, LocalDate.of(1990,1,26), 3, "Mexico"));
        pilotList.add(new F1Piloot("Nico Hulkenberg", Teams.FOR, LocalDate.of(1987,8,19), 0, "Duitsland"));
        pilotList.add(new F1Piloot("Fernando Alonso", Teams.MCL, LocalDate.of(1981,7,29), 32, "Spanje"));
        pilotList.add(new F1Piloot("Felipe Massa", Teams.WIL, LocalDate.of(1981,4,25), 11, "Brazilië"));
        pilotList.add(new F1Piloot("Carlos Sainz", Teams.TOR, LocalDate.of(1994,9,1), 4, "Spanje"));
        pilotList.add(new F1Piloot("Romain Grosjean", Teams.HAA, LocalDate.of(1986,4,17), 9, "Zwitserland"));
        pilotList.add(new F1Piloot("Daniil Kvyat", Teams.TOR, LocalDate.of(1994,4,26), 0, "Rusland"));
        pilotList.add(new F1Piloot("Jenson Button", Teams.MCL, LocalDate.of(1980,1,19), 15, "Verenigd Koninkrijk"));
        pilotList.add(new F1Piloot("Kevin Magnussen", Teams.REN, LocalDate.of(1992,10,5), 0, "Denemarken"));
        pilotList.add(new F1Piloot("Jolyon Palmer", Teams.REN, LocalDate.of(1991,1,20), 2, "Verenigd Koninkrijk"));
        pilotList.add(new F1Piloot("Pascal Wehrlein", Teams.MAN, LocalDate.of(1994,10,18), 10, "Duitsland"));
        pilotList.add(new F1Piloot("Stoffel Vandoorne", Teams.MCL, LocalDate.of(1992,3,26), 5, "België"));
        pilotList.add(new F1Piloot("Esteban Gutierrez", Teams.HAA, LocalDate.of(1991,8,5), 11, "Mexico"));
        pilotList.add(new F1Piloot("Marcus Ericsson", Teams.SAU, LocalDate.of(1990,9,2), 0, "Zweden"));
        pilotList.add(new F1Piloot("Felipe Nasr", Teams.SAU, LocalDate.of(1991,8,21), 14, "Brazilië"));
        pilotList.add(new F1Piloot("Rio Haryanto", Teams.MAN, LocalDate.of(1992,1,22), 1, "Indonesië"));
        pilotList.add(new F1Piloot("Esteban Ocon", Teams.MAN, LocalDate.of(1995,9,17), 2, "Frankrijk"));

        return pilotList;
    }
}
