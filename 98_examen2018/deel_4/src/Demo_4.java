import multithreading.Depot;
import multithreading.RunnableGiver;
import multithreading.RunnableTaker;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Examen Programmeren 2 - Java
 * januari 2018
 */
public class Demo_4 {
    public static void main(String[] args) {
        Random random = new Random();
        List<Integer> myList = Stream
                .generate(() -> random.nextInt(100))
                .limit(100)
                .collect(Collectors.toList());

        Depot depot = new Depot(myList);
        Thread t1 = new Thread(new RunnableGiver(depot));
        Thread t2 = new Thread(new RunnableTaker(depot));

        System.out.println("Voor de threads: in depot: " + depot.getSize());

        t1.start();
        t2.start();
        try {
            t1.join();
            t2.join();
        }catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("Na de threads: in depot: " + depot.getSize());
    }
}
