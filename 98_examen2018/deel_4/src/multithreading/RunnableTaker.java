package multithreading;

/**
 * Examen Programmeren 2 - Java
 * januari 2018
 */
public class RunnableTaker implements Runnable {
    private Depot depot;

    public RunnableTaker(Depot depot) {
        this.depot = depot;
    }

    @Override
    public synchronized void run() {
        for (int i = 0; i < 1000; i++) {
            depot.remove();
        }
        System.out.println("\tRunnableTaker beeindigd");
    }
}
