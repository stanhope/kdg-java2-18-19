package multithreading;

import java.util.Random;

/**
 * Examen Programmeren 2 - Java
 * januari 2018
 */
public class RunnableGiver implements Runnable {
    private Depot depot;

    public RunnableGiver(Depot depot) {
        this.depot = depot;
    }

    @Override
    public void run() {
        Random random = new Random();
        for (int i = 0; i < 1000; i++) {
            depot.add(random.nextInt(100)); //getallen 0..99
        }
        System.out.println("\tRunnableGiver beeindigd");
    }
}
