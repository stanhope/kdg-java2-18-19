package multithreading;

import java.util.List;
import java.util.Random;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.SynchronousQueue;

/**
 * Examen Programmeren 2 - Java
 * januari 2018
 */
public class Depot {
    private List<Integer> getallenList;
    private Random random = new Random();

    public Depot(List<Integer> getallenList) {
        this.getallenList = getallenList;
    }

    public synchronized void remove() {
        //Verwijder een getal op een random positie:
        int pos = random.nextInt(getallenList.size());
        getallenList.remove(pos);
    }

    public void add(int getal) {
        getallenList.add(getal);
    }

    public int getSize() {
        return getallenList.size();
    }
}
