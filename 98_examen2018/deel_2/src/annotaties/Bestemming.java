package annotaties;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by leonora on 18/01/2018.
 */
//@Target(value = ElementType.METHOD)
@Retention(value = RetentionPolicy.RUNTIME)
public @interface Bestemming {
    String value()
            default "onbekend";
    double prijs() default  0.0;
}
