package annotaties;

/**
 * Examen Programmeren 2 - Java
 * januari 2018
 *
 * HIER MOET JE NIETS AANPASSEN;
 * het rood verdwijnt vanzelf na het oplossen van opgave 2.1
 */
@Bestemming
public class Vakantie {
    private int jaar;
    private int aantalDagen;

    public Vakantie(int jaar, int aantalDagen) {
        this.jaar = jaar;
        this.aantalDagen = aantalDagen;
    }

    @Bestemming(value = "Benidorm", prijs = 263.50)
    public int getJaar() {
        return jaar;
    }

    @Bestemming(prijs = 402.50)
    public void setJaar() {
        this.jaar = jaar;
    }

    @Bestemming("Balearen")
    public int getAantalDagen() {
        return aantalDagen;
    }

    public void setAantalDagen(int aantalDagen) {
        this.aantalDagen = aantalDagen;
    }
}
