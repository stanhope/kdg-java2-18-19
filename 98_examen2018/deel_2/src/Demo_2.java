import annotaties.Bestemming;
import annotaties.Vakantie;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;

/**
 * Examen Programmeren 2 - Java
 * januari 2018
 */
public class Demo_2 {
    public static void main(String[] args) {
        double totaal = 0.0;
        StringBuilder builder =  new StringBuilder("Ik ga op reis naar ");
        //Opgave 2.2
        for (Method m : Vakantie.class.getMethods()) {
            for (Bestemming a : m.getAnnotationsByType(Bestemming.class)) {
                if(!a.value().equals("onbekend")){
                    builder.append(a.value() + " ");
                }

                totaal += a.prijs();
        }
        }



        System.out.println(builder);
        System.out.printf("Totale prijs : %.2f€", totaal);
    }
}
