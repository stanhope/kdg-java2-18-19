package liveness;

public class Chopstick {
    private boolean taken = false;

    //Synchronized, want slechts één thread tegelijkertijd!
    public synchronized boolean pickUp() {
        if (!taken) {
            taken = true;
            return true;
        } else return false;
    }

    //Synchronized, want slechts één thread tegelijkertijd!
    public synchronized void putDown() {
        taken = false;
    }

    public boolean isTaken() {
        return taken;
    }
}