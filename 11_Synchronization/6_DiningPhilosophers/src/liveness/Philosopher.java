package liveness;

import java.util.Random;

class Philosopher implements Runnable {
    private Random random = new Random();
    private final Chopstick left;
    private final Chopstick right;
    private final String id;
    private boolean hasEaten;

    public Philosopher(Chopstick left, Chopstick right, String id) {
        this.left = left;
        this.right = right;
        this.id = id;
    }

    @Override
    public void run() {
        for (int i = 0; i < 10; i++) {
            eat();
            think();
        }
    }

    public void eat() {
        if (left.pickUp()) {
            if (right.pickUp()) { // enkel eten als recht en links vrij zijn
                chew();
                putDown();
            } else {
                left.putDown();
            }
        }
    }

    public void chew() {
        try {
            System.out.println("Philosopher " + id + " is eating");
            Thread.sleep(400); //Tijd nodig om te eten
            hasEaten = true;
        } catch (InterruptedException e) {
            // leeg
        }
    }

    public void putDown() {
        left.putDown();
        right.putDown();
    }

    private void think() {
        try {
            System.out.println("Philosopher " + id + " is thinking");
            Thread.sleep(random.nextInt(1000)); //Tijd om na te denken
        } catch (InterruptedException e) {
            // leeg
        }
    }

    public boolean isStarved() {
        return !hasEaten;
    }

    public String getId() {
        return id;
    }
}

