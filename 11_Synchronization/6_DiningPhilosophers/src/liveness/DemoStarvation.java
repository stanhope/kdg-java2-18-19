package liveness;

public class DemoStarvation {

        public static Object waitObject = new Object();

        public static void main(String[] args) {

            Chopstick[] chopsticks = new Chopstick[5]; // een array van 5 vorken
            Thread[] threads = new Thread[5]; // een array van 5 threads
            Philosopher[] philosophers = new Philosopher[5];

            for (int i = 0; i < 5; i++) {
                chopsticks[i] = new Chopstick();
            }

            for (int i = 0; i < 5; i++) {
                //Elke filosoof krijgt 2 vorken toegewezen (links en rechts):
                philosophers[i] = new Philosopher(chopsticks[i] , chopsticks[(i+1) % 5], i + "");
                threads[i] = new Thread(philosophers[i]);
                threads[i].start();
            }

            //Wachten tot iedereen gedaan heeft:
            for (Thread thread : threads) {
                try {
                    thread.join();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            //Is er iemand uitgehongerd?
            System.out.println();
            for (Philosopher philosopher : philosophers) {
                if(philosopher.isStarved()) {
                    System.out.println("Philosopher " + philosopher.getId() + " is starved!");
                }
            }
        }
    }




