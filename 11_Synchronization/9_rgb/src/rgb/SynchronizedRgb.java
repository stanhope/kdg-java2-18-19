package rgb;

/**
 * Gesynchronizeerde versie van de kleurenklasse Rgb; verschillende stukken code werden threadsafe gemaakt.
 * https://docs.oracle.com/javase/tutorial/essential/concurrency/syncrgb.html
 */
public class SynchronizedRgb {
    // Kleurwaarden moeten tussen 0 and 255 liggen.
    private int red;
    private int green;
    private int blue;
    private String name;

    public SynchronizedRgb(int red, int green, int blue, String name) {
        check(red, green, blue);
        this.red = red;
        this.green = green;
        this.blue = blue;
        this.name = name;
    }

    private void check(int red, int green, int blue) {
        if (red < 0 || red > 255
                || green < 0 || green > 255
                || blue < 0 || blue > 255) {
            throw new IllegalArgumentException("Foutieve kleurwaarde");
        }
    }

    public void set(int red, int green, int blue, String name) {
        check(red, green, blue);
        synchronized(this) {
            this.red = red;
            this.green = green;
            this.blue = blue;
            this.name = name;
        }
    }

    public synchronized int getRgb() {
        //Creeer kleuwaarde uit de 3 ints door de bitwaarden naar links te shiften:
        return ((red << 16) | (green << 8) | blue);
    }

    public synchronized String getName() {
        return name;
    }

    public synchronized void invert() {
        red = 255 - red;
        green = 255 - green;
        blue = 255 - blue;
        name = "Inverse of " + name;
    }

    @Override
    public String toString() {
        return String.format("%s (%d, %d, %d)", name, red, green, blue);
    }
}

