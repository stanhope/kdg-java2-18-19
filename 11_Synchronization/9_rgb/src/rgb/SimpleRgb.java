package rgb;

/**
 * Naieve versie van de kleurenklasse Rgb; geen synchronization, niet immutable
 */
public class SimpleRgb
{
    // Kleurwaarden moeten tussen 0 and 255 liggen.
    private int red;
    private int green;
    private int blue;
    private String name;

    public SimpleRgb(int red, int green, int blue, String name) {
        check(red, green, blue);
        this.red = red;
        this.green = green;
        this.blue = blue;
        this.name = name;
    }

    private void check(int red, int green, int blue) {
        if (red < 0 || red > 255
                || green < 0 || green > 255
                || blue < 0 || blue > 255) {
            throw new IllegalArgumentException("Foutieve kleurwaarde");
        }
    }

    public void set(int red, int green, int blue, String name) {
        check(red, green, blue);
        this.red = red;
        this.green = green;
        this.blue = blue;
        this.name = name;
    }

    public int getRgb() {
        //Creeer kleuwaarde uit de 3 ints door de bitwaarden naar links te shiften:
        return ((red << 16) | (green << 8) | blue);
    }

    public String getName() {
        return name;
    }

    public void invert() {
        red = 255 - red;
        green = 255 - green;
        blue = 255 - blue;
        name = "Inverse of " + name;
    }

    @Override
    public String toString() {
        return String.format("%s (%d, %d, %d)", name, red, green, blue);
    }
}

