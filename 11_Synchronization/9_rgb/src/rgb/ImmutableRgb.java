package rgb;

/**
 * Immutable versie van de kleurenklasse Rgb.
 * Aanpassingen: geen setters meer, de invert-methode werd aangepast
 * zodat ze een nieuw object maakt ipv het bestaande object te wijzigen.
 * Alle attributen en de klasse zelf werden final gemaakt.
 * Objecten die via parameters binnenkome moeten normaal nieuw worden aangemaakt,
 * maar hier is dat geen probleem want String zelf is ook immutable
 *
 * https://docs.oracle.com/javase/tutorial/essential/concurrency/imstrat.html
 */
public final class ImmutableRgb {
    // Kleurwaarden moeten tussen 0 and 255 liggen.
    private final int red;
    private final int green;
    private final int blue;
    private final String name;

    public ImmutableRgb(int red, int green, int blue, String name) {
        check(red, green, blue);
        this.red = red;
        this.green = green;
        this.blue = blue;
        this.name = name;
    }

    private void check(int red, int green, int blue) {
        if (red < 0 || red > 255
                || green < 0 || green > 255
                || blue < 0 || blue > 255) {
            throw new IllegalArgumentException("Foutieve kleurwaarde");
        }
    }

    /*
    Geen setters!
    public void set(int red, int green, int blue, String name) {
        check(red, green, blue);
        this.red = red;
        this.green = green;
        this.blue = blue;
        this.name = name;
    }
    */

    public int getRgb() {
        //Creeer kleuwaarde uit de 3 ints door de bitwaarden naar links te shiften:
        return ((red << 16) | (green << 8) | blue);
    }

    public String getName() {
        return name;
    }

    public ImmutableRgb invert() {
        return new ImmutableRgb(255 - red,
                255 - green,
                255 - blue,
                "Inverse of " + name);
    }

    @Override
    public String toString() {
        return String.format("%s (%d, %d, %d)", name, red, green, blue);
    }
}

