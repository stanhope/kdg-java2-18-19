import rgb.ImmutableRgb;
import rgb.SimpleRgb;
import rgb.SynchronizedRgb;

public class DemoRgb {
    public static void main(String[] args) {
        SimpleRgb mySimpleColor = new SimpleRgb(0, 0, 0, "Pitch Black");
        int myColorInt = mySimpleColor.getRgb();
        String myColorName = mySimpleColor.getName();
        mySimpleColor.invert();
        System.out.println("\nSimpleColor:");
        System.out.println("myColorName = " + myColorName);
        System.out.println("myColorInt = " + myColorInt);
        System.out.println("Inverted: " + mySimpleColor);

        SynchronizedRgb mySynchronizedColor = new SynchronizedRgb(0, 0, 0, "Pitch Black");
        myColorInt = mySynchronizedColor.getRgb();
        myColorName = mySynchronizedColor.getName();
        mySynchronizedColor.invert();
        System.out.println("\nSynchronizedColor:");
        System.out.println("myColorName = " + myColorName);
        System.out.println("myColorInt = " + myColorInt);
        System.out.println("Inverted: " + mySimpleColor);

        ImmutableRgb myImmutableColor = new ImmutableRgb(0, 0, 0, "Pitch Black");
        myColorInt = myImmutableColor.getRgb();
        myColorName = myImmutableColor.getName();
        ImmutableRgb inverted = myImmutableColor.invert();
        System.out.println("\nImmutableColor:");
        System.out.println("myColorName = " + myColorName);
        System.out.println("myColorInt = " + myColorInt);
        System.out.println("Inverted: " + inverted);

    }
}
