
/**
 * @author Senne Verhaegen
 * @version 1.0 15/01/2019 15:42
 */

public class Carwash3 {
    private boolean bezet = false;

    public synchronized void aankomstWagen(int wagenNummer) throws InterruptedException {
        if (bezet) {
            System.out.printf("Wagen nr %d moet wachten!\n", wagenNummer);
            wait();
        }
        bezet = true;
    }

    public synchronized void vertrekWagen(int wagenNummer) {
        System.out.printf("Wagen nr %d klaar\n", wagenNummer);
        bezet = false;
        notifyAll();
    }
}
