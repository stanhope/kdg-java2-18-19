/**
 * @author Senne Verhaegen
 * @version 1.0 15/01/2019 15:44
 */

public class RunCarwash3 {
    public static void main(String[] args) {
        Carwash3 carwash = new Carwash3();
        Car3[] cars = {
                new Car3(1, 0, carwash),
                new Car3(2, 1, carwash),
                new Car3(3, 3, carwash),
                new Car3(4, 7, carwash),
                new Car3(5, 8, carwash),
                new Car3(6, 9, carwash)
        };
        for (Car3 car : cars) {
            new Thread(car).start();
        }
    }
}
