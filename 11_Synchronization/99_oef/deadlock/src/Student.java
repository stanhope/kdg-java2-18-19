/**
 * @author Senne Verhaegen
 * @version 1.0 15/01/2019 17:05
 */


public class Student implements Runnable {
    private final String naam;
    private final String oudeVriendin;
    private final String nieuweVriendin;

    public Student(String naam, String oudeVriendin, String nieuweVriendin) {
        this.naam = naam;
        this.oudeVriendin = oudeVriendin;
        this.nieuweVriendin = nieuweVriendin;
    }

    public void oudeVriendin() {
        synchronized (oudeVriendin) {
            System.out.println(naam + " maakt het af met " + oudeVriendin);
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                // negeer
            }
        }
    }

    public synchronized void nieuweVriendin() {
        synchronized (nieuweVriendin) {
            System.out.println(naam + " vraagt het aan bij "
                    + nieuweVriendin);
        }
    }

    public void run() {
        System.out.println(naam + " belt " + oudeVriendin);
        oudeVriendin();
        nieuweVriendin();
        System.out.println(naam + " heeft een nieuwe vriendin.");
    }
}
