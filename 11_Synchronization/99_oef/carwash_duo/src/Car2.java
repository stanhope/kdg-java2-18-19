/**
 * @author Senne Verhaegen
 * @version 1.0 15/01/2019 15:43
 */

public class Car2 implements Runnable {
    private int wagenNummer;
    private int aankomstTijd;
    private Carwash2 carwash;

    public Car2(int wagenNummer, int aankomstTijd, Carwash2 carwash) {
        this.wagenNummer = wagenNummer;
        this.aankomstTijd = aankomstTijd;
        this.carwash = carwash;
    }

    @Override
    public void run() { // Deze methode volledig aan te vullen!
        try {
            Thread.sleep(1000 * aankomstTijd);
            System.out.printf("Wagen nr %d komt aan.\n", wagenNummer);
            carwash.aankomstWagen(wagenNummer);
            System.out.printf("Start wagen nr %d\n", wagenNummer);
            Thread.sleep(1600);
            carwash.vertrekWagen(wagenNummer);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
