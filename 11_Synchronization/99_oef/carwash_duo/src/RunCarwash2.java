import java.util.Arrays;

/**
 * @author Senne Verhaegen
 * @version 1.0 15/01/2019 15:44
 */

public class RunCarwash2 {
    public static void main(String[] args) {
        Carwash2 carwash = new Carwash2();
        Car2[] cars = {
                new Car2(1, 0, carwash),
                new Car2(2, 1, carwash),
                new Car2(3, 1, carwash),
                new Car2(4, 2, carwash),
                new Car2(5, 3, carwash),
                new Car2(6, 5, carwash),
                new Car2(7, 7, carwash),
                new Car2(8, 8, carwash),
                new Car2(9, 9, carwash)
        };
        Arrays.stream(cars).forEach(c -> new Thread(c).start());
    }
}
