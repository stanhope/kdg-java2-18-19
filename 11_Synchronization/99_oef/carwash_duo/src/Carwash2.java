/**
 * @author Senne Verhaegen
 * @version 1.0 15/01/2019 15:42
 */

public class Carwash2 {
    private boolean bezet1 = false;
    private boolean bezet2 = false;

    public synchronized void aankomstWagen(int wagenNummer) throws InterruptedException {
        // Beide bezet
        if (bezet1 && bezet2) {
            System.out.printf("Wagen nr %d moet wachten!\n", wagenNummer);
            wait();
        }

        // Beide vrij
        if (!bezet1 && !bezet2) bezet1 = true;

            // 1 vrij
        else if (!bezet1 && bezet2) bezet1 = true;

            // 1 vrij
        else if (!bezet2 && bezet1) bezet2 = true;

    }

    // Terug plaats vrijgeven
    public synchronized void vertrekWagen(int wagenNummer) {
        System.out.printf("Wagen nr %d klaar\n", wagenNummer);

        // Beide bezet
        if (bezet1 && bezet2) bezet1 = false;

        // 1 bezet
        else if (!bezet1 && bezet2) bezet2 = false;

        // 1 bezet
        else if (!bezet2 && bezet1) bezet1 = false;

        notify();
    }

    /**
     * Verwachte output:
     * For some reason zijn 2 en 3 omgedraaid...
     *
     * Wagen nr 1 komt aan.
     * Start wagen nr 1
     * Wagen nr 3 komt aan.
     * Start wagen nr 3
     * Wagen nr 2 komt aan.
     * Wagen nr 2 moet wachten!
     * Wagen nr 1 klaar
     * Start wagen nr 2
     * Wagen nr 4 komt aan.
     * Wagen nr 4 moet wachten!
     * Wagen nr 3 klaar
     * Start wagen nr 4
     * Wagen nr 5 komt aan.
     * Wagen nr 5 moet wachten!
     * Wagen nr 2 klaar
     * Start wagen nr 5
     * Wagen nr 4 klaar
     * Wagen nr 5 klaar
     * Wagen nr 6 komt aan.
     * Start wagen nr 6
     * Wagen nr 6 klaar
     * Wagen nr 7 komt aan.
     * Start wagen nr 7
     * Wagen nr 8 komt aan.
     * Start wagen nr 8
     * Wagen nr 7 klaar
     * Wagen nr 9 komt aan.
     * Start wagen nr 9
     * Wagen nr 8 klaar
     * Wagen nr 9 klaar
     */
}
