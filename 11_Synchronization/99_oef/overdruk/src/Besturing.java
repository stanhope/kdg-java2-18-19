/**
 * @author Senne Verhaegen
 * @version 1.0 15/01/2019 17:23
 */

public class Besturing extends Thread {
    private static final int TOENAME = 15;
    private static final Object LOCK = new Object();

    public synchronized void verhoogDruk(int toename) {
        if (Stoomketel.getDrukWaarde() < Stoomketel.getAlarmWaarde() - TOENAME) {
            try {
                sleep(100);
            } catch (InterruptedException e) {
                // negeer
            }
            Stoomketel.verhoogDruk(toename);
        }
    }

    @Override
    public void run() {
        synchronized (LOCK) {
            verhoogDruk(TOENAME);
        }
    }
}
