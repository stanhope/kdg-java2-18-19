/**
 * @author Senne Verhaegen
 * @version 1.0 15/01/2019 17:23
 */

public class RunBesturing {
    private static final int AANTAL_THREADS = 10;

    public static void main(String[] args) {
        Besturing[] threads = new Besturing[AANTAL_THREADS];
        for (int i = 0; i < AANTAL_THREADS; i++) {
            threads[i] = new Besturing();
            threads[i].start();
        }
        try {
            for (int i = 0; i < AANTAL_THREADS; i++) {
                threads[i].join();
            }
        } catch (InterruptedException e) { // negeer
        }
        System.out.println("De druk is nu " + Stoomketel.getDrukWaarde()
                + ", de alarmwaarde is " + Stoomketel.getAlarmWaarde());
    }
}
