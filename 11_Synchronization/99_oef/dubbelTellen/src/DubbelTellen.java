/**
 * @author Senne Verhaegen
 * @version 1.0 15/01/2019 15:31
 */

public class DubbelTellen {
    public static void main(String[] args) throws InterruptedException {
        Runnable teller = () -> {
            int i = 1;
            while (i <= 10) {
                System.out.print(i + " ");
                i++;
                try {
                    Thread.sleep(200);
                } catch (InterruptedException e) {
                    //
                }
            }
        };
        Thread t1 = new Thread(teller);
        Thread t2 = new Thread(teller);
        t1.start();
        t1.join();
        t2.start();
    }
}
