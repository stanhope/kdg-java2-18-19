package carwash;

/**
 * Bij deze opdracht simuleren we een carwash met
 * één wasstraat waarin zich slechts 1 wagen
 * tegelijk kan bevinden. Je kunt dit vergelijken met
 * een queue waarbij slechts ruimte is voor 1
 * element. Als de wasstraat bezet is moet een
 * wagen die aankomt wachten, is de wasstraat vrij
 * dan kan hij onmiddellijk gewassen worden
 */
public class Carwash {
    private boolean bezet = false;


    /**
     * Als de wasstraat bezet is druk je af welke wagen moet wachten
     *
     * @param wagenNr van de wagen die aankomt
     */
    public synchronized void aankomstWagen(int wagenNr) throws InterruptedException {
        if (bezet) {
            System.out.println("Auto " + wagenNr + " moet wachten");
            wait();
        }

        bezet = true;
    }

    /**
     * De methode vertrekWagen drukt af dat de wagen klaar is.
     *
     * @param wagenNr van de wagen die vertrekt
     */
    public synchronized void vertrekWagen(int wagenNr) {
        System.out.println("wagen " + wagenNr + " vertrekt");
        bezet = false;
        notifyAll();
    }
}
