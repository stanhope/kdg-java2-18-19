package carwash;

public class Car implements Runnable {
    private int wagenNummer;
    private int aankomstTijd;
    private Carwash carwash;

    public Car(int wagenNr, int aankomstTijd, Carwash carwash) {
        wagenNummer = wagenNr;
        this.aankomstTijd = aankomstTijd;
        this.carwash = carwash;
    }

    /**
     * Vul de run-methode van de klasse Car aan. In deze methode begin je met sleep(1000 *
     * aankomsttijd) om de verschillende aankomstmomenten te simuleren. Vervolgens druk
     * je af welke wagen aankomt (wagenNummer) en roep je de methode aankomstWagen op.
     * Daarna druk je af welke wagen nu aan de beurt is om gewassen te worden en voer je daarna
     * een sleep(1600) uit om de tijd nodig voor het wassen te simuleren.
     */
    public void run() {
        try {
            Thread.sleep(1000 * aankomstTijd);

            System.out.println("Auto " + wagenNummer + " is aangekomen");
            carwash.aankomstWagen(wagenNummer);
            System.out.println("Start wagen nummer: " + wagenNummer);
            Thread.sleep(1600);
            carwash.vertrekWagen(wagenNummer);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
