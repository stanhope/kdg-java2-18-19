package lambda;

public class TestCounter {
    public static void main(String[] args) {
        Counter counter = new Counter();

        Thread t1 = new Thread(() -> {
            for (int i = 0; i < 10000; i++) {
                counter.decrement();
            }
        });

        Thread t2 = new Thread(() -> {
            for (int i = 0; i < 10000; i++) {
                counter.increment();
            }
        });

        t1.start();
        t2.start();

        // We wachten tot beide threads zijn uitgewerkt:
        try {
            t1.join();
            t2.join();
        } catch (Exception e) {
            // overbodig
        }


        // Verwachte uitvoer: Value = 0
        System.out.println("Value = " + counter.value());
    }
}

