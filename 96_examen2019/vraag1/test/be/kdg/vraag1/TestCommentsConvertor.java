package be.kdg.vraag1;

import be.kdg.vraag1.model.Comment;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class TestCommentsConvertor {

    CommentsConvertor commentsConvertor;
    List<Comment> commentList;


    @Before
    public void setUp() throws Exception {
        commentsConvertor = new CommentsConvertor();
        commentList = Arrays.asList(new Comment(1, 1, "jos", "jos@gmail.com", "lorem ipsum"),
                new Comment(1, 2, "jefffffffffffffff", "jef@gmail.com", "lorem ipsum"),
                new Comment(1, 3, "an", "an@gmail.com", "lorem ipsum"),
                new Comment(2, 4, "marie", "marie@gmail.com", "lorem ipsum"),
                new Comment(2, 5, "fien", "fien@gmail.com", "lorem ipsum"));

        commentList = commentsConvertor.convert(commentList);
    }

    @After
    public void tearDown() throws Exception {


    }

    @Test
    public void testConvertName() {

        for (Comment comment : commentList) {
            boolean small = comment.getName().length() <= 15;
            assertTrue(small);
        }
    }

    @Test
    public void testConvertPostID() {
        for (Comment comment : commentList) {
            assertEquals(comment.getPostId(), 1);
        }
    }

    @Test
    public void testConvertEmail() {
        for (Comment comment : commentList) {
            assertTrue(comment.getEmail().contains("@"));
        }
    }

    @Test
    public void testConvertContent() {
        for (Comment comment : commentList) {
            assertEquals(comment.getContent(), comment.getContent().toUpperCase());
        }

    }

}
