package be.kdg.vraag1;

/**
 * NAAM: Gerd Goemans
 * KLASGROEP: INF 201A
 */


import be.kdg.vraag1.model.Comment;

import java.io.FileNotFoundException;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.util.List;

//IN DEZE KLASSE DOE JE GEEN WIJZIGINGEN
public class Main {
    public static void main(String[] args) {
        try {
            CommentsJSON commentsJSON = new CommentsJSON(Paths.get("96_examen2019/datafiles/comments.json").toFile());
            CommentsConvertor commentsConvertor = new CommentsConvertor();
            CommentsDAO commentsDAO = new CommentsDAO("96_examen2019/database/commentsdb");
            List<Comment> comments = commentsJSON.readComments();
            System.out.println("Aantal comments: " + comments.size());
            List<Comment> converted = commentsConvertor.convert(comments);
            System.out.println("Aantal converted comments: " + converted.size());
            commentsDAO.insertComments(converted);
            commentsDAO.closeConnection();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
