package be.kdg.vraag1;

import be.kdg.vraag1.model.Comment;

import java.util.List;
import java.util.stream.Collectors;

public class CommentsConvertor {
    public List<Comment> convert(List<Comment> comments) {

        return comments.stream()
                .filter(comment -> comment.getPostId() == 1)
                .filter(comment -> comment.getEmail().contains("@"))
                .map(comment -> {
                    String name = comment.getName();
                    if (name.length() > 15) {
                        name = name.substring(0, 14);
                    }
                    String content = comment.getContent();
                    if (content != null) {
                        content = content.toUpperCase();
                    }

                    return new Comment(comment.getPostId(), comment.getId(), name, comment.getEmail(), content);
                })
                .collect(Collectors.toList());
    }
}
