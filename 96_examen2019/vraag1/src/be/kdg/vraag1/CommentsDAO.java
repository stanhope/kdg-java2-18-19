package be.kdg.vraag1;

import be.kdg.vraag1.model.Comment;

import java.sql.*;
import java.util.List;

public class CommentsDAO {
    private Connection connection;

    public CommentsDAO(String db) throws SQLException {
        this.connection =
                DriverManager.getConnection("jdbc:hsqldb:file:" + db, "sa", "");
        try {
            Statement statement = connection.createStatement();
            boolean databaseDropped = statement.execute("DROP TABLE IF EXISTS comments");
            if (databaseDropped) {
                System.out.println("Database has been dropped and is being recreated");
            }
            String createQuery = "CREATE table comments " +
                    "(ID integer not null, " +
                    "name varchar(15), " +
                    "email varchar(100), " +
                    "content varchar(500) )";
            statement.execute(createQuery);
            connection.commit();
            System.out.println("Database aangemaakt");

        } catch (Exception e) {
            e.printStackTrace();
            closeConnection();
        }
    }

    public void closeConnection() throws SQLException {
        this.connection.close();
    }

    public void insertComments(List<Comment> comments) throws SQLException {
        String sqlString = "INSERT INTO comments " +
                "(ID, name, EMAIL, CONTENT) " +
                "VALUES (?, ?,?,?);";
        comments.forEach(comment -> {
            try (PreparedStatement statement = connection.prepareStatement(sqlString)) {
                statement.setInt(1, comment.getPostId());
                statement.setString(2, comment.getName().substring(0,14));
                statement.setString(3, comment.getEmail());
                statement.setString(4, comment.getContent());

                statement.executeUpdate();

                connection.commit();
                statement.close();
            } catch (Exception e) {
                e.printStackTrace();
                try {
                    closeConnection();
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
            }
        });

    }

}
