package be.kdg.vraag1;

import be.kdg.vraag1.model.Comment;
import be.kdg.vraag1.model.Data;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.*;
import java.util.Arrays;
import java.util.List;

public class CommentsJSON {
    private File file;

    public CommentsJSON(File file) {
        this.file = file;
    }

    public List<Comment> readComments() throws FileNotFoundException {
        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();
        List<Comment> commentList = null;

        try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
            Comment[] commentArray = gson.fromJson(reader, Comment[].class);
            commentList = Arrays.asList(commentArray);
            return commentList;
        } catch (IOException e) {
            e.printStackTrace();
        }

        return Data.comments;
    }
}
