package be.kdg.vraag1.model;

//IN DEZE KLASSE MAG JE NIETS WIJZIGEN
//TENZIJ ANNOTATIES TOEVOEGEN
public class Comment {
    private int postId;
    private int id;
    private String name;
    private String email;
    private String content;

    public Comment(int postId, int id, String name, String email, String content) {
        this.postId = postId;
        this.id = id;
        this.name = name;
        this.email = email;
        this.content = content;
    }

    public int getPostId() {
        return postId;
    }
    public int getId() {
        return id;
    }
    public String getName() {
        return name;
    }
    public String getEmail() {
        return email;
    }
    public String getContent() {
        return content;
    }
    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public String toString() {
        return "Comment{" +
                "postId=" + postId +
                ", id=" + id +
                ", name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", content='" + content + '\'' +
                '}';
    }
}
