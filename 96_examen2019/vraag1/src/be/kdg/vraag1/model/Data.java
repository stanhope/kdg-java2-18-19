package be.kdg.vraag1.model;

import java.util.Arrays;
import java.util.List;

//HIER MAG JE NIETS WIJZIGEN, JE KAN DEZE DATA GEBRUIKEN OM TE TESTEN
public class Data {
    public static List<Comment> comments =
            Arrays.asList(new Comment(1,1,"jos","jos@gmail.com","lorem ipsum"),
                    new Comment(1,2,"jef","jef@gmail.com","lorem ipsum"),
                    new Comment(1,3,"an","an@gmail.com","lorem ipsum"),
                    new Comment(2,4,"marie","marie@gmail.com","lorem ipsum"),
                    new Comment(2,5,"fien","fien@gmail.com","lorem ipsum"));
}
