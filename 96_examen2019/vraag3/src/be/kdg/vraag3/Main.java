package be.kdg.vraag3;

import java.time.Duration;
import java.time.Instant;

//IN DEZE KLASSE MAG JE NIETS WIJZIGEN
public class Main {
    public static void main(String[] args) {
        Calculator calculator = new Calculator();
        Instant start = Instant.now();
        new MethodRunner().runMethods(calculator);
        Instant stop = Instant.now();
        System.out.println("Aantal seconden:" + Duration.between(start,stop).getSeconds());
    }
}
