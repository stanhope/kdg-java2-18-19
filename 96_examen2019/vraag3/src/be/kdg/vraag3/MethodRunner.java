package be.kdg.vraag3;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class MethodRunner {
    public void runMethods(Object object) {
        int sum = 0;
        Set<Future<Integer>> set = new HashSet<>();
        Method[] methods = object.getClass().getDeclaredMethods();
        ExecutorService pool = Executors.newFixedThreadPool(4);

        for (Method method : methods) {
            if (method.getModifiers() != Modifier.PUBLIC) continue;

            if (method.getDeclaredAnnotations().length > 0) {
                for (Annotation methodAnnotation : method.getDeclaredAnnotations()) {
                    /*Dit deel is fout*/
                    if (methodAnnotation.annotationType() == SlowMethod.class) {
                        Callable<Integer> callable = new TestCallable(method, object);
                        Future<Integer> future = pool.submit(callable);
                        set.add(future);
                    }
                    /*Eind foute deel*/
                }
            } else {
                try {
                    Object obj2 = method.invoke(object);
                    sum += (int) obj2;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        }

        /*Dit deel is ook fout*/
        for (Future<Integer> future2 : set) {
            try {
                sum += future2.get();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        pool.shutdown();
        /*Eind foute deel*/

        System.out.println("The sum: " + sum);
    }
}

/*Deze klasse mag eigenlijk niet bestaan*/
class TestCallable implements Callable<Integer> {
    private Method method;
    private Object obj;

    public TestCallable(Method method, Object obj) {
        this.method = method;
        this.obj = obj;
    }

    public Integer call() throws Exception {
        try {
            Object obj2 = method.invoke(obj);
            return (int) obj2;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}


