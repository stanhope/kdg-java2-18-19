package be.kdg.vraag3;

//IN DEZE KLASSE MAG JE NIETS WIJZIGEN
//TENZIJ ANNOTATIES TOEVOEGEN
public class Calculator {
    public int heavyCalculation1(){
        System.out.println("Running heavy calculation 1...");
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return 100000;
    }

    public int heavyCalculation2(){
        System.out.println("Running heavy calculation 2...");
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return 10000;
    }

    public int heavyCalculation3(){
        System.out.println("Running heavy calculation 3...");
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return 1000;
    }

    public int easyCalculation1(){
        System.out.println("Running easy calculation 1...");
        return 100;
    }

    public int easyCalculation2(){
        System.out.println("Running easy calculation 2...");
        return 10;
    }

    public int easyCalculation3(){
        System.out.println("Running easy calculation 3...");
        return 1;
    }

    private void privateMethod(){
        System.out.println("THIS SHOULD NOT RUN!");
    }
}
