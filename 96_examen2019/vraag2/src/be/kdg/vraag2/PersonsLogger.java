package be.kdg.vraag2;

import java.util.Observable;
import java.util.Observer;
import java.util.logging.Logger;

public class PersonsLogger implements Observer {
    private static final Logger logger = Logger.getLogger("be.kdg.vraag2.personenlogger");

    @Override
    public void update(Observable o, Object arg) {
        logger.info((String) arg);
    }
}
