package be.kdg.vraag2.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

//
public class Persons extends Observable {
    private List<Person> personList = new ArrayList<>();

    public void addPerson(Person person) {
        super.setChanged();
        super.notifyObservers(person.toString() + " was added");
        personList.add(person);
    }

    public double getAverageBMI() {
        /*double avarage = 0;
        for (Person person : personList) {
            avarage += person.getBMI();
        }

        return avarage / (personList.size());*/
        return personList.stream().mapToDouble(Person::getBMI).average().getAsDouble();
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder("Persons:\n");
        personList.forEach(p -> stringBuilder.append(p).append("\n"));
        return stringBuilder.toString() + "Average BMI:" + getAverageBMI();
    }
}
