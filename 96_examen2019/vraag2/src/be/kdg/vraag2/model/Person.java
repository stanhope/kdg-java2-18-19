package be.kdg.vraag2.model;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

//IN DEZE KLASSE MAG JE NIETS WIJZIGEN
public class Person {
    public static List<Person> getData() {
        return Arrays.asList(new Person(true,34,67,1.70),
                new Person(false,45,78,1.70),
                new Person(true,16,67,1.67),
                new Person(true,78,56,1.45)
                );
    }
    private boolean female;
    private int age;
    private double weight;
    private double length;

    public static Person randomPerson(){
        Random r = new Random();
        return new Person(r.nextBoolean(),r.nextInt(100),
                r.nextDouble()*100+50,r.nextDouble()*0.8 + 1.2);
    }

    private Person(boolean female, int age, double weight, double length) {
        this.female = female;
        this.age = age;
        this.weight = weight;
        this.length = length;
    }

    public boolean isFemale() {
        return female;
    }

    public int getAge() {
        return age;
    }

    public double getWeight() {
        return weight;
    }

    public double getLength() {
        return length;
    }

    public double getBMI(){
        return weight/(length*length);
    }

    @Override
    public String toString() {
        return String.format("%d year old %s, weight:%3.0fkg, length: %3.1fm, BMI %5.2f",age,
                (female?"female":"male"),weight, length, getBMI());
    }
}
