package be.kdg.vraag2;

import be.kdg.vraag2.model.Person;
import be.kdg.vraag2.model.Persons;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class OldMalesGenerator {
    public static void fill(Persons oldMales) {
        //List<Person> personsList = Stream.generate(Person::randomPerson).filter(person -> person.getAge() > 95).limit(5).collect(Collectors.toList());
        List<Person> personsList = Stream.generate(Person::randomPerson).filter(person -> person.getAge() > 95 && !person.isFemale()).limit(5).collect(Collectors.toList());
        personsList.forEach(oldMales::addPerson);
    }
}
