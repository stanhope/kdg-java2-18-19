package be.kdg.vraag2;

import be.kdg.vraag2.model.Persons;

public class Main {
    public static void main(String[] args) {
        Persons oldMales = new Persons();
        //
        PersonsLogger personsLogger = new PersonsLogger();
        oldMales.addObserver(personsLogger);


        OldMalesGenerator.fill(oldMales);
        System.out.println(oldMales);
    }
}
