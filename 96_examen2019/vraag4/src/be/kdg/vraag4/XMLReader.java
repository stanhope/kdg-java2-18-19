package be.kdg.vraag4;


import be.kdg.vraag4.model.Todo;
import be.kdg.vraag4.model.Todos;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.util.stream.Collectors;

public class XMLReader {
    private static final String FILENAME = "96_examen2019/datafiles/todo.xml";

    public static void main(String[] args) {
        try {
            JAXBContext context = JAXBContext.newInstance(Todos.class);
            Unmarshaller unmarshaller = context.createUnmarshaller();
            File file = new File(FILENAME);

            Todos todos = (Todos) unmarshaller.unmarshal(file);

            System.out.print("Total completed todos: ");
            System.out.println(todos.getTodoList().stream().filter(Todo::isCompleted).count());

            todos.getTodoList()
                    .stream()
                    .filter(Todo::isCompleted)
                    .collect(Collectors.groupingBy(Todo::getUserId))
                    .forEach((integer, todos1) -> System.out.printf("User %s completed %d\n", integer, todos1.stream().filter(todo -> todo.getUserId() == integer).count()));

        } catch (JAXBException e) {
            e.printStackTrace();
        }
    }
}
