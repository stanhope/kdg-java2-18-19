package be.kdg.vraag4.model;

public class Todo {
    private boolean completed;
    private int id;
    private String title;
    private int userId;

    public Todo(int id, String title, int userId) {
        this.id = id;
        this.title = title;
        this.userId = userId;
    }

    public boolean isCompleted() {
        return completed;
    }

    public void setCompleted(boolean completed) {
        this.completed = completed;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public Todo() {
    }

    @Override
    public String toString() {
        return "Todo{" +
                "completed=" + completed +
                ", id=" + id +
                ", title='" + title + '\'' +
                ", userId=" + userId +
                '}';
    }
}
