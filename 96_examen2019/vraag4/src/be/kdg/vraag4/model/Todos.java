package be.kdg.vraag4.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement
public class Todos {
    private List<Todo> todoList;

    public Todos() {
        this.todoList = new ArrayList<>();
    }

    public List<Todo> getTodoList() {
        return todoList;
    }

    public void add(Todo todo) {
        this.todoList.add(todo);
    }

    @XmlElement(name = "todo")
    public void setTodoList(List<Todo> todoList) {
        this.todoList = todoList;
    }
}
