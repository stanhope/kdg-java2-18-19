package factory;

import java.util.Random;

public class GetallenGenerator {
    private Random random = new Random();
    private int min;
    private int max;

    private GetallenGenerator(int min, int max) {
        if (max <= min)
            throw new IllegalArgumentException("de min-waarde moet kleiner zijn dan de max-waarde");
        else {
            this.max = max;
            this.min = min;
        }
    }

    public static GetallenGenerator getGetallenGenerator(int min, int max) {
        return new GetallenGenerator(min, max);
    }

    public static GetallenGenerator getGetallenGeneratorWithFloor(int min) {
        return new GetallenGenerator(min, Integer.MAX_VALUE);
    }

    public static GetallenGenerator getGetallenGeneratorWithCeil(int max) {
        return new GetallenGenerator(0, max);
    }


    public int genereer() {
        return random.nextInt(max - min) + min;
    }
}