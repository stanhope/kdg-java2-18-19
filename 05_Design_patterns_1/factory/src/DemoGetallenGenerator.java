import factory.GetallenGenerator;

public class DemoGetallenGenerator {
    public static void main(String[] args) {

        System.out.println("getallen 10 .. 20");
        GetallenGenerator gg1 = GetallenGenerator.getGetallenGenerator(10, 21);
        for (int i = 0; i < 20; i++) {
            System.out.print(gg1.genereer() + " ");
        }
        System.out.println();

        System.out.println("getallen > 100 000");
        GetallenGenerator gg2 = GetallenGenerator.getGetallenGeneratorWithFloor(100000);
        for (int i = 0; i < 20; i++) {
            System.out.print(gg2.genereer() + " ");
        }
        System.out.println();

        System.out.println("getallen 0 .. 100");
        GetallenGenerator gg3 = GetallenGenerator.getGetallenGeneratorWithCeil(101);
        for (int i = 0; i < 20; i++) {
            System.out.print(gg3.genereer() + " ");
        }
        System.out.println();
    }
}
