package singleton;

public class President {
    private String naam;
    private String partij;
    private static President onlyInstance = new President();

    private President() {
        this.naam = "onbekend";
        this.partij = "onbekend";
    }

    public static President getInstance() {
        return onlyInstance;
    }

    public String getNaam() {
        return naam;
    }

    public String getPartij() {
        return partij;
    }

    public void setPresident(String naam, String partij) {
        this.naam = naam;
        this.partij = partij;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        throw new CloneNotSupportedException("Singleton pattern");
    }

    public String toString() {
        return "President: " + naam + ", partij: " + partij;
    }
}

