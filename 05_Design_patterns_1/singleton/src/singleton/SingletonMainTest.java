package singleton;

public class SingletonMainTest {
    private static SingletonMainTest ourInstance = new SingletonMainTest();

    public static SingletonMainTest getInstance() {
        return ourInstance;
    }

    private SingletonMainTest() {
    }
}
