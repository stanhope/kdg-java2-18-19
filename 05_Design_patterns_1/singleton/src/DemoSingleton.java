import singleton.President;

public class DemoSingleton {
    public static void main(String[] args) {
        President p1 = President.getInstance();
        p1.setPresident("Barack Obama", "Democrats");

        President p2 = President.getInstance();
        p2.setPresident("Donald Trump", "Republicans");

        System.out.println(p1);
        System.out.println(p2);
        System.out.printf("zelfde objecten: %b", p1 == p2);
    }
}
