package be.kdg.demo.singleton;

import java.util.ArrayList;
import java.util.List;

/**
 * Todo Vul deze klasse aan.
 */
public final class UniekeVerzameling {
    private static Verzameling uniekeVerzameling = new Verzameling();


    private UniekeVerzameling(){

    }

    public static synchronized Verzameling getInstance(){
        return uniekeVerzameling;
    }


}
