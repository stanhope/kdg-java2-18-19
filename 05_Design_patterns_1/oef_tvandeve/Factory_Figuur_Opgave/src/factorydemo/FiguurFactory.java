package factorydemo;

public class FiguurFactory {

    public FiguurFactory() {
    }

    public static Figuur getFiguur(FiguurType type, double zijde){
        Figuur figuur = null;
        switch (type){
            case VIERKANT:
                figuur =  getVierkant(zijde);
                break;
            case RECHTHOEK:
                figuur = getRechthoek(zijde);
                break;
        }
        return figuur;
    }

    public static Figuur getFiguur(FiguurType type, double dimEen, double dimTwee){
        Figuur figuur = null;
        switch (type){
            case RUIT:
                figuur =  Ruit.newRuit(dimEen, dimTwee);
                break;
            case RECHTHOEK:
                figuur = getRechthoek(dimEen, dimTwee);
                break;
        }
        return figuur;
    }

    public static Figuur getRechthoek(double zijde){
        return Rechthoek.newRechthoek(zijde, zijde);
    }

    public static Figuur getRechthoek(double breedte, double hoogte){
        return Rechthoek.newRechthoek(breedte, hoogte);
    }

    public static Figuur getVierkant(double zijde){
        return Vierkant.newVierkant(zijde);
    }
}
