package factorydemo;

public class Ruit implements Figuur{
    private double groteDiagonaal;
    private double kleineDiagonaal;

    private Ruit(double groteDiagonaal, double kleineDiagonaal) {
        this.groteDiagonaal = groteDiagonaal;
        this.kleineDiagonaal = kleineDiagonaal;
    }

    public double getGroteDiagonaal() {
        return groteDiagonaal;
    }

    public double getKleineDiagonaal() {
        return kleineDiagonaal;
    }

    public static Ruit newRuit(double groot, double klein){
        return new Ruit(groot, klein);
    }


    @Override
    public double oppervlakte() {
        return getGroteDiagonaal() * getKleineDiagonaal();
    }

    @Override
    public String toString(){
       return String.format("Ruit: %f X %f", getGroteDiagonaal(), getKleineDiagonaal());
    }
}
