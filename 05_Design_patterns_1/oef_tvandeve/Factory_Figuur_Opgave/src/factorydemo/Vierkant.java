package factorydemo;

public class Vierkant implements Figuur {
    private double zijde;

    private Vierkant(double zijde) {
        this.zijde = zijde;
    }

    public double getZijde() {
        return zijde;
    }

    public static Vierkant newVierkant(double zijde){
        return new Vierkant(zijde);
    }

    @Override
    public double oppervlakte() {
        return getZijde() * getZijde();
    }

    @Override
    public String toString(){
        return String.format("Vierkant: %f X %f", getZijde(), getZijde());
    }
}
