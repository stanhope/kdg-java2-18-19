package factorydemo;

public class Rechthoek implements Figuur {
    private double breedte;
    private double hoogte;

    private Rechthoek(double breedte, double hoogte) {
        this.breedte = breedte;
        this.hoogte = hoogte;
    }

    public static Rechthoek newRechthoek(double b, double h){
        return new Rechthoek(b, h);
    }

    public double getBreedte() {
        return breedte;
    }

    public double getHoogte() {
        return hoogte;
    }

    @Override
    public double oppervlakte() {
        return getBreedte() * getHoogte();
    }

    @Override
    public String toString(){
        return String.format("Rechthoek: %f X %f", getBreedte(), getHoogte());
    }
}
