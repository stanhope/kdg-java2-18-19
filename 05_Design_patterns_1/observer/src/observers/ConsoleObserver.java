package observers;

import model.PPPmodel;

import java.util.Observable;
import java.util.Observer;

/**
 * Een Observer die enkel geïnteresseerd is in personeelsdata
 */
public class ConsoleObserver implements Observer {


    @Override
    public void update(Observable o, Object arg) {
        PPPmodel model = (PPPmodel) o;
        System.out.println(model.toString());
    }
}
