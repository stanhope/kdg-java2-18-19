package observers;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.chart.PieChart;
import javafx.scene.layout.BorderPane;
import model.PPPmodel;


import java.util.Observable;
import java.util.Observer;

/**
 * Een JavaFX PieChart die de data van het PPPmodel grafisch wil voorstellen
 */
public class PieChartView extends BorderPane implements Observer {
    private PieChart pieChart;

    public PieChartView() {
        pieChart = new PieChart();
        this.setCenter(pieChart);
    }

    @Override
    public void update(Observable o, Object arg) {
        PPPmodel model = (PPPmodel) o;

        ObservableList<PieChart.Data> myData = FXCollections.observableArrayList(
                new PieChart.Data("Personeel", model.getPersoneel()),
                new PieChart.Data("Productie", model.getProductie()),
                new PieChart.Data("Proces", model.getProces())
        );
        pieChart.setData(myData);
        pieChart.setTitle(model.getMaand());
    }

}
