package observers;

import javafx.scene.control.TextArea;
import javafx.scene.layout.BorderPane;
import model.PPPmodel;


import java.util.Observable;
import java.util.Observer;

/**
 * Een JavaFX viewklasse die geinteresseerd is in alle wijzigingen van het PPPmodel
 */
public class LoggingView extends BorderPane implements Observer {
    private TextArea txaLogging;

    public LoggingView() {
        txaLogging = new TextArea();
        txaLogging.setPrefWidth(400);
        this.setCenter(txaLogging);
    }

    @Override
    public void update(Observable o, Object arg) {
        PPPmodel model = (PPPmodel) o;
        txaLogging.setText(txaLogging.getText() + "\n" + model.toString());
    }
}
