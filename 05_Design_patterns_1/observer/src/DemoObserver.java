import data.DataInputPane;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;
import model.PPPmodel;
import observers.ConsoleObserver;
import observers.LoggingView;
import observers.PieChartView;


public class DemoObserver extends Application {
    @Override
    public void start(Stage stage1) {
        PPPmodel model = new PPPmodel();

        PieChartView pieChartView = new PieChartView();
        LoggingView loggingView = new LoggingView();
        ConsoleObserver consoleObserver = new ConsoleObserver();
        DataInputPane dataInputPane = new DataInputPane(model);

        model.addObserver(pieChartView);
        model.addObserver(consoleObserver);
        model.addObserver(loggingView);

        stage1.setScene(new Scene(dataInputPane));
        stage1.setTitle("Data input");
        stage1.setX(100);
        stage1.setY(200);
        stage1.show();

        Stage stage2 = new Stage();
        stage2.setScene(new Scene(pieChartView));
        stage2.setTitle("PieChart");
        stage2.setX(350);
        stage2.setY(200);
        stage2.show();

        Stage stage3 = new Stage();
        stage3.setScene(new Scene(loggingView));
        stage3.setTitle("Logging");
        stage3.setX(880);
        stage3.setY(200);
        stage3.show();
    }

    public static void main(String[] args) {
        Application.launch(args);
    }
}