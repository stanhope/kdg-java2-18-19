package model;

import java.util.Observable;

/**
 * Dit is een typische modelklasse:
 * dus geen communicatie met de gebruiker!
 */
public class PPPmodel extends Observable {
    private String maand;
    private double personeel;
    private double productie;
    private double proces;

    public void setData(String maand, double personeel, double productie, double proces) {
        this.maand = maand;
        this.personeel = personeel;
        this.productie = productie;
        this.proces = proces;
        super.setChanged();
        super.notifyObservers();
    }

    public String getMaand() {
        return maand;
    }

    public double getPersoneel() {
        return personeel;
    }

    public double getProductie() {
        return productie;
    }

    public double getProces() {
        return proces;
    }

    @Override
    public String toString() {
        return String.format("%-15s: PERS=%3.0f PROD=%3.0f PROC=%3.0f", maand, personeel, productie, proces);
    }
}
