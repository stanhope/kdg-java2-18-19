package data;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import model.PPPmodel;


/**
 * Deze JavaFX viewklasse laat de gebruiker data invoeren.
 * Er is geen presenter aanwezig; de view stuurt de data ineens door naar het PPPmodel
 */
public class DataInputPane extends GridPane {
    private PPPmodel model;
    private ComboBox<String> cboMaanden;
    private TextField tfPersoneel;
    private TextField tfProductie;
    private TextField tfProces;
    private Button btnVoegToe;

    public DataInputPane(PPPmodel model) {
        this.model = model;
        initNodes();
        layoutNodes();
        addEvents();
    }

    private void initNodes() {
        cboMaanden = new ComboBox<>();
        ObservableList<String> namen = FXCollections.observableArrayList(
                "januari", "februari", "maart", "april", "mei", "juni", "juli", "augustus", "september", "oktober", "november", "december");
        cboMaanden.setItems(namen);
        cboMaanden.getSelectionModel().select(0);
        tfPersoneel = new TextField();
        tfPersoneel.setPrefWidth(30);
        tfProductie = new TextField();
        tfProductie.setPrefWidth(30);
        tfProces = new TextField();
        tfProces.setPrefWidth(30);
        btnVoegToe = new Button("Voeg toe");
        btnVoegToe.setPrefWidth(200);
    }

    private void layoutNodes() {
        this.setPadding(new Insets(10));
        this.setHgap(10);
        this.setVgap(10);
        this.setAlignment(Pos.CENTER);
        this.add(new Label("Maand:"), 0, 0);
        this.add(cboMaanden, 1, 0);
        this.add(new Label("Personeel:"), 0, 1);
        this.add(tfPersoneel, 1, 1);
        this.add(new Label("Productie:"), 0, 2);
        this.add(tfProductie, 1, 2);
        this.add(new Label("Proces:"), 0, 3);
        this.add(tfProces, 1, 3);
        this.add(btnVoegToe, 0, 4, 2, 1);
        setHalignment(btnVoegToe, HPos.CENTER);
    }

    private void addEvents() {
        btnVoegToe.setOnAction(
                new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent event) {
                        try {
                            model.setData(
                                    cboMaanden.getValue(),
                                    Double.parseDouble(tfPersoneel.getText()),
                                    Double.parseDouble(tfProductie.getText()),
                                    Double.parseDouble(tfProces.getText()));
                        } catch (NumberFormatException e) {
                            System.out.println(e);
                        }
                    }
                }
        );
    }
}
