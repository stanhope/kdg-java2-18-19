package reflection;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/** Examen Programmeren 2 - Java
 * Januari 2017
 */
public class ReflectionRunner {

    public static void reflect(Class myClass){
        // Opgave 2.7
   try{     Object o = myClass.newInstance();
        System.out.println("Voor reflection: " + o);
       System.out.println(o);

       for(Method m : myClass.getDeclaredMethods()){
           System.out.print(m.getName() + " ");
           Changeable unfinished = m.getAnnotation(Changeable.class);


           if(unfinished != null){
               m.invoke(o, "Mark Goovaerts");
           }

       }

       System.out.println("\nNa reflection: " + o);
    } catch (InstantiationException | IllegalAccessException  e) {
        e.printStackTrace();
    } catch (InvocationTargetException e) {
       e.printStackTrace();
   }
    }
}
