package bestellingen;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
/** Examen Programmeren 2 - Java
 * Januari 2017
 */
final public class Bestelling {
    private String student;
    private final Set<Boek> boekenSet;

    public Bestelling(String student, Set<Boek> boekenSet) {
        this.student = student;
        this.boekenSet = boekenSet;
    }

    public String getStudent() {
        return student;
    }

    public void setStudent(String student) {
        this.student = student;
    }

    public Set<Boek> getBoekenSet() {
        // Opgave 2.2
        return  boekenSet;
    }

   /*public void setBoekenSet(Set<Boek> boekenSet) {
        this.boekenSet = boekenSet;
    }*/

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("Bestelling van %s:\n");
        for (Boek boek : boekenSet) {
            sb.append('\t').append(boek);
        }
        return sb.toString();
    }
}
