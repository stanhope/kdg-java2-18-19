import bestellingen.Bestelling;
import bestellingen.Boek;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.*;

/** Examen Programmeren 2 - Java
 * Januari 2017
 */
public class TestBestelling {
    private Bestelling bestelling;
    private Set<Boek> boekenSet = new HashSet();
    private Boek boek ;

    public void init() {
        boekenSet.add(new Boek("Java", "J. Gosling", 35));
        boekenSet.add(new Boek("Android", "Murach", 52));
        bestelling = new Bestelling("Joske Vermeulen", boekenSet);
       boek = new Boek("Java", "J. Gosling", 35);
    }

    @Test
    public void testUniek() {
        init();
        assertTrue( boekenSet.contains(boek));
      }

        // Opgave 2.5



    @Test(expected = AssertionError.class)
    public void testReadOnly() {
        init();
        // Opgave 2.6
        bestelling.getBoekenSet().add(new Boek("Java", "J. Gosling", 45));
        fail("De lijst moet READ-ONLY zijn!");


    }
}
