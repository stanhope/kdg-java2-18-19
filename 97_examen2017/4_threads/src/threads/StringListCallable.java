package threads;

import java.util.List;
import java.util.concurrent.Callable;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/** Examen Programmeren 2 - Java
 * Januari 2017
 */
public class StringListCallable implements Callable {
    private List<String> stringList;

    public StringListCallable(List<String> stringList) {
        this.stringList = stringList;
    }

    @Override
    public Object call() throws Exception {

        List<String>  list =  stringList.stream().map(x-> x.substring(0,2).toUpperCase() + "-").sorted((x,y) -> x.compareTo(y))
                .distinct().collect(Collectors.toList());
        return list;
    }

    //Opgave 4.1
}
