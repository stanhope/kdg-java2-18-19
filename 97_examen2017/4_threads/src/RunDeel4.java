import threads.StringListCallable;

import java.time.LocalDate;
import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/** Examen Programmeren 2 - Java
 * Januari 2017
 */
public class RunDeel4 {
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        List<String> spellingWoorden = Arrays.asList("Alfa", "Bravo", "Charlie", "Delta", "Echo",
                "Foxtrot", "Golf", "Hotel", "India", "Juliett", "Kilo", "Lima", "Mike",
                "November", "Oscar", "Papa", "Quebec", "Romeo", "Sierra", "Tango",
                "Uniform", "Victor", "Wisky", "X-ray", "Yankee", "Zulu");
        List<String> euroLanden = Arrays.asList("Belgie", "Bulgarije", "Cyprus", "Denemarken", "Duitsland",
                "Estland", "Finland", "Frankrijk", "Griekenland", "Hongarije", "Ierland", "Italie",
                "Kroatie", "Letland", "Litouwen", "Luxemburg", "Malta", "Nederland", "Oostenrijk",
                "Polen", "Portugal", "Roemenie", "Slovenie", "Slowakijke", "Spanje", "Tsjechie",
                "Verenigd Koninkrijk", "Zweden");

        ExecutorService pool = Executors.newFixedThreadPool(2);
        //Opgave 4.2
        StringListCallable callable1 = new StringListCallable(euroLanden);
        StringListCallable callable2 = new StringListCallable(spellingWoorden);


        HashSet<Future<List<String>>> set = new HashSet<>();
        Future<List<String>> future = pool.submit(callable1);
        set.add(future);

        future = pool.submit(callable2);
        set.add(future);
        String papegaaiList;


            try {
                System.out.println("spellingwoorden: " );  set.iterator().next().get().forEach(System.out::print);
                System.out.println();
                System.out.println("eurolanden: " );
                set.iterator().next().get().forEach(System.out::print);
            } catch (InterruptedException | ExecutionException  | ClassCastException e) {
                e.printStackTrace();
            }



                //tegles.forEach(System.out::println);



        pool.shutdown();



    }
}
