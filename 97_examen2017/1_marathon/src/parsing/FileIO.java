package parsing;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import marathon.MarathonLoper;
import org.jdom2.Attribute;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;
import java.io.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/** Examen Programmeren 2 - Java
 * Januari 2017
 * NAAM:
 * KLASGROEP:
 */
public class FileIO {

    public static List<MarathonLoper> jdomReadXml(String fileName) {
        List<MarathonLoper> myList = new ArrayList<>();
        try {
            SAXBuilder saxBuilder = new SAXBuilder();
            File inputFile = new File(fileName);
            Document document = saxBuilder.build(inputFile);
            // Opgave 1.1

            Element rootElement = document.getRootElement();
            System.out.println("Root element :" + rootElement.getName());
            System.out.println("----------------------------");

            List<MarathonLoper> marathonLoperList = rootElement.getChildren()
                    .stream()
                    .map(e -> {
                        //onderzoek sub-elements:

                        return new MarathonLoper(
                                e.getChild("naam").getText(),
                                e.getChild("herkomst").getText(),
                                LocalDate.parse(e.getChild("wedstrijddatum").getText()),
                                Integer.parseInt(e.getChild("seconden").getText()));
                    })
                    .collect(Collectors.toList());

           return marathonLoperList;

      } catch (IOException | JDOMException e) {
            System.out.println(e);
        }
        return myList;
    }

    public static void writeJson(List<MarathonLoper> marathonLoperListList, String fileName) {
        // Opgave 1.4

        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.setPrettyPrinting().create();

        String jsonString = gson.toJson(marathonLoperListList);
        try (PrintWriter jsonWriter = new PrintWriter(
                new OutputStreamWriter(new FileOutputStream(fileName)))) {
            jsonWriter.write(jsonString);
            jsonWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
