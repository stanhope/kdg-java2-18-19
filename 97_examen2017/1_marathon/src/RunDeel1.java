import marathon.MarathonLoper;
import parsing.FileIO;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.partitioningBy;

/** Examen Programmeren 2 - Java
 * Januari 2017
 */
public class RunDeel1 {
    public static void main(String[] args) {
        List<MarathonLoper> myList = FileIO.jdomReadXml("97_examen2017/1_marathon/files/marathon.xml");
        System.out.println("Na JDOM:");
        // Opgave 1.1l(
        myList.forEach(System.out::println);
        System.out.println("\nTOP-3:");
        // Opgave 1.2
     String top =   myList.stream()
             .sorted((x,y) -> x.getTimeString()
                     .compareTo(y.getTimeString()))
             .limit(3).map(e -> String.format("%s  (%s)", e.getNaam(), e.getTimeString()))
             .collect(Collectors.joining("<"));


        System.out.println(top);
        String result = "";
        System.out.println(result);

        System.out.println("\nKenia:");
        //Opgave 1.3
        Map<Boolean, List<MarathonLoper>> myMap = null;

       // myMap.values() = myList.stream().sorted((x,y) -> x.getNaam().compareTo(y.getNaam())).filter(x -> x.equals("Kenia")).collect(Collectors.toMap(Function.identity(),));
        myMap = myList.stream().sorted((x,y) -> x.getNaam()
                .compareTo(y.getNaam())).collect(partitioningBy(x -> x.getHerkomst().equals("Kenia")));
        for (Boolean b: myMap.keySet()) {
            if (b == true){
                System.out.println(myMap.get(b));
            }
        }
        //Opgave 1.4
        FileIO.writeJson(myMap.get(true), "97_examen2017/1_marathon/files/kenia.json");
    }
}
