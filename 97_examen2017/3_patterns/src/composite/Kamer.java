package composite;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import java.util.concurrent.SynchronousQueue;
import java.util.stream.Collectors;
import java.util.stream.DoubleStream;

/** Examen Programmeren 2 - Java
 * Januari 2017
 */
public class Kamer  extends Observable implements Schilderbaar{
    private List<Schilderbaar> myList = new ArrayList<>();
    private String naam;

    public Kamer(String naam) {
        this.naam = naam;
    }
    public void voegToe(Schilderbaar element) {


        setChanged();
        notifyObservers(element.getClass().getName() + " toegevoegd. Benodigde verf is nu: "   + element.benodigdeVerf()  + "… liter");

        myList.add(element);
    }


    public double benodigdeVerf() {
        // Opgave 3.2

        double y =   myList.stream().mapToDouble(Schilderbaar::benodigdeVerf).reduce((a,b) -> a+b).getAsDouble();

       /* double som = 0;
        for(Schilderbaar i : myList){
      som +=      i.benodigdeVerf();
        }*/
        return y;
    }

    public String getNaam() {
        return naam;
    }
}
