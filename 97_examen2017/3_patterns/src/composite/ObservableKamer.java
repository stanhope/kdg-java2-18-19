package composite;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

/**
 * Created by leonora on 15/01/2018.
 */
public class ObservableKamer  extends Observable implements Schilderbaar{
    private List<Schilderbaar> myList = new ArrayList<>();
    private String naam;

    public ObservableKamer(String name) {
       this.naam = name;
    }

    public void voegToe(Schilderbaar element) {


        setChanged();
        notifyObservers("toegevoegd: " + element.toString());

        myList.add(element);
    }

    @Override
    public double benodigdeVerf() {
        return 0;
    }
}
