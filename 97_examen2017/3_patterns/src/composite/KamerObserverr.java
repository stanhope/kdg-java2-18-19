package composite;

import java.util.Observable;
import java.util.Observer;

/**
 * Created by leonora on 15/01/2018.
 */
public class KamerObserverr implements Observer {

    private Kamer kamer;

    public KamerObserverr(Kamer punt) {
        this. kamer= punt;
    }

    public void update(Observable observable, Object object) {
        System.out.println(object + " waarde gewijzigd, nieuwe waarden: " + kamer.benodigdeVerf());
    }
}
